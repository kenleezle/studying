class SimplifyDirectory
  def simplify(path)
    raise "path must start with forward slash" unless path.start_with?("/")

    components = path.split("/")

    components.shift # kill the first empty string

    stack = []

    components.each do | component |
      next if component == "."
      next if component == ""

      if component == ".."
        stack.pop
      else
        stack.push(component)
      end
    end

    "/" + stack.join("/")
  end
end
