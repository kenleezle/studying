class Permutations
  def initialize
    @cache = {
      "" => [""]
    }
  end

  def removeFirstOccurence(char, str)
    index = str.index(char)
    return str[0,index] + str[index+1, str.length-index-1]
  end

  def unique_permutations(str)
    return @cache[str] if @cache.has_key?(str)

    unique_chars = str.chars.uniq

    perms = []

    unique_chars.each do | char |
      part = removeFirstOccurence(char, str)
      perms += unique_permutations(part).map { | perm | char + perm }
    end

    @cache[str] = perms

    return perms
  end
end
