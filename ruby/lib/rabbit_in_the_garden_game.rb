class Garden

  def initialize(garden_matrix)
    @num_columns = garden_matrix.size
    @num_rows = garden_matrix[0].size
    @garden_matrix = garden_matrix
  end

  def clear
    @garden_matrix.each_index do | i |
      @garden_matrix[i].each_index do | j |
        @garden_matrix[i][j] = 0
      end
    end
  end

  def rows
    (0..@num_rows-1)
  end

  def cols
    (0..@num_columns-1)
  end

  def to_s
    retval = ""
    rows.reverse_each do | j |
      retval += "[ "
      cols.each do | i |
        retval += " #{@garden_matrix[i][j]} "
      end
      retval += " ]\n"
    end
    retval
  end

  def set_carrots_at_coord(column, row, carrots)
    @garden_matrix[column][row] = carrots
  end

  def has_odd_number_of_columns?
    @num_columns % 2 == 1
  end

  def has_odd_number_of_rows?
    @num_rows % 2 == 1
  end

  def has_exact_center?
    has_odd_number_of_columns? && has_odd_number_of_rows?
  end

  def coord_is_in_garden?(column, row)
    column >= 0 && column < @num_columns && row >= 0 && row < @num_rows
  end

  def coords_accessible_from(column, row)
    possible_coords = [
      [column+1, row],
      [column-1, row],
      [column, row+1],
      [column, row-1]
    ]

    possible_coords.select { | coord | coord_is_in_garden?(*coord) }
  end

  def eat_carrots_at_coord(column, row)
    @garden_matrix[column][row] = 0
  end

  def coord_has_carrots(column, row)
    carrots_at_coord(column, row) > 0
  end

  def carrots_at_coord(column, row)
    @garden_matrix[column][row]
  end

  def accessible_coords_with_carrots_from(column, row)
    coords_accessible_from(column, row).select { | coord |
      coord_has_carrots(*coord)
    }
  end

  def coord_with_most_carrots(coords)
    best_coord = nil
    carrots_at_best_coord = 0

    coords.each do | coord |
      if (carrots_at_coord(*coord) > carrots_at_best_coord)
        best_coord = coord
        carrots_at_best_coord = carrots_at_coord(*coord)
      end
    end

    return best_coord
  end

  def coord_with_most_carrots_accessible_from(column, row)
    accessible_coords = accessible_coords_with_carrots_from(column, row)
    coord_with_most_carrots(accessible_coords)
  end

  def column_center
    @num_columns / 2
  end

  def row_center
    @num_rows / 2
  end

  # TODO test this for 0 and 1
  def exact_center
    [ column_center, row_center ]
  end

  def closest_to_center_with_highest_carrot_count
    # TODO
    # replace this erroneous simplification with this algorithm:
    #
    # search progressively farther and farther from the center for
    # "the square closest to the center with the highest carrot count"
    # breadth first search away from center
    column_centers = [column_center]
    if !has_odd_number_of_columns?
      column_centers.push(column_center - 1)
    end

    row_centers = [row_center]
    if !has_odd_number_of_rows?
      row_centers.push(row_center - 1)
    end

    center_coords = []
    column_centers.each do | column_center |
      row_centers.each do | row_center |
        center_coords.push(column_center, row_center)
      end
    end

    center_coords = center_coords.select { | coord | coord_is_in_garden?(coord) }

    return coord_with_most_carrots(center_coords)
  end
end

class Rabbit
  def initialize(garden)
    @garden = garden
  end

  def start_coords
    if @garden.has_exact_center?
      return @garden.exact_center
    else
      return @garden.closest_to_center_with_highest_carrot_count
    end
  end

  def run_rabbit_run
    carrots_eaten = 0
    current_location = start_coords

    while !current_location.nil? do
      carrots_eaten += @garden.carrots_at_coord(*current_location)
      @garden.eat_carrots_at_coord(*current_location)

      current_location = @garden.coord_with_most_carrots_accessible_from(*current_location)
    end

    return carrots_eaten
  end
end

class RabbitInTheGardenGame
  def play_game(garden_matrix)
    garden = new Garden(garden_matrix)
    rabbit = new Rabbit(garden)
    rabbit.run_rabbit_run
  end
end
