class MinStack
  def initialize
    @stack = []
    @mins = []
  end

  def push(item)
    @stack.push(item)

    min = item
    if (@mins.any? && getMin < item)
      min = getMin
    end
    @mins.push(min)
  end

  def pop
    @stack.pop
    @mins.pop
  end

  def top
    @stack.any? ? @stack.last : -1
  end

  def getMin
    @mins.any? ? @mins.last : -1
  end

end
