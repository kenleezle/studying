class PalindromeParts
  def isPalindrome(str)
    last = str.length - 1
    0.upto(last) do | i |
      return false unless str[i] == str[last - i]
    end
    return true
  end
  def parts(str)
    if (str.empty?)
      return [ [] ]
    end

    if (str.length == 1)
      return [ [str] ]
    end

    first = str[0]
    rest = str[1, str.length - 1]

    parts = parts(rest)

    avoidDups = Hash.new

    result = parts.map { | part | [first] + part }

    parts.each do | part |
      break unless part.length >= 1
      if isPalindrome(first + part[0])
        avoidDups[first + part[0]] = 0
        rest = part.length > 1 ? part[1, part.length - 1] : []
        result.push([first + part[0]] + rest)
      end
    end

    parts.each do | part |
      break unless part.length >= 2

      if isPalindrome(first + part[0] + part[1])
        if avoidDups.has_key?(first + part[0] + part[1]) && avoidDups[first + part[0] + part[1]] != part[0].length
          next
        end

        avoidDups[first + part[0] + part[1]] = part[0].length

        rest = part.length > 2 ? part[2, part.length - 1] : []
        result.push([first + part[0] + part[1]] + rest)
      end
    end

    return result
  end
end
