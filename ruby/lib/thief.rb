class Thief
  def self.max_booty(house_booties = [])
    houses, total = rob_houses(house_booties)
    return total
  end

  def self.choose_houses_to_rob(house_booties = [])
    houses, total = rob_houses(house_booties)
    return houses
  end

  def self.rob_houses(house_booties = [])
    last_total = 0
    total = 0
    hit_last = false
    house_markers = []

    house_booties.each_with_index do | booty, index |
      if !hit_last
        if (booty > 0)
          total += booty
          hit_last = true
        end
      else
        if last_total + booty > total
          tmp = total
          total =  last_total + booty
          last_total = tmp
        else
          hit_last = false
          last_total = total
          house_markers.push(index-1)
        end
      end
    end

    house_markers.push(house_booties.size - 1) if hit_last

    houses = []

    house_markers.reverse.each_with_index do | house_marker, i |
      houses.push(house_marker)
      min = (i == (house_markers.size - 1)) ? -2 : house_markers[i + 1]

      next_house_marker = house_marker - 2
      # [9,6... should produce [9,6 not [9,7
      while (next_house_marker > min + 1)
        houses.push(next_house_marker)
        next_house_marker -= 2
      end
    end

    return houses.reverse, total
  end
end
