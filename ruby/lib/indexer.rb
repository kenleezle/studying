# TODO Fix awful name
class TermFrequency
  include Comparable

  attr_accessor :file, :tf

  def <=>(anOther)
    file <=> anOther.file
  end

  def initialize(file)
    @file = file
    @tf = 0
  end

  def increment_tf
    @tf += 1
  end

  def to_s
    "(#{file},#{tf})"
  end
end

class Indexer
  def initialize(dir)
    @dir = dir
    # word2tfs
    @map = Hash.new
  end
  def files
    Dir.entries(@dir).reject { | entry |
      # TODO make constant
      # Bug - will include directories
      [".",".."].include?(entry)
    }.sort
  end
  def words(file)
    # TODO handle files so big, they don't fit in memory
    File.read(@dir + "/" + file).split(/[^A-Za-z0-9]/)
      .reject { | w | w == "" }
      .map(&:downcase)
  end
  def populateMap
    files.each { | file |
      words(file).each { | word |
        @map[word] = Array.new if @map[word].nil?
        if (@map[word].empty? || @map[word].last.file != file)
          @map[word].push TermFrequency.new(file)
        end

        @map[word].last.increment_tf
      }
    }
  end
  def tf(word, file)
    return 0 if @map[word].nil?

    map_obj = @map[word].select { | map_obj |
      map_obj.file == file
    }.first

    map_obj.nil? ? 0 : map_obj.tf
  end

  def sorted_words
    @map.keys.sort
  end

  def generate_output
    output = ""
    sorted_words.each { | word |
      output += line_for_word(word) + "\n"
    }
    output
  end
  def line_for_word(word)
		map_objs = list_of_docs_and_tfs_for_word(word)
    "#{word}:(#{@map[word].size},[#{map_objs}])"
  end
  def list_of_docs_and_tfs_for_word(word)
    @map[word].map(&:to_s).join(",")
  end
end
