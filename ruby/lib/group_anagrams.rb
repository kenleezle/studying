class GroupAnagrams
  def group_anagrams(words)
    sorted_word_to_group = {}
    groups = []

    words.each_with_index do | word, i |
      sorted_word = word.chars.sort.join

      group = sorted_word_to_group[sorted_word]
      if group.nil?
        group = []
        sorted_word_to_group[sorted_word] = group
        groups.push group
      end

      group.push(i + 1)
    end

    return groups
  end
end
