class Fraction
  def fraction(n, d)
    str = ""
    i = 0

    q = n / d
    r = n % d

    if (n == 0)
      return "0"
    end

    if (n < 0 && d < 0)
      n *= -1
      d *= -1
    end

    if (n < 0)
      str += "-"
      n *= -1
    end

    if (d < 0)
      str += "-"
      d *= -1
    end

    if (q >= 0)
      str += q.to_s
      n = n % d
      if (n > 0)
        str += "."
      end
    else
      str += "0."
    end

    i += str.length
    r_map = {}
    r_map[r] = i

    while (n > 0) do
      n *= 10
      q = n / d
      r = n % d

      if (q > 0)
        str += q.to_s
        n = r
      else
        str += "0"
      end

      if r_map.has_key?(r)
        len = i + 1 - r_map[r]
        repeating_str = str[r_map[r], len]

        return str[0, r_map[r]] + "(" + repeating_str + ")"
      else
        r_map[r] = i + 1
      end


      i += 1
    end

    return str
  end
end
