class ReversePolish
  OPERATORS = {
    "+" => lambda { | op1, op2 | op1 + op2 },
    "*" => lambda { | op1, op2 | op1 * op2 },
    "/" => lambda { | op1, op2 | op1 / op2 },
    "-" => lambda { | op1, op2 | op1 - op2 }
  }

  def initialize
    @stack = []
  end

  def operate(operator)
    operand1 = @stack.pop
    operand2 = @stack.pop

    result = OPERATORS[operator].call(operand2, operand1)

    @stack.push(result)
  end

  def compute(str)
    return 0 if str.empty?

    str.chars.each do | op |
      if OPERATORS.has_key?(op)
        operate(op)
      else
        @stack.push(op.to_i)
      end
    end

    @stack.pop
  end
end
