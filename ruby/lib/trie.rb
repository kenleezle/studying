class Trie
  class TrieNode
    attr_reader :children
    attr_accessor :ends_word

    def initialize
      # should be something lower level like an array indexed on character
      @children = Hash.new
    end

    def child(character)
      children[character]
    end

    def find_or_create_child(character)
      existing_child = child(character)
      return existing_child unless existing_child.nil?

      children[character] = TrieNode.new
    end

    def preorder_traversal(prefix, words)
      words.push(prefix) if ends_word

      children.each { | key, child |
        child.preorder_traversal(prefix + key, words)
      }

      return words
    end
  end

  attr_reader :words
  attr_reader :root

  def initialize(words)
    @root = TrieNode.new
    words.map(&method(:add))
  end

  def add(word)
    node = root
    word.chars.each do | char |
      node = node.find_or_create_child(char)
    end
    node.ends_word = true
  end

  def sort
    root.preorder_traversal("", [])
  end
end
