class MyQueue
  def initialize
    @enqueue_stack = []
    @dequeue_stack = []
  end

  def enqueue(e)
    @enqueue_stack.push(e)
  end

  def dequeue
    if @dequeue_stack.empty?
      pop_stack_push_stack(@enqueue_stack, @dequeue_stack)
    end
    @dequeue_stack.pop
  end

  private

  def pop_stack_push_stack(from_stack, to_stack)
    while from_stack.any?
      to_stack.push(from_stack.pop)
    end
  end
end
