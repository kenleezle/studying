require 'singleton'
require './lib/choose.rb'
class Dictionary
  include Singleton
  attr_accessor :words
  attr_accessor :a10n_2_word_count
  attr_accessor :word_2_a10nableword
  def load_a10n_2_word_count
  end
  def load_word_2_a10nableword
  end
end
class A10nableWord
  attr_accessor :word
  attr_accessor :a10ns
  def initialize(word)
    @word = word
    load_all_a10ns
  end
  def shortest_unique_a10n
  end
  private
  def load_all_a10ns
    @a10ns = []
    n = word.length
    (0..n).each do | k |
      @a10ns.concat(choose(n,k)
        .collect{|indices| A10n.new(word,indices)}
        .select{|a10n| a10n.valid?})
    end
  end
  class A10n
    attr_accessor :word
    attr_accessor :letter_indices
    def initialize(word,letter_indices)
      @word = word
      @letter_indices = letter_indices
      @letter_indices.each_index do | i |
        @letter_indices[i] -= 1
      end
    end
    def to_s
      retval = ""
      i = 0
      j = 0
      count = 0
      while i != word.length do
        if i == letter_indices[j]
          retval += count.to_s if count > 0
          retval += word[i]
          j += 1
          count = -1
        end
        count += 1
        i += 1
      end
      retval += count.to_s unless letter_indices.last == word.length - 1
      return retval
    end
    def valid?
      return false if to_s.include?("1")
      !to_s.include?("0")
    end
  end
end
