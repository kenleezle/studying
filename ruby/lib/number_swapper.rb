class NumberSwapper
  def swap(nums)
    1.upto(32) do |i|
      mask = 1 << i
      left = (nums[0] & mask) == 1
      right = (nums[1] & mask) == 1

      if left != right
        if left
          nums[0] &= ~mask
          nums[1] |= mask
        else
          nums[0] |= mask
          nums[1] &= ~mask
        end
      end
    end
    nums
  end
end
