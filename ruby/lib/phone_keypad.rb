class PhoneKeypad
  DIGIT_TO_ALPHAS = {
    "2" => %W(a b c),
    "5" => %W(j k l),
    "9" => %W(w x y z)
  }
  def alphaReps(digits)
    if digits.empty?
      return [""]
    end

    firstDigit = digits[0]

    rest = ""
    if (digits.length > 1)
      rest = digits[1,digits.length - 1]
    end

    DIGIT_TO_ALPHAS[firstDigit].map do | alpha |
      alphaReps(rest).map do | partial_combo |
        alpha + partial_combo
      end
    end.flatten
  end
end
