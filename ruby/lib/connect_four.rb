class ConnectFour
  CELL_STATE_EMPTY = 0
  CELL_STATE_PLAYER_1 = 1
  CELL_STATE_PLAYER_2 = 2

  attr_reader :is_player_ones_turn

  # TODO
  # use line implementation with slope and y-intercept
  # for figuring out diagonal connect-4's
  # slope 1 and -1
  #

  def initialize(column_count = 7, row_count = 6)
    @column_count = column_count
    @row_count = row_count

    @is_player_ones_turn = true

    @board = []
    @column_count.times do
      @board.push([])
    end
    clear_board!
  end

  def change_turns
    @is_player_ones_turn = !@is_player_ones_turn 
  end

  def clear_board!
    0.upto(@column_count - 1) do | i |
      0.upto(@row_count - 1) do | j |
        @board[i][j] = CELL_STATE_EMPTY
      end
    end
  end

  def to_s
    result = ""
    (@row_count - 1).downto(0) do | j |
      0.upto(@column_count - 1) do | i |
        result += "#{@board[i][j]} "
      end
      result += "\n"
    end
    result
  end
  
  def pieces_in_column(column)
    @board[column].select { | cell | cell != CELL_STATE_EMPTY }.count
  end

  def column_is_full(column)
    pieces_in_column(column) == @row_count
  end

  def current_player
    @is_player_ones_turn ? CELL_STATE_PLAYER_1 : CELL_STATE_PLAYER_2
  end

  def drop_piece(column)
    return false if column_is_full(column)

    @board[column][pieces_in_column(column)] = current_player

    change_turns
    true
  end
end
