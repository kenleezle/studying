class Array
  def insertionSortDesc!
    1.upto(self.length()-1) do | i |
      val = self[i]
      j = i - 1
      while(j >= 0 && self[j] < val) do
          self[j+1] = self[j]
          j = j - 1
      end
      self[j+1] = val
    end
    self
  end
  def insertionSortAsc!
    1.upto(self.length()-1) do | i |
      val = self[i]
      j = i - 1
      while(j >= 0 && self[j] > val) do
          self[j+1] = self[j]
          j = j - 1
      end
      self[j+1] = val
    end
    self
  end
  def selectionSort!
    return self if self.length() == 0
    return self if self.length() == 1
    0.upto(self.length()-2) do | i |
      smallest = self[i]
      smallest_index = i
      (i+1).upto(self.length()-1) do | j |
        if self[j] < smallest
          smallest = self[j]
          smallest_index = j
        end
      end
      tmp = self[i]
      self[i] = smallest
      self[smallest_index] = tmp
    end
    self
  end
  # this operation is recursive
  # it is O(n*log_2(n))
  def mergeSort
    return self if self.length() == 0
    return self if self.length() == 1
    left = self[0...self.length()/2].mergeSort
    right = self[self.length()/2...self.length()].mergeSort
    return self.class.merge_two_sorted_arrays(left, right)
  end

  # this operation is O(n)
  # where n is the length of the longer of the two arrays
  def self.merge_two_sorted_arrays(arr1, arr2)
    i = 0
    j = 0
    result = self.new
    while i < arr1.length && j < arr2.length do
      if arr1[i] < arr2[j]
        result.push(arr1[i])
        i += 1
      else
        result.push(arr2[j])
        j += 1
      end
    end
    (i...arr1.length).each { | i | result.push(arr1[i]) }
    (j...arr2.length).each { | j | result.push(arr2[j]) }
    result
  end
  def quickSort!
    Array.quickSort!(self, 0, self.length-1)
  end
  def self.quickSort!(arr, low, high)
    return if (low > high)
    pivotIndex = quickSortPartition!(arr, low, high, high)
    quickSort!(arr, low, pivotIndex)
    quickSort!(arr, pivotIndex+1, high)
  end
  def self.quickSortPartition!(arr, low, high, pivotIndex)
  end
  def isSortedDesc?
    return true if self.length() == 1
    0.upto(self.length()-2) do | i |
      return false if self[i] < self[i+1]
    end
    return true
  end
  def isSortedAsc?
    return true if self.length() == 1
    0.upto(self.length()-2) do | i |
      return false if self[i] > self[i+1]
    end
    return true
  end
end
