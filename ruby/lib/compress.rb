class Compress

  def compress(str)
    dup_char = nil
    dup_count = 0

    compressed_str = ""

    str.chars.each do | char |

      if dup_char == char
        dup_count += 1
      else
        if dup_count == 2
          # a2 is same length as aa and thus dumb
          compressed_str += dup_char
        elsif dup_count > 2
          compressed_str += dup_count.to_s
        end
        compressed_str += char

        dup_char = char
        dup_count = 1
      end
    end

    if dup_count == 2
      # a2 is same length as aa and thus dumb
      compressed_str += dup_char
    elsif dup_count > 2
      compressed_str += dup_count.to_s
    end

    return compressed_str
  end
end
