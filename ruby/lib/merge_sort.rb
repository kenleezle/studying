class MergeSort
  def self.sort(list)
    r_sort(list, 0, list.size)
  end

  private

  def self.r_sort(list, lo, hi)
    size = hi - lo
    return list if size < 2

    m = lo + size/2
    r_sort(list, lo, m)
    r_sort(list, m, hi)
    merge(list, lo, m, hi)
  end

  def self.merge(list, lo, m, hi)
    li = lo
    lhi = ri = m
    rhi = hi

    tmp = []

    while (li < lhi && ri < rhi)
      if (list[li] <= list[ri])
        tmp.push(list[li])
        li += 1
      else
        tmp.push(list[ri])
        ri += 1
      end
    end

    while (li < lhi)
      tmp.push(list[li])
      li += 1
    end

    while (ri < rhi)
      tmp.push(list[ri])
      ri += 1
    end

    tmp.each_index do |i|
      list[lo+i] = tmp[i]
    end

    return list
  end
end
