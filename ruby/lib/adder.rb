class Adder
  def add(a, b)

    result = 0
    carry = 0
    mask = 1

    0.upto(31) do
      ma = mask & a
      mb = mask & b

      curr = ma ^ mb ^ carry
      carry = ((ma & mb) | (ma & carry) | (mb & carry))
      result |= curr

      carry = carry << 1
      mask = mask << 1
    end

    return result
  end
end
