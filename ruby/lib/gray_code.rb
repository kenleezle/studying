class GrayCode
  def gray_codes(n)
    return [] if (n == 0)
    return [0, 1] if (n == 1)

    codes = []
    sub_codes = gray_codes(n - 1)

    codes += sub_codes

    mask = 1 << (n - 1)

    codes += sub_codes.reverse.map { | code |
      code | mask
    }

    return codes
  end

  def slow_gray_codes
    set = Hash.new
    codes = Array.new

    my_slow_gray_codes(0, n, codes, set)

    return codes
  end


  def codes_diff_by_1(curr, n)
    (0...n).to_a.map { | i | curr ^ (1 << i) }
  end

  private

  def my_gray_codes(n)
  end

  def my_slow_gray_codes(curr, n, codes, set)
    return true if n == 0

    codes.push(curr)
    set[curr] = true

    return true if 2**n == codes.length

    candidates_for_next_code = codes_diff_by_1(curr, n).reject do | code |
      set.has_key? code
    end

    candidates_for_next_code.each do | candidate_for_next_code |
      found_all = my_slow_gray_codes(candidate_for_next_code, n, codes, set)
      return true if found_all
    end

    codes.pop
    set.delete(curr)
    return false
  end
end
