class Subset
  def all_subsets(set)
    result = [ [] ]
    stack = set.sort.reverse # ascending
    return result + my_all_subsets(stack)
  end

  private

  def my_all_subsets(stack)

    if stack.empty?
      return []
    end

    first = stack.pop
    result = [[first]]

    subsets = my_all_subsets(stack)

    result += subsets.map { | subset | [first] + subset }
    result += subsets.map { | subset | subset }

    return result
  end
end
