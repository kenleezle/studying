class Multiply
  def multiply(a, b)
    return b if (a == 1)
    return a if (b == 1)

    a_is_odd = (a & 1) == 1
    k = a >> 1

    result = multiply(k, b) << 1
    result += b if a_is_odd

    return result
  end
end
