class GuardGame

  OPEN = "O"
  WALL = "W"
  GUARD = "G"
  VISITED = "V"

  VISITED_BY_GUARD_1 = VISITED + "1"
  VISITED_BY_GUARD_2 = VISITED + "2"

  DEBUG = false

  def debug(str)
    return unless DEBUG
    puts str
  end

  def initialize(size = 3)
    @size = size
    @board = Array.new(size)
    @board.each_index do | i |
      @board[i] = Array.new(size)
    end
    clear
  end

  def clear
    @board.each_index do | i |
      @board[i].each_index do | j |
        @board[i][j] = OPEN
      end
    end
  end

  def rows
    (0..@size-1)
  end

  def cols
    rows # its a square
  end

  def positions_of_guards
    positions = Array.new
    cols.each do | i |
      rows.each do | j |
        positions.push([i,j]) if is_guard_at(i, j)
      end
    end
    return positions
  end

  def to_s
    retval = ""
    rows.reverse_each do | j |
      retval += "[ "
      cols.each do | i |
        retval += " #{@board[i][j]} "
      end
      retval += " ]\n"
    end
    retval
  end

  def is_on_board(i, j)
    i >= 0 && i < @size && j >= 0 && j < @size
  end

  def is_guard_at(i, j)
    @board[i][j] == GUARD
  end

  def set_guard_at(i, j)
    @board[i][j] = GUARD
  end

  def is_wall_at(i, j)
    @board[i][j] == WALL
  end

  def set_wall_at(i, j)
    @board[i][j] = WALL
  end

  def is_open_at(i, j)
    @board[i][j] == OPEN
  end

  def is_accessible_at(i, j, guard)
    return false unless is_on_board(i, j)

    is_guard_at(i, j) || is_visited_at(i, j, guard) || is_open_at(i, j)
  end

  def accessible_from_for_guard(i, j, guard)
    other_guard = guard == 1 ? 2 : 1

    possible = [
                [i+1, j, other_guard],
                [i-1, j, other_guard],
                [i, j+1, other_guard],
                [i, j-1, other_guard]
               ]

    possible.select { | poss | is_accessible_at(*poss) }
  end

  def length_of_shortest_route_between_guards
    length = my_length_of_shortest_route_between_guards
    clear_visited
    return length
  end

  private

  def is_visited_at(i, j, guard = "")
    @board[i][j].start_with?(VISITED + guard.to_s)
  end

  def set_visited_at(i, j, guard)
    @board[i][j] = VISITED + guard.to_s
  end

  def clear_visited
    @board.each_index do | i |
      @board[i].each_index do | j |
        @board[i][j] = OPEN if is_visited_at(i, j)
      end
    end
  end

  def my_length_of_shortest_route_between_guards

    guard_positions = positions_of_guards
    return -1 unless guard_positions.length == 2

    guard1_start = guard_positions[0]
    guard2_start = guard_positions[1]

    guard1_steps = 0
    guard2_steps = 0

    next_steps_for_guard1 = accessible_from_for_guard(*guard1_start, 1)
    next_steps_for_guard2 = accessible_from_for_guard(*guard2_start, 2)

    while next_steps_for_guard1.any? && next_steps_for_guard2.any?

      debug "*****************"
      debug "     PRE G1      "
      debug "*****************"
      debug to_s
      debug "*****************"

      next_next_steps = Array.new
      guard1_steps += 1
      next_steps_for_guard1.each do | i, j |
        next if guard1_start == [i, j]
        return guard1_steps if is_guard_at(i, j)
        if is_visited_at(i, j, 2)
          return guard1_steps + guard2_steps
        end

        set_visited_at(i, j, 1)
        debug "G1: accessible from (#{i}, #{j}): #{accessible_from_for_guard(i, j, 1)}"

        next_next_steps += accessible_from_for_guard(i, j, 1)
      end
      next_steps_for_guard1 = next_next_steps

      debug "*****************"
      debug "     PRE G2      "
      debug "*****************"
      debug to_s
      debug "*****************"

      next_next_steps = Array.new
      guard2_steps += 1
      next_steps_for_guard2.each do | i, j |
        next if guard2_start == [i, j]
        return guard2_steps if is_guard_at(i, j)
        if is_visited_at(i, j, 2)
          return guard1_steps + guard2_steps
        end

        set_visited_at(i, j, 2)
        debug "G2: accessible from (#{i}, #{j}): #{accessible_from_for_guard(i, j, 2)}"
        next_next_steps += accessible_from_for_guard(i, j, 2)
      end
      next_steps_for_guard2 = next_next_steps
    end

    debug "*****************"
    debug to_s
    debug "*****************"

    return -1
  end

end
=begin
puts "initializing game"
g = Game.new
puts g.to_s

puts "setting guard at 0,0"
g.set_guard_at(0,0)
puts g.to_s

puts "setting guard at 2,0"
g.set_guard_at(2,1)
puts g.to_s

puts "positions of guards:"
puts "#{g.positions_of_guards}"

puts "length of shortest path:"
puts g.length_of_shortest_route_between_guards

puts "setting wall"
g.set_wall_at(1, 0)
puts g.to_s

puts "length of shortest path:"
puts g.length_of_shortest_route_between_guards

puts "setting wall"
g.set_wall_at(1,1)
puts g.to_s

puts "length of shortest path:"
puts g.length_of_shortest_route_between_guards
=end
