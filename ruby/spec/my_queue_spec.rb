require "./lib/my_queue.rb"

describe MyQueue do
  let (:myq) { MyQueue.new }

  it "works" do
    expect(myq.dequeue).to be_nil

    myq.enqueue(5)

    expect(myq.dequeue).to eq(5)
    expect(myq.dequeue).to be_nil

    myq.enqueue(5)
    myq.enqueue(6)

    expect(myq.dequeue).to eq(5)

    myq.enqueue(7)
    myq.enqueue(8)

    expect(myq.dequeue).to eq(6)
    expect(myq.dequeue).to eq(7)
    expect(myq.dequeue).to eq(8)
    expect(myq.dequeue).to be_nil
  end
end
