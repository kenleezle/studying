require "./lib/group_anagrams.rb"

describe GroupAnagrams do
  let (:lib) { GroupAnagrams.new }
  it "works for trivial cases" do
    expect(lib.group_anagrams([])).to match_array([])
    expect(lib.group_anagrams(["a"])).to match_array([[1]])
    expect(lib.group_anagrams(["a", "b", "c"])).to match_array([[1],[2],[3]])
  end

  it "works for more interesting cases" do
    expect(lib.group_anagrams(%W(cat dog god tca))).to match_array([[1,4],[2,3]])

  end
end
