require "./lib/gray_code.rb"

describe GrayCode do

  let (:lib) { GrayCode.new }

  it "produces codes_diff_by_1" do
    expect(lib.codes_diff_by_1(0, 1)).to match_array([1])
    expect(lib.codes_diff_by_1(0, 2)).to match_array([1,2])
    expect(lib.codes_diff_by_1(1, 2)).to match_array([0,3])
  end

  it "works for trivial cases" do
    expect(lib.gray_codes(0)).to match_array([])
    expect(lib.gray_codes(1)).to match_array([0,1])
  end
  it "works for n = 2" do
    expect(lib.gray_codes(2)).to match_array([0,1,3,2])
  end
end
