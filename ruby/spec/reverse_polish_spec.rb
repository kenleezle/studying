require "./lib/reverse_polish.rb"

describe ReversePolish do

  let (:rp) { ReversePolish.new }

  it "can handle trivial cases" do
    expect(rp.compute("")).to equal(0)
    expect(rp.compute("1")).to equal(1)
  end

  it "divides and subtracts case where operand order matters" do
    expect(rp.compute("42/")).to equal(2)
    expect(rp.compute("52-")).to equal(3)
  end

  it "adds and multiplies" do
    expect(rp.compute("12+")).to equal(3)
    expect(rp.compute("52*")).to equal(10)
  end

  it "handles long complex nested expressions" do
    expect(rp.compute("11111++++")).to equal(5)
    expect(rp.compute("211111++++*")).to equal(10)
    expect(rp.compute("46+211111++++*+")).to equal(20)
  end
end

