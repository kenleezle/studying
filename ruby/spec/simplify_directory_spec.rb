require "./lib/simplify_directory.rb"

describe SimplifyDirectory do
  let (:sd) { SimplifyDirectory.new }

  it "returns root for trivial cases" do
    expect(sd.simplify("/")).to eq("/")
  end

  it "removes trailing slashes" do
    expect(sd.simplify("/a/")).to eq("/a")
    expect(sd.simplify("/a/./")).to eq("/a")
  end

  it "removes /./ components" do
    expect(sd.simplify("/./a/./b/./c/./")).to eq("/a/b/c")
  end


  it "simplifies ..'s" do
    expect(sd.simplify("/a/..")).to eq("/")
    expect(sd.simplify("/a/../")).to eq("/")
  end

  it "handles back to back slashes" do
    expect(sd.simplify("/a//b")).to eq("/a/b")
  end

  it "handles extra ..'s" do
    expect(sd.simplify("/a/../../../b")).to eq("/b")
  end
end

