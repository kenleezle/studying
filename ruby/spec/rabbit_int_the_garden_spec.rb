require "./lib/rabbit_in_the_garden_game.rb"

describe RabbitInTheGardenGame do
  let (:odd_matrix) { Array.new(3) { Array.new(3) { 0 } } }
  let (:odd_garden) { Garden.new(odd_matrix) }
  let (:garden) { odd_garden }
  let (:rabbit) { Rabbit.new(garden) }

  it "odd garden has exact center" do
    garden = odd_garden
    expect(garden.has_exact_center?).to be_truthy
  end

  it "coords_accessible_from" do
    garden = odd_garden
    expect(garden.coords_accessible_from(1,1).size).to eq(4)
    expect(garden.coords_accessible_from(0,0).size).to eq(2)
    expect(garden.coords_accessible_from(1,0).size).to eq(3)
  end

  it "coords_with_most_carrots" do
    garden = odd_garden
    expect(garden.coord_with_most_carrots([[0,0],[1,1]])).to be_nil

    garden.set_carrots_at_coord(0,0,5)
    expect(garden.coord_with_most_carrots([[0,0],[1,1]])).to eq([0,0])
  end

  it "eat_carrots_at_coord" do
    garden = odd_garden

    garden.set_carrots_at_coord(0,0,5)
    expect(garden.carrots_at_coord(0,0)).to eq(5)

    garden.eat_carrots_at_coord(0,0)
    expect(garden.carrots_at_coord(0,0)).to eq(0)
  end

  it "run_rabbit_run" do
    garden.set_carrots_at_coord(1,1,5)
    expect(rabbit.run_rabbit_run).to eq(5)

    garden.set_carrots_at_coord(1,1,6)
    garden.set_carrots_at_coord(2,1,5)
    garden.set_carrots_at_coord(0,1,4)
    expect(rabbit.run_rabbit_run).to eq(11)

    garden.set_carrots_at_coord(1,1,10)
    garden.set_carrots_at_coord(2,1,5)
    garden.set_carrots_at_coord(0,1,4)
    garden.set_carrots_at_coord(2,0,4)
    garden.set_carrots_at_coord(2,2,6)
    garden.set_carrots_at_coord(1,2,2)
    garden.set_carrots_at_coord(0,2,8)
    garden.set_carrots_at_coord(0,0,1)
    garden.set_carrots_at_coord(1,0,2)
    expect(rabbit.run_rabbit_run).to eq(42)
  end
end
