require './lib/adder.rb'
describe Adder do
  it "works for zero" do
    expect(Adder.new.add(0,0)).to eq(0)
  end
  it "works for some small positive numbers" do
    expect(Adder.new.add(0,1)).to eq(1)
    expect(Adder.new.add(1,0)).to eq(1)
    expect(Adder.new.add(1,1)).to eq(2)
  end
  it "works for some large positive numbers" do
    expect(Adder.new.add(128390,1287923)).to eq(1416313)
  end
end
