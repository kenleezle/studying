require "./lib/compress.rb"
describe Compress do
  let(:lib) { Compress.new }
  it "compresses" do
    expect(lib.compress("a")).to eq("a")
    expect(lib.compress("aa")).to eq("aa")
    expect(lib.compress("aaa")).to eq("a3")
    expect(lib.compress("aabcccccaaa")).to eq("aabc5a3")
  end
end
