require "./lib/merge_sort.rb"
describe MergeSort do
  it "sorts an unsorted array" do
    expect(MergeSort.sort([3,2,1])).to match_array([1,2,3])
  end

  it "leaves a sorted array sorted" do
    expect(MergeSort.sort([1,2,3])).to match_array([1,2,3])
  end

  it "sorts an array of length 2" do
    expect(MergeSort.sort([1,2])).to match_array([1,2])
    expect(MergeSort.sort([2,1])).to match_array([2,1])
    expect(MergeSort.sort([1,1])).to match_array([1,1])
  end

  it "doesn't croak on an array of length 1" do
    expect(MergeSort.sort([1])).to match_array([1])
  end

  it "doesn't croak on an array of length 0" do
    expect(MergeSort.sort([])).to be_empty
  end
end
