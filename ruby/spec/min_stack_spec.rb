require "./lib/min_stack.rb"

describe MinStack do

  let (:stack) { MinStack.new }

  it "can always return the top item" do
    stack.push(1)
    expect(stack.top).to equal(1)

    stack.push(2)
    expect(stack.top).to equal(2)

    stack.push(3)
    expect(stack.top).to equal(3)

    stack.pop
    expect(stack.top).to equal(2)

    stack.pop
    expect(stack.top).to equal(1)

    stack.pop
    expect(stack.top).to equal(-1)
  end

  it "can return the min item" do
    stack.push(1)
    expect(stack.getMin).to equal(1)

    stack.push(2)
    expect(stack.getMin).to equal(1)

    stack.push(3)
    expect(stack.getMin).to equal(1)

    stack.pop
    expect(stack.getMin).to equal(1)

    stack.pop
    expect(stack.getMin).to equal(1)

    stack.pop
    expect(stack.getMin).to equal(-1)
  end

  it "can handle trivial cases" do
    expect(stack.top).to equal(-1)
    expect(stack.getMin).to equal(-1)
  end
end

