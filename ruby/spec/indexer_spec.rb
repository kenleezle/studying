require './lib/indexer.rb'
describe Indexer do
  before do
    @indexer = Indexer.new("lib/example/input")
  end
  it "gets the list of files" do
    expect(@indexer.files.size).to eq 4
  end
  it "gets a list of words given a file" do
    expect(@indexer.words("1").size).to eq 6
  end
  it "map obj works" do
    expect(TermFrequency.new("fname").tf).to eq 0
    map_obj = TermFrequency.new("fname")
    map_obj.increment_tf
    expect(map_obj.tf).to eq 1
  end
  it "populates map" do
    @indexer.populateMap
    expect(@indexer.tf("bay","1")).to eq 1
  end
  it "gets the sorted words" do
    @indexer.populateMap
    expect(@indexer.sorted_words.first).to eq "bay"
  end
  it "can sort map objs by document name" do
    expect([TermFrequency.new("a"),TermFrequency.new("b")].sort.first.file).to eq "a"
    expect([TermFrequency.new("b"),TermFrequency.new("a")].sort.first.file).to eq "a"
  end
  it "list_of_docs_and_tfs_for_word" do
    @indexer.populateMap
    expect(@indexer.list_of_docs_and_tfs_for_word("coconut")).to eq "(2,1),(3,1),(4,1)"
  end
  it "produces the right output" do
    @indexer.populateMap
    expected_output = File.read("lib/example/output/index.txt")
    expect(@indexer.generate_output).to eq expected_output
  end
end
