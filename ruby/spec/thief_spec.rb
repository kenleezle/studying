require "./lib/thief.rb"

describe Thief do
  it "can handle trivial cases" do
    expect(Thief.max_booty([])).to equal(0)
    expect(Thief.choose_houses_to_rob([])).to match_array([])
    expect(Thief.max_booty([1])).to equal(1)
    expect(Thief.choose_houses_to_rob([1])).to match_array([0])
  end

  it "can handle zeros" do
    expect(Thief.max_booty([0])).to equal(0)
    expect(Thief.choose_houses_to_rob([0])).to match_array([])


    expect(Thief.max_booty([1,0,1,0,0,1])).to equal(3)
    expect(Thief.choose_houses_to_rob([1,0,1,0,0,1])).to match_array([0,2,5])

    expect(Thief.max_booty([1,0,0,0,0,1])).to equal(2)
    expect(Thief.choose_houses_to_rob([1,0,0,0,0,1])).to match_array([0,5])

    expect(Thief.max_booty([0,1,0,1,0])).to equal(2)
    expect(Thief.choose_houses_to_rob([0,1,0,1,0])).to match_array([1,3])

    expect(Thief.max_booty([6,5,4,3,2,1,0])).to equal(12)
    expect(Thief.choose_houses_to_rob([6,5,4,3,2,1,0])).to match_array([0,2,4])
  end

  it "can handle ascending numbers" do
    expect(Thief.max_booty([1,2])).to equal(2)
    expect(Thief.choose_houses_to_rob([1,2])).to match_array([1])
    expect(Thief.max_booty([1,2,3])).to equal(4)
    expect(Thief.choose_houses_to_rob([1,2,3])).to match_array([0,2])
    expect(Thief.max_booty([1,2,3,4])).to equal(6)
    expect(Thief.choose_houses_to_rob([1,2,3,4])).to match_array([1,3])
    expect(Thief.max_booty([1,2,3,4,5])).to equal(9)
    expect(Thief.choose_houses_to_rob([1,2,3,4,5])).to match_array([0,2,4])
    expect(Thief.max_booty([1,2,3,4,5,6])).to equal(12)
    expect(Thief.choose_houses_to_rob([1,2,3,4,5,6])).to match_array([1,3,5])
  end

  it "can handle descending numbers" do
    expect(Thief.max_booty([2,1])).to equal(2)
    expect(Thief.choose_houses_to_rob([2,1])).to match_array([0])

    expect(Thief.max_booty([3,2,1])).to equal(4)
    expect(Thief.choose_houses_to_rob([3,2,1])).to match_array([0,2])

    expect(Thief.max_booty([4,3,2,1])).to equal(6)
    expect(Thief.choose_houses_to_rob([4,3,2,1])).to match_array([0,2])

    expect(Thief.max_booty([5,4,3,2,1])).to equal(9)
    expect(Thief.choose_houses_to_rob([5,4,3,2,1])).to match_array([0,2,4])

    expect(Thief.max_booty([6,5,4,3,2,1])).to equal(12)
    expect(Thief.choose_houses_to_rob([6,5,4,3,2,1])).to match_array([0,2,4])
  end

  it "can handle alternating numbers" do
    expect(Thief.max_booty([100,1,1,100])).to equal(200)
    expect(Thief.choose_houses_to_rob([100,1,1,100])).to match_array([0,3])

    expect(Thief.max_booty([100,1,1,1,100])).to equal(201)
    expect(Thief.choose_houses_to_rob([100,1,1,1,100])).to match_array([0,2,4])
  end
end
