require './lib/sorting_algorithms.rb'
describe Array do
  desc_algorithms = [:insertionSortDesc!]
  asc_algorithms = [:insertionSortAsc!, :selectionSort!, :mergeSort]
  input_arrays = [
    [1,5,2,0,9,1],
    [],
    [9],
    [1,2,3,4,5],
    [5,4,3,2,1]
  ]

  desc_algorithms.each do | algorithm |
    input_arrays.each do |input_array|
      it "sorts #{input_array} desc using #{algorithm.to_s}" do
        expect(input_array.send(algorithm).isSortedDesc?).to eq(true)
      end
    end
  end

  asc_algorithms.each do | algorithm |
    input_arrays.each do |input_array|
      it "sorts #{input_array} asc using #{algorithm.to_s}" do
        expect(input_array.send(algorithm).isSortedAsc?).to eq(true)
      end
    end
  end
end
