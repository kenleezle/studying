require './lib/a10n.rb'
describe A10nableWord do
  before do
    @dictionary = Dictionary.instance()
    @dictionary.words = ["book","boot","took"]
  end
  it "converts abbreviations to strings" do
    a10n = A10nableWord::A10n.new("hello",[1,5])
    expect(a10n.to_s).to eq "h3o"

    a10n = A10nableWord::A10n.new("hello",[1])
    expect(a10n.to_s).to eq "h4"

    a10n = A10nableWord::A10n.new("hello",[5])
    expect(a10n.to_s).to eq "4o"

    a10n = A10nableWord::A10n.new("hello",[])
    expect(a10n.to_s).to eq "5"

    a10n = A10nableWord::A10n.new("hello",[1,2,3,4,5])
    expect(a10n.to_s).to eq "hello"
  end
  it "finds all possible a10ns for one word" do
    a10n = A10nableWord.new("book")
    a10ns = a10n.a10ns.collect &:to_s
    answer = ["book","4","b3","b2k","bo2","2ok","3k"]
    expect(a10ns).to match_array(answer)
  end
  it "finds the shortest unique a10n for one word" do
    a10n = A10nableWord.new("book")
    # expect(a10n.shortest_unique_a10n).to eq "b2k"
  end
end
