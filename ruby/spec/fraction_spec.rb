require "./lib/fraction.rb"

describe Fraction do
  let(:lib) { Fraction.new }
  it "works when there is no remainder" do
    expect(lib.fraction(100,20)).to eq("5")
  end

  it "works when there are weird negative numbers" do
    expect(lib.fraction(0,-1)).to eq("0")
    expect(lib.fraction(-1,2)).to eq("-0.5")
    expect(lib.fraction(1,-2)).to eq("-0.5")
  end

  it "works when there is no repeating" do
    expect(lib.fraction(1,5)).to eq("0.2")
    expect(lib.fraction(6,5)).to eq("1.2")
  end

  it "works when there is repeating" do
    expect(lib.fraction(1,3)).to eq("0.(3)")
    expect(lib.fraction(1,11)).to eq("0.(09)")
    expect(lib.fraction(1,12)).to eq("0.08(3)")
    expect(lib.fraction(5,12)).to eq("0.41(6)")
    expect(lib.fraction(87,22)).to eq("3.9(54)")
  end

end
