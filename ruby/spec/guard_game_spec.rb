require "./lib/guard_game.rb"

describe GuardGame do
  it "can find guards standing next to each other" do
    game = GuardGame.new
    game.set_guard_at(0,0)
    game.set_guard_at(1,0)
    expect(game.length_of_shortest_route_between_guards).to equal(1)

    game.clear
    game.set_guard_at(0,0)
    game.set_guard_at(0,1)
    expect(game.length_of_shortest_route_between_guards).to equal(1)
  end

  it "can find guards standing one step away from each other" do
    game = GuardGame.new
    game.set_guard_at(0,0)
    game.set_guard_at(2,0)
    expect(game.length_of_shortest_route_between_guards).to equal(2)

    game = GuardGame.new
    game.set_guard_at(0,0)
    game.set_guard_at(0,2)
    expect(game.length_of_shortest_route_between_guards).to equal(2)
  end

  it "can find paths around walls" do
    game = GuardGame.new
    game.set_guard_at(0,0)
    game.set_wall_at(1,0)
    game.set_guard_at(2,0)
    expect(game.length_of_shortest_route_between_guards).to equal(5)

    game.set_wall_at(1,1)
    expect(game.length_of_shortest_route_between_guards).to equal(7)
  end

  it "returns -1 when there is no path" do
    game = GuardGame.new
    game.set_guard_at(0,0)
    game.set_wall_at(1,0)
    game.set_wall_at(1,1)
    game.set_wall_at(1,2)
    game.set_guard_at(2,0)
    expect(game.length_of_shortest_route_between_guards).to equal(-1)
  end

  it "returns -1 when there is 0, 1 or >2 guards" do
    game = GuardGame.new
    expect(game.length_of_shortest_route_between_guards).to equal(-1)

    game.set_guard_at(0,0)
    expect(game.length_of_shortest_route_between_guards).to equal(-1)

    game.set_guard_at(0,1)
    game.set_guard_at(0,2)
    expect(game.length_of_shortest_route_between_guards).to equal(-1)
  end
end

