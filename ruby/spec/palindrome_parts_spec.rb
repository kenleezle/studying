require "./lib/palindrome_parts.rb"

describe PalindromeParts do
  let (:lib) { PalindromeParts.new }
  it "works for trivial cases" do
    expect(lib.parts("")).to match_array([ [] ])
    expect(lib.parts("a")).to match_array([ ["a"] ])
    expect(lib.parts("abc")).to match_array([ %W(a b c) ])
  end

  it "works for slightly more complicated cases" do
    expect(lib.parts("bab")).to match_array([ %W(b a b), %W(bab)  ])
    expect(lib.parts("aabc")).to match_array([ %W(a a b c), %W(aa b c)  ])
  end

  it "works for a mildly hairy case" do
    expect(lib.parts("bbaabb")).to match_array([
      %W(b b a a b b),
      %W(b b a a bb),
      %W(b b aa b b ),
      %W(b b aa bb),
      %W(b baab b),
      %W(bb a a b b),
      %W(bb a a bb),
      %W(bb aa b b ),
      %W(bb aa bb),
      %W(bbaabb)
    ])
  end

end
