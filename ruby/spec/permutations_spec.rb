require './lib/permutations.rb'
describe Permutations do
  let (:p) { Permutations.new }
  it "permutes" do
    expect(p.unique_permutations("abc").size).to eq(6)
    expect(p.unique_permutations("aac").size).to eq(3)
    expect(p.unique_permutations("aaa").size).to eq(1)
    expect(p.unique_permutations("aabc").size).to eq(12)
    expect(p.unique_permutations("aaaaaaa").size).to eq(1)
    expect(p.unique_permutations("abacadae").size).to eq(1680)
    expect(p.unique_permutations("abacadae").uniq.size).to eq(1680)
  end
end
