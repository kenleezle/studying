require "./lib/multiply.rb"
describe Multiply do
  let(:lib) { Multiply.new }
  it "multiplies" do
    expect(lib.multiply(1,1)).to eq(1)
    expect(lib.multiply(1,2)).to eq(2)
    expect(lib.multiply(1,3)).to eq(3)
    expect(lib.multiply(2,2)).to eq(4)
    expect(lib.multiply(4,4)).to eq(16)
    expect(lib.multiply(4,5)).to eq(20)
    expect(lib.multiply(1,5)).to eq(5)
    expect(lib.multiply(876,165)).to eq(144540)
  end
end
