require "./lib/trie.rb"

describe Trie do
  it "can sort 3 words" do
    trie = Trie.new(["abc","bcd", "def"])
    expect(trie.sort).to match_array(["abc", "bcd", "def"])
  end

  it "can sort substrings " do
    trie = Trie.new(["def", "abcd", "abc","bcd"])
    expect(trie.sort).to match_array(["abc", "abcd", "bcd", "def"])
  end
end
