require "./lib/phone_keypad.rb"

describe PhoneKeypad do
  let (:lib) { PhoneKeypad.new }
  it "handles empty digit strings" do
    expect(lib.alphaReps("")).to match_array([""])
  end
  it "handles single digits" do
    expect(lib.alphaReps("2")).to match_array(["a","b","c"])
  end
  it "handles two digits" do
    alpha_reps = %W(wj wk wl xj xk xl yj yk yl zj zk zl)
    expect(lib.alphaReps("95")).to match_array(alpha_reps)
  end
end
