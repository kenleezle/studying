require "./lib/connect_four.rb"
describe ConnectFour do
  let(:game) { ConnectFour.new(column_count, row_count) }

  let(:column_count) { 2 }
  let(:row_count) { 3 }

  it "clears the board" do
    expect(game.to_s).to eq("0 0 \n0 0 \n0 0 \n")
    0.upto(column_count - 1) do | i |
      expect(game.pieces_in_column(0)).to be 0
    end
  end

  it "stalemates the board" do
    expect(game.pieces_in_column(0)).to be 0
    expect(game.drop_piece(0)).to be true
    expect(game.pieces_in_column(0)).to be 1

    expect(game.pieces_in_column(1)).to be 0
    expect(game.drop_piece(1)).to be true
    expect(game.pieces_in_column(1)).to be 1

    expect(game.to_s).to eq("0 0 \n0 0 \n1 2 \n")

    expect(game.drop_piece(1)).to be true
    expect(game.drop_piece(0)).to be true
    expect(game.to_s).to eq("0 0 \n2 1 \n1 2 \n")

    expect(game.drop_piece(0)).to be true
    expect(game.drop_piece(1)).to be true

    expect(game.to_s).to eq("1 2 \n2 1 \n1 2 \n")
  end

  it "starts on player 1" do
    expect(game.is_player_ones_turn).to be true

    expect(game.drop_piece(0)).to be true
    expect(game.pieces_in_column(0)).to be 1
    expect(game.is_player_ones_turn).to be false

    expect(game.drop_piece(0)).to be true
    expect(game.pieces_in_column(0)).to be 2
    expect(game.is_player_ones_turn).to be true

    expect(game.drop_piece(0)).to be true
    expect(game.is_player_ones_turn).to be false

    expect(game.drop_piece(0)).to be false
    expect(game.is_player_ones_turn).to be false
  end
end
 
