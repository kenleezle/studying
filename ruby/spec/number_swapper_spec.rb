require "./lib/number_swapper.rb"

describe NumberSwapper do
  let (:lib) { NumberSwapper.new }
  it "works for trivial cases" do
    expect(lib.swap([5,2])).to match_array([2,5])
    expect(lib.swap([0,0])).to match_array([0,0])
    expect(lib.swap([10,10])).to match_array([10,10])
  end
  it "works for edge cases" do
    max = 2 ** 31
    min = max * -1
    expect(lib.swap([max, min])).to match_array([min, max])
  end
end
