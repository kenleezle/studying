require "./lib/subset.rb"

describe Subset do
  let (:lib) { Subset.new }

  it "works for empty sets" do
    expect(lib.all_subsets([])).to match_array([ [] ])
    expect(lib.all_subsets([0])).to match_array([ [], [0] ])
    expect(lib.all_subsets([0,1])).to match_array([ [], [0], [0,1], [1] ])
    expect(lib.all_subsets([0,1, 2])).to match_array([ [], [0], [0,1], [0,1,2], [0, 2], [1], [1, 2], [2] ])
  end
end
