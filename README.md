# Studying

[![Codacy Badge](https://api.codacy.com/project/badge/grade/7c596298c66f45d9b6be72abf19f0eab)](https://www.codacy.com)

## Introduction to Algorithms Solutions
* [selected solutions pdf](https://mitpress.mit.edu/sites/default/files/titles/content/Intro_to_Algo_Selected_Solutions.pdf)
* [solutions on github in chinese lol](https://github.com/gzc/CLRS)

## Algorithms to Commit to Memory

algorithm | LOC | locals | helper methods | minutes to write
--- | --- | --- | --- | ---
Binary Search | 27 | l, r, m_i, m_e | | 2 
Quick Sort | 35 | lo, hi, p, p_i | rSort, partition, swap | 5
Merge Sort | 48 | lo, hi, length, leftI, leftHi, rightI, rightHi, temp[] | rSort, merge | 6
Permutations | 
Combinations | 
Insertion Sort | 
Selection Sort | 
Depth First Tree Traversal | 
DFS Pre-Order | 
DFS In-Order | 
DFS Post-Order | 
Breadth First Tree Traversal / Level Order | 
Heap Sort |
Radix Sort |

## Synchronization Primitives

### Mutex

properties:
* boolean - locked?

interface:
* mutex.lock()
* mutex.unlock()
* mutex.synchronize(block) -> lock(), block.call(), unlock()

### Semaphore

properties:
* count
* queue of processes waiting

interface:
* semaphore.wait()
* semaphore.signal()

### Monitor

provides synchronized access to section of code
