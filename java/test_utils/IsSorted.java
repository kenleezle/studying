package test_utils;

import java.util.Arrays;
import java.util.List;

public class IsSorted {
  public static boolean isSorted(int[] a) {
      for (int i = 0; i < (a.length - 1); i++) {
        if (a[i] > a[i+1]) {
          return false;
        }
      }
      return true;
  }
  public static boolean isSorted(List<Integer> list) {
    for (int i = 0; i < list.size()-1; i++) {
      if (list.get(i) > list.get(i+1)) {
        return false;
      }
    }
    return true;
  }

  public static boolean haveSameLength(int [] array1, int[] array2) {
    return array1.length == array2.length;
  }
  public static boolean haveSameLength(List<Integer> list1, List<Integer> list2) {
    return list1.size() == list2.size();
  }

  public static boolean haveSameContents(int[] array1, int[] array2) {
    if (!haveSameLength(array1, array2)) {
      return false;
    }

    int[] sortedCopyOfArray1 = Arrays.copyOf(array1, array1.length);
    int[] sortedCopyOfArray2 = Arrays.copyOf(array2, array2.length);
    Arrays.sort(sortedCopyOfArray1);
    Arrays.sort(sortedCopyOfArray2);

    for (int i = 0; i < array1.length; i++) {
      if (sortedCopyOfArray1[i] != sortedCopyOfArray2[i]) {
        return false;
      }
    }
    return true;
  }

  public static Integer max(List<Integer> list) {
    if (list.isEmpty()) {
      return null;
    }

    Integer max = list.get(0);
    for(Integer possibleMax : list) {
      if (possibleMax > max) {
        max = possibleMax;
      }
    }
    return max;
  }
}
