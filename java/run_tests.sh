#!/bin/bash

source set_classpath.sh
while true; do
  OUTPUT=""
  OUTPUT=$OUTPUT"$(javac *.java)"
  OUTPUT=$OUTPUT"$(javac */*.java)"
  OUTPUT=$OUTPUT"$(java -ea org.junit.runner.JUnitCore JavaTest)"
  clear
  echo "${OUTPUT}"
  sleep 5
done
