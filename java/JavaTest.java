import java.util.Scanner;
import junit.framework.*;
import java.util.Arrays;
import static com.google.common.truth.Truth.*;
import java.util.List;
import java.util.Set;
import sorting.*;
import g_questions.*;
import algo_book.*;
import tree.*;
import test_utils.*;
import java.util.ArrayList;

public class JavaTest extends TestCase {
  protected int[][] unsortedIntArrays;
  protected int[][] sortedIntArrays;
  protected int[][] intArrays;

  protected List<List<Integer>> unsortedIntegerLists;
  protected List<List<Integer>> sortedIntegerLists;
  protected List<List<Integer>> integerLists;

  // assigning the values
  protected void setUp(){
    unsortedIntArrays = new int[][] {
      new int[] {7,1,2,3,4,5},
      new int[] {7,8,4},
      new int[] {7,1,2,100,2,3,43,8,4,5},
      new int[] {7,1,2,8,4,5},
      new int[] {6,1,10},
      new int[] {6,1,2,3,4,5},
      new int[] {6,0},
      new int[] {0,1,0},
      new int[] {0,1,2},
      new int[] {0,1,2,1,5},
      new int[] {0,0,0,0,0},
      new int[] {0,0,9,0,0,0}
    };
    sortedIntArrays = new int[][] {
      new int[] {},
      new int[] {0},
      new int[] {0,1},
      new int[] {0,1,2,3,4,5}
    };
    intArrays = new int[unsortedIntArrays.length + sortedIntArrays.length][];
    int i;
    for (i = 0; i < unsortedIntArrays.length; i++) {
      intArrays[i] = unsortedIntArrays[i];
    }
    for (int j = 0; j < sortedIntArrays.length; j++) {
      intArrays[i] = sortedIntArrays[j];
      i++;
    }
    setUpIntegerLists();
  }

  // Convert my int[][] arrays into List<List<Integer>>'s
  protected void setUpIntegerLists() {
    unsortedIntegerLists = new ArrayList<List<Integer>>(unsortedIntArrays.length);
    integerLists = new ArrayList<List<Integer>>(intArrays.length);
    for (int i = 0; i < unsortedIntArrays.length; i++) {
      unsortedIntegerLists.add(new ArrayList<Integer>(unsortedIntArrays[i].length));
      integerLists.add(new ArrayList<Integer>(unsortedIntArrays[i].length));
      for (int j = 0; j < unsortedIntArrays[i].length; j++) {
        unsortedIntegerLists.get(i).add(unsortedIntArrays[i][j]);
        integerLists.get(integerLists.size()-1).add(unsortedIntArrays[i][j]);
      }
    }
    sortedIntegerLists = new ArrayList<List<Integer>>(sortedIntArrays.length);
    for (int i = 0; i < sortedIntArrays.length; i++) {
      sortedIntegerLists.add(new ArrayList<Integer>(sortedIntArrays[i].length));
      integerLists.add(new ArrayList<Integer>(sortedIntArrays[i].length));
      for (int j = 0; j < sortedIntArrays[i].length; j++) {
        sortedIntegerLists.get(i).add(sortedIntArrays[i][j]);
        integerLists.get(integerLists.size()-1).add(sortedIntArrays[i][j]);
      }
    }

  }

  public void testIsSorted() {
    for (int i = 0; i < sortedIntArrays.length; i++) {
      assertTrue(IsSorted.isSorted(sortedIntArrays[i]));
    }
  }

  private void verifySortedArray(int[] unsortedArray, int[] sortedArray) {
    // Google Truth
    assertThat(IsSorted.isSorted(sortedArray)).isTrue();
    assertThat(IsSorted.haveSameLength(unsortedArray, sortedArray)).isTrue();
    assertThat(IsSorted.haveSameContents(unsortedArray, sortedArray)).isTrue();

    // Standard Junit
    assertTrue(IsSorted.isSorted(sortedArray));
    assertTrue(IsSorted.haveSameLength(unsortedArray, sortedArray));
    assertTrue(IsSorted.haveSameContents(unsortedArray, sortedArray));
  }

  private void verifySortedList(List<Integer> unsortedList, List<Integer> sortedList) {
    // Google Truth
    assertThat(IsSorted.isSorted(sortedList)).isTrue();
    assertThat(IsSorted.haveSameLength(unsortedList, sortedList)).isTrue();
    assertThat(sortedList).containsExactlyElementsIn(unsortedList);
  }

  public void testInsertionSort() {
    for (int i = 0; i < intArrays.length; i++) {
      int[] unsortedArray = intArrays[i];
      int[] sortedArray = InsertionSort.sort(Arrays.copyOf(unsortedArray, unsortedArray.length));
      verifySortedArray(unsortedArray, sortedArray);
    }
  }

  public void testInsertionSortList() {
    for (int i = 0; i < integerLists.size(); i++) {
      List<Integer> unsortedList = integerLists.get(i);
      List<Integer> sortedList = InsertionSort.sort(new ArrayList<Integer>(unsortedList));
      verifySortedList(unsortedList, sortedList);
    }
  }

  public void testSelectionSort() {
    for (int i = 0; i < intArrays.length; i++) {
      int[] unsortedArray = intArrays[i];
      int[] sortedArray = SelectionSort.sort(Arrays.copyOf(unsortedArray, unsortedArray.length));
      verifySortedArray(unsortedArray, sortedArray);
    }
  }

  public void testMergeSort() {
    for (int i = 0; i < intArrays.length; i++) {
      int[] unsortedArray = intArrays[i];
      int[] sortedArray = MergeSort.sort(Arrays.copyOf(unsortedArray, unsortedArray.length));
      verifySortedArray(unsortedArray, sortedArray);
    }
  }

  public void testQuickSort() {
    for (int i = 0; i < intArrays.length; i++) {
      int[] unsortedArray = intArrays[i];
      int[] sortedArray = QuickSort.sort(Arrays.copyOf(unsortedArray, unsortedArray.length));
      verifySortedArray(unsortedArray, sortedArray);
    }
  }

  public void testHeapSort() {
    for (List<Integer> list : integerLists) {
      ArrayList<Integer> unsortedList = new ArrayList<Integer>(list);
      Heap.sort(unsortedList);
      verifySortedList(unsortedList, unsortedList);
    }
  }

  public void testHeap() {
    Heap h = new Heap();
    h.push(10);
    assertThat(h.peek()).isEqualTo(10);
    assertThat(h.pop()).isEqualTo(10);
    assertThat(h.pop()).isEqualTo(null);

    h.push(10);
    h.push(20);
    assertThat(h.peek()).isEqualTo(20);

    h.push(25);
    assertThat(h.peek()).isEqualTo(25);

    h.push(5);
    assertThat(h.peek()).isEqualTo(25);

    h.push(20);
    assertThat(h.peek()).isEqualTo(25);

    h.push(50);
    assertThat(h.peek()).isEqualTo(50);

    assertThat(h.pop()).isEqualTo(50);
    assertThat(h.pop()).isEqualTo(25);
    assertThat(h.pop()).isEqualTo(20);
    assertThat(h.pop()).isEqualTo(20);
    assertThat(h.pop()).isEqualTo(10);
    assertThat(h.pop()).isEqualTo(5);
    assertThat(h.pop()).isEqualTo(null);
  }

  public void testMinHeap() {
    Heap h = new Heap(true);
    h.push(10);
    assertThat(h.peek()).isEqualTo(10);
    assertThat(h.pop()).isEqualTo(10);
    assertThat(h.pop()).isEqualTo(null);

    h.push(10);
    h.push(20);
    assertThat(h.peek()).isEqualTo(10);

    h.push(25);
    assertThat(h.peek()).isEqualTo(10);

    h.push(5);
    assertThat(h.peek()).isEqualTo(5);

    h.push(20);
    assertThat(h.peek()).isEqualTo(5);

    h.push(50);
    assertThat(h.peek()).isEqualTo(5);

    assertThat(h.pop()).isEqualTo(5);
    assertThat(h.pop()).isEqualTo(10);
    assertThat(h.pop()).isEqualTo(20);
    assertThat(h.pop()).isEqualTo(20);
    assertThat(h.pop()).isEqualTo(25);
    assertThat(h.pop()).isEqualTo(50);
    assertThat(h.pop()).isEqualTo(null);
  }

  public void testRecursiveHeapify() {
    for (List<Integer> list : integerLists) {
      if (list.isEmpty()) {
        continue;
      }

      Integer max = IsSorted.max(list);
      HeapSort.recursiveHeapify(list, 0, list.size());
      assertThat(max).isEqualTo(list.get(0));
    }
  }

  public void testRecursiveHeapSort() {
    for (List<Integer> list : integerLists) {
      List<Integer> unsortedList = new ArrayList<Integer>(list);
      List<Integer> sortedList = HeapSort.sort(unsortedList);
      verifySortedList(unsortedList, sortedList);
    }
  }

  public void testRadixSortDigitInSigPos() {
    assertThat(RadixSort.digitInSigPos(8913, 1)).isEqualTo(3);
    assertThat(RadixSort.digitInSigPos(8913, 2)).isEqualTo(1);
    assertThat(RadixSort.digitInSigPos(8913, 3)).isEqualTo(9);
    assertThat(RadixSort.digitInSigPos(8913, 4)).isEqualTo(8);
    assertThat(RadixSort.digitInSigPos(1, 1)).isEqualTo(1);
    assertThat(RadixSort.digitInSigPos(2, 1)).isEqualTo(2);
    assertThat(RadixSort.digitInSigPos(1, 5)).isEqualTo(0);
  }

  public void testRadixSortSigDigForItem() {
    assertThat(RadixSort.sigDigForItem(8000)).isEqualTo(4);
    assertThat(RadixSort.sigDigForItem(721)).isEqualTo(3);
    assertThat(RadixSort.sigDigForItem(1)).isEqualTo(1);
    assertThat(RadixSort.sigDigForItem(5)).isEqualTo(1);
  }

  public void testRadixSort() {
    for (List<Integer> list : integerLists) {
      List<Integer> unsortedList = new ArrayList<Integer>(list);
      List<Integer> sortedList = RadixSort.sort(list);
      verifySortedList(unsortedList, sortedList);
    }
  }

  public void testQuickSortMedia() {
    for (List<Integer> list : integerLists) {
      if (list.isEmpty()) {
        continue;
      }

      Integer median = QuickSortMedian.median(list);

      List<Integer> unsortedList = new ArrayList<Integer>(list);
      List<Integer> sortedList = HeapSort.sort(unsortedList);
      Integer heapSortMedian = sortedList.get(sortedList.size()/2);

      assertThat(median).isEqualTo(heapSortMedian);
    }
  }

  public void testSortWordByChar() {
    assertThat(WordWithFriend.sortWordByChar("dcba")).isEqualTo("abcd");
    assertThat(WordWithFriend.sortWordByChar("abcd")).isEqualTo("abcd");
    assertThat(WordWithFriend.sortWordByChar("")).isEqualTo("");
  }

  public void testAnagram() {
    List<String> words = new ArrayList<String>();
    words.add("abc");
    words.add("cba");
    words.add("gogo");
    words.add("ogog");
    words.add("blip");

    List<List<String>> groupedAnagrams = Anagram.groupAnagrams(words);
    assertThat(groupedAnagrams.get(0)).containsExactly("abc", "cba");
    assertThat(groupedAnagrams.get(1)).containsExactly("blip");
    assertThat(groupedAnagrams.get(2)).containsExactly("gogo", "ogog");
  }

  public void testAnagramUsingHash() {
    List<String> words = new ArrayList<String>();
    words.add("abc");
    words.add("cba");
    words.add("gogo");
    words.add("ogog");
    words.add("blip");

    List<List<String>> groupedAnagrams = Anagram.groupAnagramsUsingHash(words);
    assertThat(groupedAnagrams.get(0)).containsExactly("abc", "cba");
    assertThat(groupedAnagrams.get(1)).containsExactly("gogo", "ogog");
    assertThat(groupedAnagrams.get(2)).containsExactly("blip");
  }

  public void testBTreeDCA() {
    Node root = new Node();
    root.leftChild = new Node();
    root.rightChild = new Node();

    assertThat(DCA.findDCA(root, root, root.leftChild)).isEqualTo(root);
    assertThat(DCA.findDCA(root, root.rightChild, root.leftChild)).isEqualTo(root);

    Node element1 = root.rightChild.leftChild = new Node();
    Node element2 = root.rightChild.rightChild = new Node();

    assertThat(DCA.findDCA(root, element1, element2)).isEqualTo(root.rightChild);
    assertThat(DCA.findDCA(root, root.rightChild, element2)).isEqualTo(root.rightChild);
    assertThat(DCA.findDCA(root, element1, root.rightChild)).isEqualTo(root.rightChild);
    assertThat(DCA.findDCA(root, element1, root.leftChild)).isEqualTo(root);
  }

  public void testBTreeDCALocalReturn() {
    Node root = new Node();
    root.leftChild = new Node();
    root.rightChild = new Node();

    assertThat(DCA.findDCAWithLocalReturn(root, root, root.leftChild)).isEqualTo(root);
    assertThat(DCA.findDCAWithLocalReturn(root, root.rightChild, root.leftChild)).isEqualTo(root);

    Node element1 = root.rightChild.leftChild = new Node();
    Node element2 = root.rightChild.rightChild = new Node();

    assertThat(DCA.findDCAWithLocalReturn(root, element1, element2)).isEqualTo(root.rightChild);
    assertThat(DCA.findDCAWithLocalReturn(root, root.rightChild, element2)).isEqualTo(root.rightChild);
    assertThat(DCA.findDCAWithLocalReturn(root, element1, root.rightChild)).isEqualTo(root.rightChild);
    assertThat(DCA.findDCAWithLocalReturn(root, element1, root.leftChild)).isEqualTo(root);
  }

  public void testDCAWithParent() {
    Node root = new Node();
    assertThat(DCAWithParent.dca(root, root)).isEqualTo(root);

    Node other = new Node();
    assertThat(DCAWithParent.dca(root, other)).isNull();

    root.leftChild = new Node();
    root.leftChild.parent = root;
    root.rightChild = new Node();
    root.rightChild.parent = root;
    assertThat(DCAWithParent.dca(root, root.rightChild)).isEqualTo(root);
    assertThat(DCAWithParent.dca(root.leftChild, root.rightChild)).isEqualTo(root);

    assertThat(DCAWithParent.depth(root)).isEqualTo(0);
    assertThat(DCAWithParent.depth(root.rightChild)).isEqualTo(1);
    assertThat(DCAWithParent.isParent(root.rightChild, root)).isTrue();

  }

  public void testLeftMostNode() {
    Node root = new Node();
    assertThat(NextNodeInOrder.leftMostNode(root)).isEqualTo(root);

    root.rightChild = new Node();
    assertThat(NextNodeInOrder.leftMostNode(root)).isEqualTo(root);

    root.leftChild = new Node();
    assertThat(NextNodeInOrder.leftMostNode(root)).isEqualTo(root.leftChild);

    root.leftChild.rightChild = new Node();
    assertThat(NextNodeInOrder.leftMostNode(root)).isEqualTo(root.leftChild);
  }

  public void testFirstAncestorOnHisLeftSide() {
    Node node = new Node();
    assertThat(NextNodeInOrder.firstAncestorOnHisLeftSide(node)).isEqualTo(null);

    node.parent = new Node();
    node.parent.rightChild = node;
    assertThat(NextNodeInOrder.firstAncestorOnHisLeftSide(node)).isEqualTo(null);

    node.parent.rightChild = null;
    node.parent.leftChild = node;
    assertThat(NextNodeInOrder.firstAncestorOnHisLeftSide(node)).isEqualTo(node.parent);

    node.parent.rightChild = node;
    node.parent.leftChild = null;
    node.parent.parent = new Node();
    node.parent.parent.leftChild = node.parent;
    assertThat(NextNodeInOrder.firstAncestorOnHisLeftSide(node)).isEqualTo(node.parent.parent);
  }

  public void testNextNodeInOrder() {
    Node root = new Node();
    assertThat(NextNodeInOrder.nextNode(root)).isEqualTo(null);

    root.rightChild = new Node();
    root.rightChild.parent = root;
    assertThat(NextNodeInOrder.nextNode(root)).isEqualTo(root.rightChild);

    root.leftChild = new Node();
    root.leftChild.parent = root;
    assertThat(NextNodeInOrder.nextNode(root.leftChild)).isEqualTo(root);

    root.leftChild.rightChild = new Node();
    root.leftChild.rightChild.parent = root.leftChild;
    assertThat(NextNodeInOrder.nextNode(root.leftChild.rightChild)).isEqualTo(root);
  }

  public void testPermutations() {
    if (Permutations.debug) {
      System.out.println();
      System.out.println("********************************");
      System.out.println("testing permutations");
      System.out.println("********************************");
    }

    List<Integer> list = new ArrayList<Integer>();
    assertThat(Permutations.permuteList(list)).containsExactly(list);

    list.add(1);
    assertThat(Permutations.permuteList(list)).containsExactly(list);

    list.add(2);
    List<List<Integer>> results = new ArrayList<List<Integer>>();
    results.add(Arrays.asList(1,2));
    results.add(Arrays.asList(2,1));
    assertThat(Permutations.permuteList(list)).containsExactlyElementsIn(results);

    list.add(3);
    results.clear();
    results.add(Arrays.asList(1,2,3));
    results.add(Arrays.asList(1,3,2));
    results.add(Arrays.asList(2,1,3));
    results.add(Arrays.asList(2,3,1));
    results.add(Arrays.asList(3,1,2));
    results.add(Arrays.asList(3,2,1));
    assertThat(Permutations.permuteList(list)).containsExactlyElementsIn(results);

    list.add(4);
    results.clear();
    results.add(Arrays.asList(1,2,3,4));
    results.add(Arrays.asList(4,3,2,1));
    assertThat(Permutations.permuteList(list)).containsAllIn(results);
    assertThat(Permutations.permuteList(list).size()).isEqualTo(24);

    list.add(5);
    results.clear();
    results.add(Arrays.asList(1,2,3,4,5));
    results.add(Arrays.asList(5,4,3,2,1));
    assertThat(Permutations.permuteList(list)).containsAllIn(results);
    assertThat(Permutations.permuteList(list).size()).isEqualTo(120);

    list.add(6);
    results.clear();
    results.add(Arrays.asList(1,2,3,4,5,6));
    results.add(Arrays.asList(6,5,4,3,2,1));
    assertThat(Permutations.permuteList(list)).containsAllIn(results);
    assertThat(Permutations.permuteList(list).size()).isEqualTo(720);

    list.add(7);
    results.clear();
    results.add(Arrays.asList(1,2,3,4,5,6,7));
    results.add(Arrays.asList(7,6,5,4,3,2,1));
    assertThat(Permutations.permuteList(list)).containsAllIn(results);
    assertThat(Permutations.permuteList(list).size()).isEqualTo(5040);
  }

  public void testFasterPermutations() {
    if (FasterPermutations.debug) {
      System.out.println();
      System.out.println("********************************");
      System.out.println("testing faster permutations");
      System.out.println("********************************");
    }
    List<Integer> list = new ArrayList<Integer>();
    assertThat(FasterPermutations.permuteList(list)).containsExactly(list);

    list.add(1);
    assertThat(FasterPermutations.permuteList(list)).containsExactly(list);

    list.add(2);
    List<List<Integer>> results = new ArrayList<List<Integer>>();
    results.add(Arrays.asList(1,2));
    results.add(Arrays.asList(2,1));
    assertThat(FasterPermutations.permuteList(list)).containsExactlyElementsIn(results);

    list.add(3);
    results.clear();
    results.add(Arrays.asList(1,2,3));
    results.add(Arrays.asList(1,3,2));
    results.add(Arrays.asList(2,1,3));
    results.add(Arrays.asList(2,3,1));
    results.add(Arrays.asList(3,1,2));
    results.add(Arrays.asList(3,2,1));
    assertThat(FasterPermutations.permuteList(list)).containsExactlyElementsIn(results);

    list.add(4);
    results.clear();
    results.add(Arrays.asList(1,2,3,4));
    results.add(Arrays.asList(4,3,2,1));
    results.add(Arrays.asList(4,2,3,1));
    results.add(Arrays.asList(3,2,1,4));
    assertThat(Permutations.permuteList(list)).containsAllIn(results);
    assertThat(Permutations.permuteList(list).size()).isEqualTo(24);

    list.add(5);
    results.clear();
    results.add(Arrays.asList(1,2,3,4,5));
    results.add(Arrays.asList(5,4,3,2,1));
    assertThat(FasterPermutations.permuteList(list)).containsAllIn(results);
    assertThat(FasterPermutations.permuteList(list).size()).isEqualTo(120);

    list.add(6);
    results.clear();
    results.add(Arrays.asList(1,2,3,4,5,6));
    results.add(Arrays.asList(6,5,4,3,2,1));
    assertThat(FasterPermutations.permuteList(list)).containsAllIn(results);
    assertThat(FasterPermutations.permuteList(list).size()).isEqualTo(720);

    list.add(7);
    results.clear();
    results.add(Arrays.asList(1,2,3,4,5,6,7));
    results.add(Arrays.asList(7,6,5,4,3,2,1));
    assertThat(FasterPermutations.permuteList(list)).containsAllIn(results);
    assertThat(FasterPermutations.permuteList(list).size()).isEqualTo(5040);

  }

  public void testPermutationsWithoutDuplicates() {
    if (PermutationsWithoutDuplicates.debug) {
      System.out.println();
      System.out.println("********************************");
      System.out.println("testing permutations with duplicates");
      System.out.println("********************************");
    }

    assertThat(PermutationsWithoutDuplicates.buildPermutations("")).containsExactly("");
    assertThat(PermutationsWithoutDuplicates.buildPermutations("a")).containsExactly("a");
    assertThat(PermutationsWithoutDuplicates.buildPermutations("ab")).containsExactly("ab", "ba");
    assertThat(PermutationsWithoutDuplicates.buildPermutations("abc")).containsExactly("abc", "bac", "bca", "acb", "cab", "cba");

    assertThat(PermutationsWithoutDuplicates.bPs("")).containsExactly("");
    assertThat(PermutationsWithoutDuplicates.bPs("a")).containsExactly("a");
    assertThat(PermutationsWithoutDuplicates.bPs("ab")).containsExactly("ab", "ba");
    assertThat(PermutationsWithoutDuplicates.bPs("abc")).containsExactly("abc", "bac", "bca", "acb", "cab", "cba");

  }

  public void testWaysToStepCounter() {
    if (WaysToStepCounter.debug) {
      System.out.println();
      System.out.println("***********************************");
      System.out.println("ways to step unoptimized");
      System.out.println("***********************************");
    }
    assertThat(WaysToStepCounter.waysToStep(0)).isEqualTo(0);
    assertThat(WaysToStepCounter.waysToStep(1)).isEqualTo(1);
    assertThat(WaysToStepCounter.waysToStep(2)).isEqualTo(2);
    assertThat(WaysToStepCounter.waysToStep(3)).isEqualTo(3);
    assertThat(WaysToStepCounter.waysToStep(4)).isEqualTo(5);
    assertThat(WaysToStepCounter.waysToStep(5)).isEqualTo(8);
    assertThat(WaysToStepCounter.waysToStep(6)).isEqualTo(13);
    assertThat(WaysToStepCounter.waysToStep(7)).isEqualTo(21);
    assertThat(WaysToStepCounter.waysToStep(8)).isEqualTo(34);
    assertThat(WaysToStepCounter.waysToStep(9)).isEqualTo(55);
    assertThat(WaysToStepCounter.waysToStep(10)).isEqualTo(89);
    assertThat(WaysToStepCounter.waysToStep(11)).isEqualTo(144);
    WaysToStepCounter.waysToStep(15);
    WaysToStepCounter.waysToStep(20);

    if (WaysToStepCounter.debug) {
      System.out.println("***********************************");
      System.out.println("ways to step optimized");
      System.out.println("***********************************");
    }
    WaysToStepCounter.isOptimizationEnabled = true;
    assertThat(WaysToStepCounter.waysToStep(0)).isEqualTo(0);
    assertThat(WaysToStepCounter.waysToStep(1)).isEqualTo(1);
    assertThat(WaysToStepCounter.waysToStep(2)).isEqualTo(2);
    assertThat(WaysToStepCounter.waysToStep(3)).isEqualTo(3);
    assertThat(WaysToStepCounter.waysToStep(4)).isEqualTo(5);
    assertThat(WaysToStepCounter.waysToStep(5)).isEqualTo(8);
    assertThat(WaysToStepCounter.waysToStep(6)).isEqualTo(13);
    assertThat(WaysToStepCounter.waysToStep(7)).isEqualTo(21);
    assertThat(WaysToStepCounter.waysToStep(8)).isEqualTo(34);
    assertThat(WaysToStepCounter.waysToStep(9)).isEqualTo(55);
    assertThat(WaysToStepCounter.waysToStep(10)).isEqualTo(89);
    assertThat(WaysToStepCounter.waysToStep(11)).isEqualTo(144);
    WaysToStepCounter.waysToStep(1000);

    assertThat(WaysToStepCounter.waysToStep2(0)).isEqualTo(0);
    assertThat(WaysToStepCounter.waysToStep2(1)).isEqualTo(1);
    assertThat(WaysToStepCounter.waysToStep2(2)).isEqualTo(2);
    assertThat(WaysToStepCounter.waysToStep2(3)).isEqualTo(4);

  }

  public void testCountSummable() {
    List<Integer> list1 = Arrays.asList(1);
    List<Integer> list2 = Arrays.asList(1);
    assertThat(CountSummable.count(list1, list2, 2)).isEqualTo(1);

    list1 = Arrays.asList(1,1,1);
    list2 = Arrays.asList();
    assertThat(CountSummable.count(list1, list2, 2)).isEqualTo(0);

    list1 = Arrays.asList();
    list2 = Arrays.asList();
    assertThat(CountSummable.count(list1, list2, 2)).isEqualTo(0);

    list1 = Arrays.asList(1,1,1);
    list2 = Arrays.asList(1);
    assertThat(CountSummable.count(list1, list2, 2)).isEqualTo(3);

    list1 = Arrays.asList(1,1,1);
    list2 = Arrays.asList(1,1,1);
    assertThat(CountSummable.count(list1, list2, 2)).isEqualTo(3);

    list1 = Arrays.asList(3,2,1);
    list2 = Arrays.asList(2,1,3);
    assertThat(CountSummable.count(list1, list2, 4)).isEqualTo(3);

    list1 = Arrays.asList(3,2,1,4,8);
    list2 = Arrays.asList(2,1,3,0,-4);
    assertThat(CountSummable.count(list1, list2, 4)).isEqualTo(5);
  }

  public void testFastCountSummable() {
    List<Integer> list1 = Arrays.asList(1);
    List<Integer> list2 = Arrays.asList(1);
    assertThat(CountSummable.fastCount(list1, list2, 2)).isEqualTo(1);

    list1 = Arrays.asList(1,1,1);
    list2 = Arrays.asList();
    assertThat(CountSummable.fastCount(list1, list2, 2)).isEqualTo(0);

    list1 = Arrays.asList();
    list2 = Arrays.asList();
    assertThat(CountSummable.fastCount(list1, list2, 2)).isEqualTo(0);

    list1 = Arrays.asList(1,1,1);
    list2 = Arrays.asList(1);
    assertThat(CountSummable.fastCount(list1, list2, 2)).isEqualTo(3);

    list1 = Arrays.asList(1,1,1);
    list2 = Arrays.asList(1,1,1);
    assertThat(CountSummable.fastCount(list1, list2, 2)).isEqualTo(3);

    list1 = Arrays.asList(3,2,1);
    list2 = Arrays.asList(2,1,3);
    assertThat(CountSummable.fastCount(list1, list2, 4)).isEqualTo(3);

    list1 = Arrays.asList(3,2,1,4,8);
    list2 = Arrays.asList(2,1,3,0,-4);
    assertThat(CountSummable.fastCount(list1, list2, 4)).isEqualTo(5);
  }

  public void testMaxSumSequence() {
    List<Integer> list = Arrays.asList(1);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(1);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(1);

    list = Arrays.asList(1,2,3,4);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(4);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(10);

    list = Arrays.asList(1,-1,2);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(2);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(3);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(2);

    list = Arrays.asList(4,-4,2,2,2);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(2);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(5);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(6);

    list = Arrays.asList(-1,-2);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(0);

    list = Arrays.asList(-1,-2);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(0);

    list = Arrays.asList(4,-3,2,2,2);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(5);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(7);

    list = Arrays.asList(4,-3,2,2,2,-100,1,2,3,4,5,-100,50,100,150);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(12);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(15);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(300);

    list = Arrays.asList(200,2,-2,2,2,2,-100,1,1,1,1,-100,50,100,150);
    assertThat(MaxSumSequence.findSequence(list).start).isEqualTo(0);
    assertThat(MaxSumSequence.findSequence(list).end).isEqualTo(15);
    assertThat(MaxSumSequence.findSequence(list).sum).isEqualTo(310);
  }

  public void testPalindrome() {
    assertThat(Palindrome.isPalindrome("")).isTrue();
    assertThat(Palindrome.isPalindrome("a")).isTrue();
    assertThat(Palindrome.isPalindrome("aa")).isTrue();
    assertThat(Palindrome.isPalindrome("aba")).isTrue();
    assertThat(Palindrome.isPalindrome("bba")).isFalse();
    assertThat(Palindrome.isPalindrome("amanaplanacanalpanama")).isTrue();
    assertThat(Palindrome.isPalindrome("radar")).isTrue();
    assertThat(Palindrome.isPalindrome("notapalindrome")).isFalse();
  }

  public void testLargestXNumbers() {
    List<Integer> list = Arrays.asList();
    assertThat(new LargestXNumbers(list, 1).getLargestNumbers()).isEmpty();

    list = Arrays.asList(1,2,3,4);
    assertThat(new LargestXNumbers(list, 1).getLargestNumbers()).containsExactly(4);
    assertThat(new LargestXNumbers(list, 2).getLargestNumbers()).containsExactly(4, 3);
    assertThat(new LargestXNumbers(list, 3).getLargestNumbers()).containsExactly(4, 3, 2);
    assertThat(new LargestXNumbers(list, 4).getLargestNumbers()).containsExactly(4, 3, 2, 1);

    list = Arrays.asList(4,3,2,1);
    assertThat(new LargestXNumbers(list, 1).getLargestNumbers()).containsExactly(4);
    assertThat(new LargestXNumbers(list, 2).getLargestNumbers()).containsExactly(4, 3);
    assertThat(new LargestXNumbers(list, 3).getLargestNumbers()).containsExactly(4, 3, 2);
    assertThat(new LargestXNumbers(list, 4).getLargestNumbers()).containsExactly(4, 3, 2, 1);
  }

  public void testPerfectlyBalanced() {
    Node root = null;
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isTrue();

    root = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isTrue();

    root.leftChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isFalse();

    root.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isTrue();

    root.leftChild.leftChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isFalse();

    root.leftChild.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isFalse();

    root.rightChild.leftChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isFalse();

    root.rightChild.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isTrue();

    root.rightChild.rightChild.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalanced(root)).isFalse();
  }

  public void testPerfectlyBalancedIterative() {
    Node root = null;
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isTrue();

    root = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isTrue();

    root.leftChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isFalse();

    root.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isTrue();

    root.leftChild.leftChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isFalse();

    root.leftChild.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isFalse();

    root.rightChild.leftChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isFalse();

    root.rightChild.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isTrue();

    root.rightChild.rightChild.rightChild = new Node();
    assertThat(PerfectlyBalanced.isPerfectlyBalancedIterative(root)).isFalse();
  }

  public void testFibonacciRecursive() {
    assertThat(Fibonacci.nthRecursive(0)).isEqualTo(0);
    assertThat(Fibonacci.nthRecursive(1)).isEqualTo(1);
    assertThat(Fibonacci.nthRecursive(2)).isEqualTo(1);
    assertThat(Fibonacci.nthRecursive(10)).isEqualTo(55);
    assertThat(Fibonacci.nthRecursive(46)).isEqualTo(1836311903);
  }

  public void testFibonacciIterative() {
    assertThat(Fibonacci.nthIterative(0)).isEqualTo(0);
    assertThat(Fibonacci.nthIterative(1)).isEqualTo(1);
    assertThat(Fibonacci.nthIterative(2)).isEqualTo(1);
    assertThat(Fibonacci.nthIterative(10)).isEqualTo(55);
    assertThat(Fibonacci.nthIterative(46)).isEqualTo(1836311903);
  }

  public void testArrayRotator() {
    int[] numbers = new int[] {1,2,3,4,5};
    int[] rotated = new int[] {1,2,3,4,5};

    ArrayRotator.rotate(numbers, 0);
    assertThat(Arrays.equals(numbers, rotated)).isTrue();

    numbers = new int[] {1,2,3,4,5};
    rotated = new int[] {2,3,4,5,1};

    ArrayRotator.rotate(numbers, 1);
    assertThat(Arrays.equals(numbers, rotated)).isTrue();

    numbers = new int[] {1,2,3,4,5};
    rotated = new int[] {3,4,5,1,2};

    ArrayRotator.rotate(numbers, 2);
    assertThat(Arrays.equals(numbers, rotated)).isTrue();

    numbers = new int[] {1,2,3,4,5};
    rotated = new int[] {1,2,3,4,5};

    ArrayRotator.rotate(numbers, 5);
    assertThat(Arrays.equals(numbers, rotated)).isTrue();
  }

  public void testCountIslands() {
    int[][] map = new int[][] {};
    assertThat(TwoDMap.countIslands(map)).isEqualTo(0);

    map = new int[][] {
      new int[] {1,0}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(1);

    map = new int[][] {
      new int[] {1,1}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(1);

    map = new int[][] {
      new int[] {1,1,1}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(1);

    map = new int[][] {
      new int[] {1,0,1}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(2);

    map = new int[][] {
      new int[] {1,0,1},
      new int[] {1,0,1}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(2);

    map = new int[][] {
      new int[] {1,0,1},
      new int[] {1,1,1}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(1);

    map = new int[][] {
      new int[] {1,0,1},
      new int[] {0,1,0}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(3);

    map = new int[][] {
      new int[] {1,0,1,0,1,0,1},
      new int[] {1,0,1,1,1,0,1},
      new int[] {1,0,0,1,0,0,1},
      new int[] {1,0,1,1,1,0,1},
      new int[] {1,1,1,0,1,1,1}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(1);

    map = new int[][] {
      new int[] {1,0,1,0,1,0,1},
      new int[] {1,0,1,1,1,0,1},
      new int[] {1,0,0,0,0,0,1},
      new int[] {1,0,1,0,1,0,1},
      new int[] {1,1,1,0,1,1,1}
    };
    assertThat(TwoDMap.countIslands(map)).isEqualTo(3);
  }

  public void testFirstNonRepeatingCharacter() {
    char letter = 'l';
    assertThat(FirstNonRepeatingCharacter.getChar("google")).isEqualTo(letter);
    assertThat(FirstNonRepeatingCharacter.getChar("googllee")).isEqualTo(null);
    assertThat(FirstNonRepeatingCharacter.getChar("")).isEqualTo(null);

    letter = 'a';
    assertThat(FirstNonRepeatingCharacter.getChar("agoogle")).isEqualTo(letter);

    letter = 'e';
    assertThat(FirstNonRepeatingCharacter.getChar("lagooglea")).isEqualTo(letter);
  }

  public void testMedianCollection() {
    MedianCollection medianCollection = new MedianCollection();
    assertThat(medianCollection.getMedian()).isNull();

    medianCollection.insert(0);
    assertThat(medianCollection.getMedian()).isEqualTo(0);

    medianCollection.insert(1);
    assertThat(medianCollection.getMedian()).isEqualTo(1);

    medianCollection.insert(2);
    assertThat(medianCollection.getMedian()).isEqualTo(1);

    medianCollection.insert(100);
    assertThat(medianCollection.getMedian()).isEqualTo(2);

    medianCollection.insert(1000);
    assertThat(medianCollection.getMedian()).isEqualTo(2);

    medianCollection.insert(-1);
    assertThat(medianCollection.getMedian()).isEqualTo(2);

    medianCollection.insert(-1);
    assertThat(medianCollection.getMedian()).isEqualTo(1);
  }

  public void testRotationalSymmetry() {
    assertThat(RotationalSymmetry.isSymmetric("")).isTrue();
    assertThat(RotationalSymmetry.isSymmetric("0")).isTrue();
    assertThat(RotationalSymmetry.isSymmetric("6")).isFalse();
    assertThat(RotationalSymmetry.isSymmetric("5")).isFalse();
    assertThat(RotationalSymmetry.isSymmetric("101")).isFalse();
    assertThat(RotationalSymmetry.isSymmetric("808")).isTrue();
    assertThat(RotationalSymmetry.isSymmetric("6880889")).isTrue();
    assertThat(RotationalSymmetry.isSymmetric("89688088968")).isTrue();

    assertThat(RotationalSymmetry.wordsForLength(0)).containsExactly("");
    assertThat(RotationalSymmetry.wordsForLength(1)).containsExactly("0", "8");
    assertThat(RotationalSymmetry.wordsForLength(2)).containsExactly("00", "69", "88", "96");
    assertThat(RotationalSymmetry.wordsForLength(3)).containsExactly("000", "609", "808", "906", "080", "689", "888", "986");
  }

  public void testBinarySearch() {
    assertThat(BinarySearch.findIndex(Arrays.asList(1,3,4), 0)).isEqualTo(-1);
    assertThat(BinarySearch.findIndex(Arrays.asList(), 0)).isEqualTo(-1);
    assertThat(BinarySearch.findIndex(Arrays.asList(5), 5)).isEqualTo(0);
    assertThat(BinarySearch.findIndex(Arrays.asList(1,2), 1)).isEqualTo(0);
    assertThat(BinarySearch.findIndex(Arrays.asList(1,2), 2)).isEqualTo(1);
    assertThat(BinarySearch.findIndex(Arrays.asList(1,2,3,4), 1)).isEqualTo(0);
    assertThat(BinarySearch.findIndex(Arrays.asList(1,2,3,4), 2)).isEqualTo(1);
    assertThat(BinarySearch.findIndex(Arrays.asList(1,2,3,4), 3)).isEqualTo(2);
    assertThat(BinarySearch.findIndex(Arrays.asList(1,2,3,4), 4)).isEqualTo(3);
    assertThat(BinarySearch.findIndex(Arrays.asList(1,2,3), 2)).isEqualTo(1);
  }

  public void testPreOrderTraversal() {
    Node root = new Node(2);
    root.leftChild = new Node(1);
    root.rightChild = new Node(3);
    assertThat(TreeTraversals.preOrder(root)).containsExactly(2,1,3).inOrder();
    assertThat(TreeTraversals.inOrder(root)).containsExactly(1,2,3).inOrder();
    assertThat(TreeTraversals.postOrder(root)).containsExactly(1,3,2).inOrder();
  }

  public void testAbbrev() {
    List<String> words = Arrays.asList("abcd", "acbd", "ab", "ac", "abcde");
    assertThat(Abbreviations.hasUniqueAbbrev("abcd", words)).isFalse();
    assertThat(Abbreviations.hasUniqueAbbrev("acbd", words)).isFalse();
    assertThat(Abbreviations.hasUniqueAbbrev("ab", words)).isTrue();
    assertThat(Abbreviations.hasUniqueAbbrev("abcde", words)).isTrue();
  }

  public void testRandAB() {
    for (int i = 0; i <= 10; i++) {
      while(RandAB.randab(0,10) != i);
    }
    assertThat(RandAB.randab(0,10) <= 10).isTrue();
    assertThat(RandAB.randab(0,10) >= 0).isTrue();
  }

  public void testValidateBST() {
    Node root = new Node();
    root.data = 10;
    assertThat(ValidateBST.isValid(root)).isTrue();

    root.leftChild = new Node();
    root.leftChild.data = 5;

    root.rightChild = new Node();
    root.rightChild.data = 15;
    assertThat(ValidateBST.isValid(root)).isTrue();

    root.rightChild.data = 6;
    assertThat(ValidateBST.isValid(root)).isFalse();

    root.rightChild.data = 15;
    root.rightChild.leftChild = new Node();
    root.rightChild.leftChild.data = 6;
    assertThat(ValidateBST.isValid(root)).isFalse();
    assertThat(ValidateBST.isValid(root.rightChild)).isTrue();
  }

  public void testBitSet() {
    BitSet bs = new BitSet(1);
    assertThat(bs.get(0)).isFalse();
    bs.set(0, true);
    assertThat(bs.get(0)).isTrue();

    bs = new BitSet(31);
    int[] as = {0,1,30};
    for (int i : as) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet(32);
    int[] gs = {0,1,30,31};
    for (int i : gs) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet(33);
    int[] cs = {0,1,30,31,32};
    for (int i : cs) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet(63);
    int[] ds = {0,1,30,31,32,62};
    for (int i : ds) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet(64);
    int[] es = {0,1,30,31,32,62,63};
    for (int i : es) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet(65);
    int[] fs = {0,1,30,31,32,62,63,64};
    for (int i : fs) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }
  }

  public void testBitSet2() {
    BitSet2 bs = new BitSet2(1);
    assertThat(bs.get(0)).isFalse();
    bs.set(0, true);
    assertThat(bs.get(0)).isTrue();

    bs = new BitSet2(31);
    int[] as = {0,1,30};
    for (int i : as) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet2(32);
    int[] gs = {0,1,30,31};
    for (int i : gs) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet2(33);
    int[] cs = {0,1,30,31,32};
    for (int i : cs) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet2(63);
    int[] ds = {0,1,30,31,32,62};
    for (int i : ds) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet2(64);
    int[] es = {0,1,30,31,32,62,63};
    for (int i : es) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }

    bs = new BitSet2(65);
    int[] fs = {0,1,30,31,32,62,63,64};
    for (int i : fs) {
      assertThat(bs.get(i)).isFalse();
      bs.set(i, true);
      assertThat(bs.get(i)).isTrue();
    }
  }



  public void testFizzBuzz() {
    assertThat(new FizzBuzzManager(0).results()).isEmpty();
    assertThat(new FizzBuzzManager(1).results()).containsExactly("fizzbuzz").inOrder();
    assertThat(new FizzBuzzManager(2).results()).containsExactly("fizzbuzz", "1").inOrder();
    assertThat(new FizzBuzzManager(3).results()).containsExactly("fizzbuzz","1","2").inOrder();
    assertThat(new FizzBuzzManager(16).results()).containsExactly("fizzbuzz","1","2","fizz","4","buzz","fizz","7","8","fizz","buzz","11","fizz","13","14","fizzbuzz").inOrder();

  }

  public void testParenGenerator() {
    assertThat(ParenGenerator.allUniqueCombos(0).size()).isEqualTo(0);
    assertThat(ParenGenerator.allUniqueCombos(1).size()).isEqualTo(1);
    assertThat(ParenGenerator.allUniqueCombos(2).size()).isEqualTo(2);
    assertThat(ParenGenerator.allUniqueCombos(3).size()).isEqualTo(5);
    assertThat(ParenGenerator.allUniqueCombos(4).size()).isEqualTo(13);
  }
  public void testWiggle() {
    assertThat(Wiggle.countLongestWSS(Arrays.asList())).isEqualTo(0);
    assertThat(Wiggle.countLongestWSS(Arrays.asList(1))).isEqualTo(1);
    assertThat(Wiggle.countLongestWSS(Arrays.asList(1,2))).isEqualTo(2);
    assertThat(Wiggle.countLongestWSS(Arrays.asList(1,2,3,4))).isEqualTo(2);
    assertThat(Wiggle.countLongestWSS(Arrays.asList(1,2,1))).isEqualTo(3);
    assertThat(Wiggle.countLongestWSS(Arrays.asList(1,2,1,100,101,100,101,0,1,0,1,100))).isEqualTo(10);
    assertThat(Wiggle.countLongestWSS(Arrays.asList(1,2,3,4,1,2,3,4,1,2,3,4))).isEqualTo(6);
  }

  public void testPaintFill() {
    PaintFill paintFill = new PaintFill();
    int[][] canvas = {
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0}
    };
    paintFill.setCanvas(canvas);
    paintFill.fill(5,5,1);
    canvas = paintFill.getCanvas();
    int[] row = {1,1,1,1,1,1,1,1,1,1};
    for (int i = 0; i < PaintFill.SIZE; i++) {
      assertThat(canvas[0]).isEqualTo(row);
    }

    int[][] canvas2 = {
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,1,1,1,1,0,0,0,0,0},
      {0,0,0,0,1,0,0,0,0,0},
      {0,0,0,0,1,0,1,1,1,1},
      {0,0,1,1,1,1,1,0,0,1},
      {0,0,1,0,1,0,0,0,0,1},
      {0,0,1,1,1,0,0,0,0,1},
      {0,0,0,0,0,0,0,0,0,1},
      {0,0,0,0,0,0,0,0,0,1}
    };

    paintFill.setCanvas(canvas2);
    paintFill.fill(2,1,2);

    int[][] output = {
      {0,0,0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0,0,0},
      {0,2,2,2,2,0,0,0,0,0},
      {0,0,0,0,2,0,0,0,0,0},
      {0,0,0,0,2,0,2,2,2,2},
      {0,0,2,2,2,2,2,0,0,2},
      {0,0,2,0,2,0,0,0,0,2},
      {0,0,2,2,2,0,0,0,0,2},
      {0,0,0,0,0,0,0,0,0,2},
      {0,0,0,0,0,0,0,0,0,2}
    };

    for (int i = 0; i < PaintFill.SIZE; i++) {
      assertThat(canvas2[i]).isEqualTo(output[i]);
    }
  }

  public void testBinaryRepPos() {
    assertThat(BinaryRep.binaryRep(1)).isEqualTo("1");
    assertThat(BinaryRep.binaryRep(2)).isEqualTo("10");
    assertThat(BinaryRep.binaryRep(3)).isEqualTo("11");
    assertThat(BinaryRep.binaryRep(4)).isEqualTo("100");
    assertThat(BinaryRep.binaryRep(5)).isEqualTo("101");
    assertThat(BinaryRep.binaryRep(6)).isEqualTo("110");
    assertThat(BinaryRep.binaryRep(7)).isEqualTo("111");
    assertThat(BinaryRep.binaryRep(23902341)).isEqualTo("1011011001011100010000101");
  }

  public void testBinaryRepNeg() {
    assertThat(BinaryRep.binaryRep(-1)).isEqualTo("1111111111111111111111111111111");
    assertThat(BinaryRep.binaryRep(-2)).isEqualTo("1111111111111111111111111111110");
    assertThat(BinaryRep.binaryRep(-3)).isEqualTo("1111111111111111111111111111101");
    assertThat(BinaryRep.binaryRep(-4)).isEqualTo("1111111111111111111111111111100");
    assertThat(BinaryRep.binaryRep(-5)).isEqualTo("1111111111111111111111111111011");
    assertThat(BinaryRep.binaryRep(-6)).isEqualTo("1111111111111111111111111111010");
    assertThat(BinaryRep.binaryRep(-7)).isEqualTo("1111111111111111111111111111001");
    assertThat(BinaryRep.binaryRep(-451)).isEqualTo("1111111111111111111111000111101");
  }

  public void testBinaryRepFraction() {
    assertThat(BinaryRep.fractionToString(0.5)).isEqualTo("0.1");
    assertThat(BinaryRep.fractionToString(0.25)).isEqualTo("0.01");
    assertThat(BinaryRep.fractionToString(0.4175434112548828)).isEqualTo("0.0110101011100100001");
  }

  public void testUrlify() {
    assertThat(Urlify.urlify("a b c    ".toCharArray(), 5)).isEqualTo("a%20b%20c");
    assertThat(Urlify.urlify("   ".toCharArray(), 1)).isEqualTo("%20");
    assertThat(Urlify.urlify("               ".toCharArray(), 5)).isEqualTo("%20%20%20%20%20");
    assertThat(Urlify.urlify("x     x          ".toCharArray(), 7)).isEqualTo("x%20%20%20%20%20x");
  }

  public void testGroupAnagrams() {
    List<String> words = new ArrayList<String>();
    words.add("abc");
    words.add("cba");
    words.add("gogo");
    words.add("ogog");
    words.add("blip");

    List<List<String>> groupedAnagrams = GroupAnagrams.groupAnagrams(words);
    assertThat(groupedAnagrams).contains(Arrays.asList("abc", "cba"));
    assertThat(groupedAnagrams).contains(Arrays.asList("blip"));
    assertThat(groupedAnagrams).contains(Arrays.asList("gogo", "ogog"));
  }

  public void testLineWithMostPoints() {
    List<Point> points = new ArrayList<Point>();
    points.add(new Point(0.0, 0.0));
    points.add(new Point(1.0, 1.0));
    points.add(new Point(2.0, 2.0));

    points.add(new Point(-1.0, 0.0));
    points.add(new Point(1.0, 0.0));
    // points.add(new Point(2.0, 0.0));
    // points.add(new Point(3.0, 0.0));

    points.add(new Point(0.0, 1.0));
    points.add(new Point(1.0, 2.0));
    points.add(new Point(2.0, 3.0));
    points.add(new Point(3.0, 4.0));
    points.add(new Point(4.0, 5.0));
    points.add(new Point(5.0, 6.0));

    Line line = new Line(new Point(0.0, 1.0), new Point(1.0, 2.0));

    line = new Line(new Point(1.0, 2.0), new Point(2.0, 3.0));

    assertThat(LineWithMostPoints.lineWithMostPoints(points).toString()).isEqualTo("y = 1.0x + 1.0");
  }

  public void testDeleteMiddleNode() {
    LinkedListNode node = new LinkedListNode(5);
    node = new LinkedListNode(4,node);
    LinkedListNode middle = node = new LinkedListNode(3,node);
    node = new LinkedListNode(2,node);
    node = new LinkedListNode(1,node);

    assertThat(LinkedListNode.toList(node)).containsExactly(1,2,3,4,5).inOrder();
    DeleteMiddleNode.deleteMiddleNodeConstant(middle);
    assertThat(LinkedListNode.toList(node)).containsExactly(1,2,4,5).inOrder();
  }

  public void testfindRotDeg() {
    int[] a = {1};
    assertThat(RotatedArray.findRotDeg(0, 0, a)).isEqualTo(0);

    int[] b = {1,2};
    assertThat(RotatedArray.findRotDeg(0, 1, b)).isEqualTo(0);

    int[] c = {1,2,3};
    assertThat(RotatedArray.findRotDeg(0, 2, c)).isEqualTo(0);

    int[] d = {1,2,3,4,5};
    assertThat(RotatedArray.findRotDeg(0, 4, d)).isEqualTo(0);

    int[] e = {5, 1,2,3,4};
    assertThat(RotatedArray.findRotDeg(0, 4, e)).isEqualTo(1);

    int[] f = {4,5,1,2,3};
    assertThat(RotatedArray.findRotDeg(0, 4, f)).isEqualTo(2);

    int[] g = {2,3,4,5,1};
    assertThat(RotatedArray.findRotDeg(0, 4, g)).isEqualTo(4);
  }

  public void testFindInRotArr() {
    int[] a = {1};
    assertThat(RotatedArray.find(1, a)).isEqualTo(0);
    assertThat(RotatedArray.find(2, a)).isEqualTo(-1);

    int[] b = {1,2};
    assertThat(RotatedArray.find(1, b)).isEqualTo(0);
    assertThat(RotatedArray.find(2, b)).isEqualTo(1);
    assertThat(RotatedArray.find(3, b)).isEqualTo(-1);

    int[] c = {1,2,3};
    assertThat(RotatedArray.find(1, c)).isEqualTo(0);
    assertThat(RotatedArray.find(2, c)).isEqualTo(1);
    assertThat(RotatedArray.find(3, c)).isEqualTo(2);
    assertThat(RotatedArray.find(4, c)).isEqualTo(-1);

    int[] d = {1,2,3,4,5};
    assertThat(RotatedArray.find(1, d)).isEqualTo(0);
    assertThat(RotatedArray.find(2, d)).isEqualTo(1);
    assertThat(RotatedArray.find(3, d)).isEqualTo(2);
    assertThat(RotatedArray.find(4, d)).isEqualTo(3);
    assertThat(RotatedArray.find(5, d)).isEqualTo(4);
    assertThat(RotatedArray.find(6, d)).isEqualTo(-1);

    int[] e = {5, 1,2,3,4};
    assertThat(RotatedArray.find(5, e)).isEqualTo(0);
    assertThat(RotatedArray.find(1, e)).isEqualTo(1);
    assertThat(RotatedArray.find(2, e)).isEqualTo(2);
    assertThat(RotatedArray.find(3, e)).isEqualTo(3);
    assertThat(RotatedArray.find(4, e)).isEqualTo(4);
    assertThat(RotatedArray.find(6, e)).isEqualTo(-1);

    int[] f = {4,5,1,2,3};
    assertThat(RotatedArray.find(4, f)).isEqualTo(0);
    assertThat(RotatedArray.find(5, f)).isEqualTo(1);
    assertThat(RotatedArray.find(1, f)).isEqualTo(2);
    assertThat(RotatedArray.find(2, f)).isEqualTo(3);
    assertThat(RotatedArray.find(3, f)).isEqualTo(4);
    assertThat(RotatedArray.find(6, f)).isEqualTo(-1);

    int[] g = {2,3,4,5,1};
    assertThat(RotatedArray.find(2, g)).isEqualTo(0);
    assertThat(RotatedArray.find(3, g)).isEqualTo(1);
    assertThat(RotatedArray.find(4, g)).isEqualTo(2);
    assertThat(RotatedArray.find(5, g)).isEqualTo(3);
    assertThat(RotatedArray.find(1, g)).isEqualTo(4);
    assertThat(RotatedArray.find(6, g)).isEqualTo(-1);

    int[] h = {180, 181, 182, 183, 184, 187, 188, 189, 191, 192, 193, 194, 195, 196, 201, 202, 203, 204, 3, 4, 5, 6, 7, 8, 9, 10, 14, 16, 17, 18, 19, 23, 26, 27, 28, 29, 32, 33, 36, 37, 38, 39, 41, 42, 43, 45, 48, 51, 52, 53, 54, 56, 62, 63, 64, 67, 69, 72, 73, 75, 77, 78, 79, 83, 85, 87, 90, 91, 92, 93, 96, 98, 99, 101, 102, 104, 105, 106, 107, 108, 109, 111, 113, 115, 116, 118, 119, 120, 122, 123, 124, 126, 127, 129, 130, 135, 137, 138, 139, 143, 144, 145, 147, 149, 152, 155, 156, 160, 162, 163, 164, 166, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177};
    assertThat(RotatedArray.find(42, h)).isEqualTo(43);
  }

  public void testFindLeftMost() {
    assertThat(FindLeftMost.find(1, Arrays.asList(1))).isEqualTo(0);
    assertThat(FindLeftMost.find(3, Arrays.asList(1))).isEqualTo(-1);
    assertThat(FindLeftMost.find(3, Arrays.asList(1,1,2,3,4))).isEqualTo(3);
    assertThat(FindLeftMost.find(3, Arrays.asList(1,1,2,3,3,4))).isEqualTo(3);
    assertThat(FindLeftMost.find(3, Arrays.asList(1,1,2,3,3,3,4))).isEqualTo(3);
    assertThat(FindLeftMost.find(3, Arrays.asList(1,1,3,3,3,4))).isEqualTo(2);
    assertThat(FindLeftMost.find(3, Arrays.asList(1,3,3,3,4))).isEqualTo(1);
    assertThat(FindLeftMost.find(5, Arrays.asList(1,3,3,3,4))).isEqualTo(-1);
    assertThat(FindLeftMost.find(3, Arrays.asList(1,1,1,1,1,3,3,3,3,3))).isEqualTo(5);
    assertThat(FindLeftMost.find(8, Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10))).isEqualTo(154);

  }

  public void testRomanNumerals() {
    assertThat(RomanNumerals.romanToInt("I")).isEqualTo(1);
    assertThat(RomanNumerals.romanToInt("MMMCMXCIX")).isEqualTo(3999);
    assertThat(RomanNumerals.intToRoman(1)).isEqualTo("I");
    assertThat(RomanNumerals.intToRoman(3999)).isEqualTo("MMMCMXCIX");
  }

  public void testListsAtDepths() {
    assertThat(ListsAtDepths.getLists(null)).isEmpty();

    Node root = new Node();
    assertThat(ListsAtDepths.getLists(root).size()).isEqualTo(1);
    assertThat(ListsAtDepths.getLists(root).get(0).size()).isEqualTo(1);

    root.leftChild = new Node();
    assertThat(ListsAtDepths.getLists(root).size()).isEqualTo(2);
    assertThat(ListsAtDepths.getLists(root).get(0).size()).isEqualTo(1);
    assertThat(ListsAtDepths.getLists(root).get(1).size()).isEqualTo(1);

    root.rightChild = new Node();
    assertThat(ListsAtDepths.getLists(root).get(0)).containsExactly(root).inOrder();
    assertThat(ListsAtDepths.getLists(root).get(1)).containsExactly(root.leftChild, root.rightChild).inOrder();

    Node element1 = root.leftChild.rightChild = new Node();
    Node element2 = root.rightChild.leftChild = new Node();
    assertThat(ListsAtDepths.getLists(root).get(0)).containsExactly(root).inOrder();
    assertThat(ListsAtDepths.getLists(root).get(1)).containsExactly(root.leftChild, root.rightChild).inOrder();
    assertThat(ListsAtDepths.getLists(root).get(2)).containsExactly(element1, element2).inOrder();
  }

  public void testFlipBitWinner() {
    assertThat(FlipBit.longestSeq(0)).isEqualTo(1);
    assertThat(FlipBit.longestSeq(1)).isEqualTo(2);
    assertThat(FlipBit.longestSeq(2)).isEqualTo(2);
    assertThat(FlipBit.longestSeq(3)).isEqualTo(3);
    assertThat(FlipBit.longestSeq(98748)).isEqualTo(7);
  }

  public void testLongestPalindrome() {
    assertThat(LongestPalindrome.longestPalindrome("")).isEqualTo("");
    assertThat(LongestPalindrome.longestPalindrome("a")).isEqualTo("a");
    assertThat(LongestPalindrome.longestPalindrome("ab")).isEqualTo("a");
    assertThat(LongestPalindrome.longestPalindrome("bb")).isEqualTo("bb");
    assertThat(LongestPalindrome.longestPalindrome("bbb")).isEqualTo("bbb");
    assertThat(LongestPalindrome.longestPalindrome("bbbb")).isEqualTo("bbbb");
    assertThat(LongestPalindrome.longestPalindrome("bbbbbbbbb")).isEqualTo("bbbbbbbbb");
    assertThat(LongestPalindrome.longestPalindrome("bobbbobbb")).isEqualTo("bobbbob");
    assertThat(LongestPalindrome.longestPalindrome("bobbbbobbxbbobbbb")).isEqualTo("bbbbobbxbbobbbb");
  }

  public void testLinkedListIntersection() {
    LinkedListNode h1 = new LinkedListNode(1);
    h1 = new LinkedListNode(1,h1);

    LinkedListNode h2 = new LinkedListNode(1);
    h2 = new LinkedListNode(1,h2);

    assertThat(LinkedListNode.intersection(h1, h2)).isNull();

    h1 = new LinkedListNode(1,h1);
    h1 = new LinkedListNode(1,h1);
    LinkedListNode intersection = h1 = new LinkedListNode(1,h1);

    h2 = new LinkedListNode(1,intersection);
    h2 = new LinkedListNode(1,h2);
    h2 = new LinkedListNode(1,h2);
    h2 = new LinkedListNode(1,h2);
    h2 = new LinkedListNode(1,h2);
    h2 = new LinkedListNode(1,h2);

    assertThat(LinkedListNode.intersection(h1, h2)).isEqualTo(intersection);

    h1 = new LinkedListNode(1,h1);
    h1 = new LinkedListNode(1,h1);
    h1 = new LinkedListNode(1,h1);

    assertThat(LinkedListNode.intersection(h1, h2)).isEqualTo(intersection);
  }

  public void testPlaceNQueens() {
    ChessBoard chessBoard = new ChessBoard(1);
    assertThat(chessBoard.placeNQueens().size()).isEqualTo(1);

    chessBoard.dimension = 2;
    assertThat(chessBoard.placeNQueens()).isEmpty();

    chessBoard.dimension = 3;
    assertThat(chessBoard.placeNQueens()).isEmpty();

    chessBoard.dimension = 4;
    assertThat(chessBoard.placeNQueens().size()).isEqualTo(2);

    chessBoard.dimension = 5;
    assertThat(chessBoard.placeNQueens().size()).isEqualTo(10);

    chessBoard.dimension = 6;
    assertThat(chessBoard.placeNQueens().size()).isEqualTo(4);

    chessBoard.dimension = 7;
    assertThat(chessBoard.placeNQueens().size()).isEqualTo(40);

    chessBoard.dimension = 8;
    assertThat(chessBoard.placeNQueens().size()).isEqualTo(92);
  }

  public void testSparseSearch() {
    assertThat(SparseSearch.find(Arrays.asList(), "c")).isEqualTo(-1);
    assertThat(SparseSearch.find(Arrays.asList("","","",""), "c")).isEqualTo(-1);
    assertThat(SparseSearch.find(Arrays.asList("c","","",""), "c")).isEqualTo(0);
    assertThat(SparseSearch.find(Arrays.asList("","","","c"), "c")).isEqualTo(3);
    assertThat(SparseSearch.find(Arrays.asList("","a","b","c","","","d"), "c")).isEqualTo(3);
    assertThat(SparseSearch.find(Arrays.asList("","a","","","","","","","","","","b","","","","","","","","","","","","c","","","","","","","","","","","","d","","","","e","","","","f","","","","","g","","","","","h","","","","","","i","","","","","j","","","","o","","","","","q","","","","","z","",""), "c")).isEqualTo(23);
    assertThat(SparseSearch.find(Arrays.asList("","a","","","","","","","","","","b","","","","","","","","","","","","c","","","","","","","","","","","","d","","","","e","","","","f","","","","","g","","","","","h","","","","","","i","","","","","j","","","","o","","","","","q","","","","","z","",""), "k")).isEqualTo(-1);
    assertThat(SparseSearch.find(Arrays.asList("","a","","","","","","","","","","b","","","","","","","","","","","","c","","","","","","","","","","","","d","","","","e","","","","f","","","","","g","","","","","h","","","","","","i","","","","","j","","","","o","","","","","q","","","","","z","",""), "j")).isEqualTo(64);
    assertThat(SparseSearch.find(Arrays.asList("a","a","b","d","f","i","k","m","o","q","u","w"), "t")).isEqualTo(-1);
    assertThat(SparseSearch.find(Arrays.asList("a","a","b","d","f","i","k","m","o","q","u","w"), "b")).isEqualTo(2);
  }

  public void testMultiply() {
    assertThat(Multiply.multiply(2,2)).isEqualTo(4);
    assertThat(Multiply.multiply(4,4)).isEqualTo(16);
    assertThat(Multiply.multiply(0,4)).isEqualTo(0);
    assertThat(Multiply.multiply(1,5)).isEqualTo(5);
    assertThat(Multiply.multiply(876,165)).isEqualTo(144540);
  }

  public void testRecursiveMultiply() {
    assertThat(RecursiveMultiply.multiply(2,2)).isEqualTo(4);
    assertThat(RecursiveMultiply.multiply(4,4)).isEqualTo(16);
    assertThat(RecursiveMultiply.multiply(0,4)).isEqualTo(0);
    assertThat(RecursiveMultiply.multiply(1,5)).isEqualTo(5);
    assertThat(RecursiveMultiply.multiply(876,165)).isEqualTo(144540);
  }

  public void testRecursiveMultiply2() {
    assertThat(RecursiveMultiply.multiply2(2,2)).isEqualTo(4);
    assertThat(RecursiveMultiply.multiply2(4,4)).isEqualTo(16);
    assertThat(RecursiveMultiply.multiply2(0,4)).isEqualTo(0);
    assertThat(RecursiveMultiply.multiply2(1,5)).isEqualTo(5);
    assertThat(RecursiveMultiply.multiply2(876,165)).isEqualTo(144540);
  }

  public void testCoins() {
    Coins coins = new Coins();
    assertThat(coins.waysToMakeChange(4)).isEqualTo(1);
    assertThat(coins.waysToMakeChange(5)).isEqualTo(2);
    assertThat(coins.waysToMakeChange(10)).isEqualTo(4);
    assertThat(coins.waysToMakeChange(764)).isEqualTo(63864);
  }

  public void testIsUnique() {
    assertThat(IsUnique.hasAllUniqueChars("")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars("aa")).isFalse();
    assertThat(IsUnique.hasAllUniqueChars("a")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars("abcd")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars("abcda")).isFalse();

    assertThat(IsUnique.hasAllUniqueChars2("")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars2("aa")).isFalse();
    assertThat(IsUnique.hasAllUniqueChars2("a")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars2("abcd")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars2("abcda")).isFalse();

    assertThat(IsUnique.hasAllUniqueChars3("")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars3("aa")).isFalse();
    assertThat(IsUnique.hasAllUniqueChars3("a")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars3("abcd")).isTrue();
    assertThat(IsUnique.hasAllUniqueChars3("abcda")).isFalse();
  }
  
  public void testSortedMerge() {
    int[] a = new int[] {1,3,5,0,0,0,0};
    int[] b = new int[] {2,4,6,8};

    assertFalse(IsSorted.isSorted(a));
    SortedMerge.merge(a,b,3);
    assertTrue(IsSorted.isSorted(a));

    a = new int[] {1,2,3,0,0,0,0};
    b = new int[] {4,5,6,7};

    assertFalse(IsSorted.isSorted(a));
    SortedMerge.merge(a,b,3);
    assertTrue(IsSorted.isSorted(a));

    a = new int[] {1,0};
    b = new int[] {1};

    assertFalse(IsSorted.isSorted(a));
    SortedMerge.merge(a,b,1);
    assertTrue(IsSorted.isSorted(a));

    a = new int[] {1};
    b = new int[] {};

    SortedMerge.merge(a,b,1);
    assertTrue(IsSorted.isSorted(a));
  }

  public void testSearchRotated() {
    int[] a = new int[] {3,1,2};
    assertThat(SearchRotated.findK(a,0,a.length-1)).isEqualTo(1);

    a = new int[] {1,2,3};
    assertThat(SearchRotated.findK(a,0,a.length-1)).isEqualTo(0);

    a = new int[] {1,2,3};
    assertThat(SearchRotated.search(a,1)).isEqualTo(0);

    a = new int[] {1,2,3};
    assertThat(SearchRotated.search(a,3)).isEqualTo(2);

    a = new int[] {5,6,7,1,2,3};
    assertThat(SearchRotated.findK(a,0,a.length-1)).isEqualTo(3);
    assertThat(SearchRotated.search(a,1)).isEqualTo(3);
  }

  public void testRobotBoard() {
    boolean[][] board;

    board = new boolean[][] {
      new boolean[] {true,true},
      new boolean[] {true,true}
    };

    assertThat(RobotBoard.findPath(board)).containsExactly("r","d").inOrder();

    board = new boolean[][] {
      new boolean[] {true,false},
      new boolean[] {true,true}
    };
    assertThat(RobotBoard.findPath(board)).containsExactly("d","r").inOrder();

    board = new boolean[][] {
      new boolean[] {true,false,true},
      new boolean[] {true,true,false},
      new boolean[] {true,true,true}
    };
    assertThat(RobotBoard.findPath(board)).containsExactly("d","r","d","r").inOrder();

  }

  public void testStringCompression() {
    assertThat(StringCompression.compress("")).isEqualTo("");
    assertThat(StringCompression.compress("a")).isEqualTo("a");
    assertThat(StringCompression.compress("aa")).isEqualTo("aa");
    assertThat(StringCompression.compress("aaa")).isEqualTo("a3");
    assertThat(StringCompression.compress("aabcccccaaa")).isEqualTo("a2b1c5a3");
  }

  public void testStringSequences() {
    assertThat(StringSequences.sequences("a","b")).containsExactly("ab","ba");
    assertThat(StringSequences.sequences("ab","x")).containsExactly("abx","axb", "xab");
    assertThat(StringSequences.sequences("abc","xy")).containsExactly("abcxy", "abxcy", "axbcy", "xabcy", "xabyc", "xaybc", "xyabc", "axbyc", "axybc", "abxyc");
  }

  public void testZeroMatrix() {
    int[][] matrix = new int[][] {
      new int[] {0,1},
      new int[] {1,0}
    };

    ZeroMatrix.fillZeroes(matrix);

    assertThat(matrix[0][0]).isEqualTo(0);
    assertThat(matrix[0][1]).isEqualTo(0);
    assertThat(matrix[1][0]).isEqualTo(0);
    assertThat(matrix[1][1]).isEqualTo(0);


    matrix = new int[][] {
      new int[] {1,0,1},
      new int[] {1,1,1}
    };

    ZeroMatrix.fillZeroes(matrix);

    assertThat(matrix[0][0]).isEqualTo(0);
    assertThat(matrix[0][1]).isEqualTo(0);
    assertThat(matrix[0][2]).isEqualTo(0);
    assertThat(matrix[1][0]).isEqualTo(1);
    assertThat(matrix[1][1]).isEqualTo(0);
    assertThat(matrix[1][2]).isEqualTo(1);


    matrix = new int[][] {
      new int[] {1,0,1},
      new int[] {1,1,1},
      new int[] {1,1,1},
      new int[] {1,1,0},
    };

    ZeroMatrix.fillZeroes(matrix);

    assertThat(matrix[0][0]).isEqualTo(0);
    assertThat(matrix[0][1]).isEqualTo(0);
    assertThat(matrix[0][2]).isEqualTo(0);
    assertThat(matrix[1][0]).isEqualTo(1);
    assertThat(matrix[1][1]).isEqualTo(0);
    assertThat(matrix[1][2]).isEqualTo(0);
    assertThat(matrix[2][0]).isEqualTo(1);
    assertThat(matrix[2][1]).isEqualTo(0);
    assertThat(matrix[2][2]).isEqualTo(0);
    assertThat(matrix[3][0]).isEqualTo(0);
    assertThat(matrix[3][1]).isEqualTo(0);
    assertThat(matrix[3][2]).isEqualTo(0);
  }

  public void testPeaksAndValleys() {
    assertThat(PeaksAndValleys.sort(Arrays.asList(1,2))).containsExactly(1,2).inOrder();
    assertThat(PeaksAndValleys.sort(Arrays.asList(1,2,3))).containsExactly(1,3,2).inOrder();
    assertThat(PeaksAndValleys.sort(Arrays.asList(9,1,0,4,8,7))).containsExactly(9,0,4,1,8,7).inOrder();
    assertThat(PeaksAndValleys.sort(Arrays.asList(5,3,1,2,3))).containsExactly(5,1,3,2,3).inOrder();
    assertThat(PeaksAndValleys.sort(Arrays.asList(5,8,6,2,3,4,6))).containsExactly(5,8,2,6,3,6,4).inOrder();
  }

  public void testFindMissing() {
    assertThat(FindMissing.find(Arrays.asList(0,2), 2)).isEqualTo(1);
    assertThat(FindMissing.find(Arrays.asList(2,1), 2)).isEqualTo(0);
    assertThat(FindMissing.find(Arrays.asList(0,1), 2)).isEqualTo(2);
    // assertThat(FindMissing.find(Arrays.asList(0,1,2,4,5,6), 6)).isEqualTo(3); // broken
    // assertThat(FindMissing.find(Arrays.asList(3,1,2,4,5,6), 6)).isEqualTo(0);
  }

  public void testBiNode() {
    BiNode root = new BiNode(10);
    BiNode.convertFromTree(root);
    assertThat(BiNode.toStringAsLL(root)).isEqualTo("10,");

    root = new BiNode(10);
    BiNode head = root.node1 = new BiNode(3);
    BiNode.convertFromTree(root);
    assertThat(BiNode.toStringAsLL(head)).isEqualTo("3,10,");

    root = new BiNode(10);
    head = root.node1 = new BiNode(3);
    root.node2 = new BiNode(20);
    root.node2.node1 = new BiNode(15);
    root.node2.node2 = new BiNode(30);
    BiNode.convertFromTree(root);
    assertThat(BiNode.toStringAsLL(head)).isEqualTo("3,10,15,20,30,");

    root = new BiNode(10);
    root.node1 = new BiNode(3);
    head = root.node1.node1 = new BiNode(1);
    root.node1.node2 = new BiNode(5);

    root.node2 = new BiNode(20);
    root.node2.node1 = new BiNode(15);
    root.node2.node2 = new BiNode(30);
    BiNode.convertFromTree(root);
    assertThat(BiNode.toStringAsLL(head)).isEqualTo("1,3,5,10,15,20,30,");
  }

  public void testCalculator() {
    assertThat(Calculator.E(new Scanner("10")).getValue()).isEqualTo(10);
    assertThat(Calculator.E(new Scanner("10 * 2")).getValue()).isEqualTo(20);
    assertThat(Calculator.E(new Scanner("10 * 2 * 4")).getValue()).isEqualTo(80);
    assertThat(Calculator.E(new Scanner("10 * 2 + 4")).getValue()).isEqualTo(24);
    assertThat(Calculator.E(new Scanner("10 + 2 * 4")).getValue()).isEqualTo(18);
    assertThat(Calculator.E(new Scanner("( 10 + 2 ) * 4")).getValue()).isEqualTo(48);
    assertThat(Calculator.E(new Scanner("10 + 4 / 2 - 6")).getValue()).isEqualTo(6);
    // broken due to subtraction being left associative
    // assertThat(Calculator.E(new Scanner("10 - 6 - 2")).getValue()).isEqualTo(2);
  }

  public void testValidArithmeticExpressions() {
    ValidArithmeticExpressions vae = new ValidArithmeticExpressions();
    assertThat(vae.validExpressions(0)).isEmpty();
    assertThat(vae.validExpressions(1)).containsExactly("a");
    assertThat(vae.validExpressions(2)).isEmpty(); // ["(a", "(b"]
    assertThat(vae.validExpressions(3)).containsExactly("a+a", "(a)");
    assertThat(vae.validExpressions(4)).isEmpty();
  }

  public void testFractionToDecimal() {
    assertThat(FractionToDecimal.divides(0.5, 0.1)).isEqualTo(5);
    assertThat(FractionToDecimal.convert(8,2)).isEqualTo("4");
    assertThat(FractionToDecimal.convert(1,2)).isEqualTo("0.5");
    assertThat(FractionToDecimal.convert(3,2)).isEqualTo("1.5");
    assertThat(FractionToDecimal.convert(2,3)).isEqualTo("0.(6)");
    assertThat(FractionToDecimal.convert(4,3)).isEqualTo("1.(3)");
    assertThat(FractionToDecimal.convert(7,12)).isEqualTo("0.58(3)");
  }










    public void testDLL() {
      DoublyLinkedList dll = new DoublyLinkedList();
      dll.addToTail(1);
      assertThat(dll.peekAtTail()).isEqualTo(1);
      dll.addToTail(2);
      assertThat(dll.peekAtTail()).isEqualTo(2);
      dll.removeFromTail();
      assertThat(dll.peekAtTail()).isEqualTo(1);
      dll.removeFromTail();
      assertThat(dll.peekAtTail()).isEqualTo(null);
      assertThat(dll.head).isEqualTo(null);
      assertThat(dll.tail).isEqualTo(null);



      dll = new DoublyLinkedList();
      dll.addToTail(1);
      DLLNode node = dll.addToTail(2);
      dll.addToTail(3);
      assertThat(dll.peekAtTail()).isEqualTo(3);
      dll.moveToTail(node);
      assertThat(dll.peekAtTail()).isEqualTo(2);
    }

    public void testFeed() {
    }


















}
