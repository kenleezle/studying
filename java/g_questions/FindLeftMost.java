package g_questions;

import java.util.List;

public class FindLeftMost {
  public static int find(int search, List<Integer> nums) {
    return myFind(0, nums.size() - 1, search, nums);
  }

  private static int myFind(int lo, int hi, int search, List<Integer> nums) {
    int size = hi - lo + 1;

    if (size <= 0) return -1;

    if (size == 1 && search == nums.get(lo)) return lo;

    if (size == 2) {
      if (search == nums.get(lo)) return lo;
      if (search == nums.get(hi)) return hi;
    }

    int m = lo + (size / 2);

    if (search == nums.get(m)) {
      return myFind(lo, m, search, nums);
    } else if (search < nums.get(m)) {
      return myFind(lo, m - 1, search, nums);
    }

    // search > nums.get(m)
    return myFind(m + 1, hi, search, nums);
  }
}
