package g_questions;

public class SearchRotated {
  public static int search(int[] a, int search) {
    int k = findK(a, 0, a.length-1);
    return bSearch(a, search, k, 0, a.length - 1);
  }

  public static int findK(int[] a, int start, int end) {
    if (a[start] <= a[end]) { return start; }

    int m = (start + end) / 2;

    boolean isLeftInOrder = a[start] <= a[m-1];
    boolean isRightInOrder = a[m] <= a[end];

    if (isLeftInOrder && isRightInOrder) {
      return m;
    }

    if (!isLeftInOrder) { return findK(a, start, m-1); }
    else { return findK(a, m, end); }
  }

  public static int bSearch(int[] a, int search, int k, int start, int end) {
    if (start > end) { return -1; }

    int middle = (start + end) / 2;
    int m = (middle + k) % a.length;

    if (a[m] == search) { return m; }

    if (a[m] < search) { return bSearch(a, search, k, middle+1, end); }
    else { return bSearch(a, search, k, start, middle-1); }
  }
}
