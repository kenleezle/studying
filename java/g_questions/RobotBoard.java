package g_questions;

import java.util.ArrayList;
import java.util.List;

public class RobotBoard {
  public static class Coord {
    public int row, col;

    public Coord(int row, int col) {
      this.row = row;
      this.col = col;
    }

    public Coord right() {
      return new Coord(this.row, this.col + 1);
    }

    public Coord down() {
      return new Coord(this.row + 1, this.col);
    }
  }

  public static List<String> findPath(boolean[][] board) {
    List<String> path = new ArrayList<String>();
    boolean[][] hasVisited = new boolean[board.length][board[0].length];

    if (DFS(board, new Coord(0,0), hasVisited, path)) { return path; }
    else { return null; }
  }

  public static boolean DFS(boolean[][] board, Coord coord, boolean[][] hasVisited, List<String> path) {
    // Out of bounds
    if (coord.row >= board.length || coord.col >= board[0].length) { return false; }

    // Obstacle
    if (!board[coord.row][coord.col]) { return false; }

    if (hasVisited[coord.row][coord.col]) { return false; }
    hasVisited[coord.row][coord.col] = true;


    // Success: Found Bottom Right!
    if (coord.row == board.length - 1 && coord.col == board[0].length - 1) {
      return true;
    }

    path.add("r");
    if (DFS(board, coord.right(), hasVisited, path)) {
      return true;
    } 
    path.remove(path.size() - 1);
    
    path.add("d");
    if (DFS(board, coord.down(), hasVisited, path)) {
      return true;
    }
    path.remove(path.size() - 1);

    return false;
  }
}
