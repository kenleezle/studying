package g_questions;

import java.util.List;
import java.util.ArrayList;
public class MedianCollection {
  public List<Integer> list;

  public MedianCollection() {
    list = new ArrayList<Integer>();
  }

  public void insert(Integer item) {
    int startIndex = 0;
    int endIndex = list.size();
    int length = endIndex - startIndex;
    int insertIndex = startIndex + length/2;

    while (length > 0) {
      if (item > list.get(insertIndex)) {
        startIndex = insertIndex + 1;
      } else {
        endIndex = insertIndex;
      }
      length = endIndex - startIndex;
      insertIndex = startIndex + length/2;
    }
    list.add(insertIndex, item);
  }

  public Integer getMedian() {
    if (list.size() == 0) {
      return null;
    } else {
      return list.get(list.size()/2);
    }
  }
}
