package g_questions;
import java.util.List;
import java.util.ArrayList;

public class DoublyLinkedList {
  public DLLNode head;
  public DLLNode tail;

  public DLLNode addToTail(Integer data) {
    DLLNode newNode = new DLLNode(data);
    if (head == null) {
      head = tail = newNode;
    } else {
      tail.next = newNode;
      newNode.prev = tail;
      tail = newNode;
    }
    return newNode;
  }

  public DLLNode removeFromTail() {
    if (tail == null) return null; //TODO throw exception?

    DLLNode nodeToReturn = tail;
    tail = tail.prev;
    nodeToReturn.prev = null;

    if (tail == null) head = null;

    return nodeToReturn;
  }

  public Integer peekAtTail() {
    return (tail == null) ? null : tail.data;
  }

  public void remove(DLLNode node) {
    if (node == head) {
      head = node.next;
      if (node.next != null) node.next.prev = null;
      node.next = null;
    } else {
      DLLNode prev = node.prev;
      prev.next = node.next;
      node.prev = null;

      DLLNode next = node.next;
      if (next != null) node.next.prev = node.prev;
      node.next = null;
    }
  }

  public void moveToTail(DLLNode node) {
    remove(node);
    // TODO wish this was actually the node
    addToTail(node.data);
  }

  public List<DLLNode> evict(long maxDurationInFeedMs) {
    List<DLLNode> nodesToEvict = new ArrayList<DLLNode>();
    DLLNode node = head;
    while (node != null && node.duration() >= maxDurationInFeedMs)
      nodesToEvict.add(node);

    for (DLLNode nodeToEvict : nodesToEvict)
      remove(nodeToEvict);

    return nodesToEvict;
  }
}
