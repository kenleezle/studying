package g_questions;

public class ArrayRotator {
  public static void rotate(int[] numbers, int k) {
    int[] temp = new int[k];
    int tempIndex = 0;

    for (int i = 0; i < numbers.length; i++) {
      if (i < k) {
        temp[tempIndex++] = numbers[i];
      } else {
        numbers[i-k] = numbers[i];
      }
    }

    tempIndex = 0;
    for (int i = numbers.length - k; i < numbers.length; i++) {
      numbers[i] = temp[tempIndex++];
    }
  }
}
