package g_questions;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class WordWithFriend
  implements Comparable<WordWithFriend> {

  private String word;
  private String wordSortedByChar;

  public static String sortWordByChar(String word) {
    String wordSortedByChar = "";
    List<Character> characters =
      new ArrayList<Character>(word.length());

    for (int i = 0; i < word.length(); i++) {
      characters.add(word.charAt(i));
    }

    Collections.sort(characters);

    for (Character c : characters) {
      wordSortedByChar += c;
    }

    return wordSortedByChar;
  }

  public WordWithFriend(String inWord) {
    word = inWord;
    wordSortedByChar = sortWordByChar(word);
  }

  public int compareTo(WordWithFriend otherWWF) {
    return wordSortedByChar.compareTo(otherWWF.wordSortedByChar);
  }

  public String getWord() {
    return word;
  }

}
