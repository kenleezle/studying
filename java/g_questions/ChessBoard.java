package g_questions;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;

// the board is laid out with
//  * (0,0) is the bottom left
//  * (n - 1, 0) is the bottom right
//  * (0, n - 1) is the top left
//  * (n - 1, n - 1) is the top right
public class ChessBoard {
  Set<Integer> occupiedColumns;
  Set<Integer> occupiedRows;
  Set<Line> occupiedDiagonals;

  final static boolean debug = false;

  public int dimension;

  private static void println(String str) {
    if (debug) {
      System.out.println(str);
    }
  }

  public static class Line {
    public int slope;
    public Coords coords;

    public Line(int slope, Coords coords) {
      this.slope = slope;
      this.coords = coords;
    }

    public boolean equals(Object other) {
      Line otherLine = (Line)other;
      return (slope == otherLine.slope) && (yIntercept() == otherLine.yIntercept());
    }

    public int yIntercept() {
      // y = m*x + b
      // b = y - m*x
      return coords.row - slope * coords.col;
    }

    public int hashCode() {
      return Objects.hash(slope, yIntercept());
    }
  }

  public static class Coords {
    public int col;
    public int row;
    public Coords(int col, int row) {
      this.col = col;
      this.row = row;
    }

    public String toString() {
      return "[" + col + "," + row + "]";
    }

    public Line upDiagonal() {
      return new Line(1, this);
    }

    public Line downDiagonal() {
      return new Line(-1, this);
    }
  }

  public ChessBoard(int dimension) {
    this.dimension = dimension;
    this.occupiedColumns = new HashSet<Integer>();
    this.occupiedRows = new HashSet<Integer>();
    this.occupiedDiagonals = new HashSet<Line>();

  }

  public List<List<Coords>> placeNQueens() {
    println("***************************************************");
    println("placeNQueens on board dimension: " + dimension);
    println("***************************************************");
    return placeNQueens(0);
  }

  private List<List<Coords>> placeNQueens(int col) {
    println("placeNQueens(" + col + ")");
    List<List<Coords>> solutions = new ArrayList<List<Coords>>();
    if (col >= dimension) {
      println("base case, col = " + col);
      solutions.add(new ArrayList<Coords>());
      return solutions;
    }

    for (int row = 0; row < dimension; row++) {
      Coords coords = new Coords(col, row);
      if (!isUnderAttack(coords)) {
        placeQueen(coords);

        for (List<Coords> partialSolution : placeNQueens(col + 1)) {
          partialSolution.add(coords);
          solutions.add(partialSolution);
        }
        removeQueen(coords);
      }
    }

    return solutions;
  }

  private boolean isUnderAttack(Coords coords) {
    boolean isUnderAttack = occupiedColumns.contains(coords.col)
      || occupiedRows.contains(coords.row)
      || occupiedDiagonals.contains(coords.upDiagonal())
      || occupiedDiagonals.contains(coords.downDiagonal());

    println("coordinates " + coords + " are " + (isUnderAttack ? "" : "not ") + "under attack");

    return isUnderAttack;
  }

  private void placeQueen(Coords coords) {
    println("placing queen at " + coords.toString());
    occupiedColumns.add(coords.col);
    occupiedRows.add(coords.row);
    occupiedDiagonals.add(coords.upDiagonal());
    occupiedDiagonals.add(coords.downDiagonal());
  }

  private void removeQueen(Coords coords) {
    occupiedColumns.remove(coords.col);
    occupiedRows.remove(coords.row);
    occupiedDiagonals.remove(coords.upDiagonal());
    occupiedDiagonals.remove(coords.downDiagonal());
  }

}
