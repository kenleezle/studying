package g_questions;

public class BitSet2 {
  byte[] data;

  public BitSet2(int size) {
    int bytes = size / 8;
    if (size % 8 > 0) { bytes += 1; }

    this.data = new byte[bytes];
  }

  private int mapIndex(int index) {
    return index / 8;
  }

  private byte mapMask(int index) {
    return (byte) ((index % 8) + 1);
  }

  public boolean get(int index) {
    return (data[mapIndex(index)] & mapMask(index)) != 0;
  }

  public void set(int index, boolean value) {
    byte mask = mapMask(index);
    index = mapIndex(index);

    data[index] = value ? (byte) (data[index] | mask) : (byte) (data[index] & ~mask); 
  }
}
