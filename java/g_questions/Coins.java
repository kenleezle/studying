package g_questions;

import java.util.HashMap;
import java.util.Objects;
public class Coins {
  static final int[] DEMOMINATIONS = {25,10,5,1};

  static class HashKey {
    int denomIndex;
    int n;

    public HashKey(int n, int denomIndex) {
      this.n = n;
      this.denomIndex = denomIndex;
    }

    public boolean equals(Object other) {
      HashKey otherHashKey = (HashKey) other;
      return denomIndex == otherHashKey.denomIndex && n == otherHashKey.n;
    }

    public int hashCode() {
      return Objects.hash(n, denomIndex);
    }
  }

  HashMap<HashKey, Integer> cache;

  public Coins() {
    cache = new HashMap<HashKey, Integer>();
  }

  public int waysToMakeChange(int n) {
    if (n < 0) {
      return 0;
    }
    return waysToMakeChange(n, 0);
  }

  private int waysToMakeChange(int n, int denomIndex) {
    if (denomIndex >= DEMOMINATIONS.length - 1) {
      return 1;
    }

    HashKey key = new HashKey(n, denomIndex);
    if (cache.containsKey(key)) {
      return cache.get(key);
    }

    int count = 0;
    int denom = DEMOMINATIONS[denomIndex];
    while (n >= 0) {
      count += waysToMakeChange(n, denomIndex + 1);
      n -= denom;
    }

    cache.put(key, count);
    return count;
  }
}

