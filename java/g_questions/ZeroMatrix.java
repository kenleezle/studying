package g_questions;

public class ZeroMatrix {
  public static void clearColumn(int[][] matrix, int i) {
    for (int j = 0; j < matrix[i].length; j++) {
      matrix[i][j] = 0;
    }
  }
  
  public static void clearRow(int[][] matrix, int j) {
    for (int i = 0; i < matrix.length; i++) {
      matrix[i][j] = 0;
    }
  }

  public static void fillZeroes(int[][] matrix) {
    int rowsToZeroi, columnsToZeroj;
    rowsToZeroi = columnsToZeroj = 0;
    boolean foundFirstZero = false;

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        if (matrix[i][j] == 0) {
          if (!foundFirstZero) {
            foundFirstZero = true;
            rowsToZeroi = i;
            columnsToZeroj = j;
          }
          matrix[rowsToZeroi][j] = 0;
          matrix[i][columnsToZeroj] = 0;
        }
      }
    }

    for (int i = 0; i < matrix.length; i++) {
      if (i == rowsToZeroi) { continue; }
      if (matrix[i][columnsToZeroj] == 0) { clearColumn(matrix, i); }
    }

    for (int j = 0; j < matrix[rowsToZeroi].length; j++) {
      if (matrix[rowsToZeroi][j] == 0) { clearRow(matrix, j); }
    }

    clearColumn(matrix, rowsToZeroi);
  }
}
