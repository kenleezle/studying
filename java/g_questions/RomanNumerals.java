package g_questions;

import java.util.List;
import java.util.ArrayList;

public class RomanNumerals {

  private static class RomanNumeral {
    public String s;
    public int value;
    public RomanNumeral(String s, int value) {
      this.s = s;
      this.value = value;
    }
    public int length() {
      return s.length();
    }
  }

  private static List<RomanNumeral> romanNumerals() {
    List<RomanNumeral> romanNumerals = new ArrayList<RomanNumeral>();
    romanNumerals.add(new RomanNumeral("M", 1000));
    romanNumerals.add(new RomanNumeral("CM", 900));
    romanNumerals.add(new RomanNumeral("D", 500));
    romanNumerals.add(new RomanNumeral("CD", 400));
    romanNumerals.add(new RomanNumeral("C", 100));
    romanNumerals.add(new RomanNumeral("XC", 90));
    romanNumerals.add(new RomanNumeral("L", 50));
    romanNumerals.add(new RomanNumeral("XL", 40));
    romanNumerals.add(new RomanNumeral("X", 10));
    romanNumerals.add(new RomanNumeral("IX", 9));
    romanNumerals.add(new RomanNumeral("V", 5));
    romanNumerals.add(new RomanNumeral("IV", 4));
    romanNumerals.add(new RomanNumeral("I", 1));
    return romanNumerals;
  }

  public static int romanToInt(String s) {
    int total = 0;
    int stringOffset = 0;
    while (stringOffset < s.length()) {
      boolean matchWasFound = false;
      for (RomanNumeral rn : romanNumerals()) {
        if (s.startsWith(rn.s, stringOffset)) {
          total += rn.value;
          stringOffset += rn.length();
          matchWasFound = true;
          break;
        }
      }
      if (!matchWasFound) {
        assert false : "invalid roman numeral: " + s;
      }
    }
    return total;
  }

  public static String nTimes(String s, int n) {
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < n; i++) {
      sb.append(s);
    }

    return sb.toString();
  }

  public static String intToRoman(int num) {
    if (num >= 1000) {
      int n = num / 1000;
      String prefix = nTimes("M", n);
      return prefix + intToRoman(num - n * 1000);
    }

    if (num >= 900) {
      return "CM" + intToRoman(num - 900);
    }
    if (num >= 500) {
      return "D" + intToRoman(num - 500);
    }
    if (num >= 400) {
      return "CD" + intToRoman(num - 400);
    }
    if (num >= 100) {
      int n = num / 100;
      String prefix = nTimes("C", n);
      return prefix + intToRoman(num - n * 100);
    }
    if (num >= 90) {
      return "XC" + intToRoman(num - 90);
    }
    if (num >= 50) {
      return "L" + intToRoman(num - 50);
    }
    if (num >= 40) {
      return "XL" + intToRoman(num - 40);
    }
    if (num >= 10) {
      int n = num / 10;
      String prefix = nTimes("X", n);
      return prefix + intToRoman(num - n * 10);
    }
    if (num >= 9) {
      return "IX" + intToRoman(num - 9);
    }
    if (num >= 5) {
      return "V" + intToRoman(num - 5);
    }
    if (num >= 4) {
      return "IV" + intToRoman(num - 4);
    }
    if (num >= 1) {
      int n = num / 1;
      String prefix = nTimes("I", n);
      return prefix + intToRoman(num - n * 1);
    }
    if (num == 0) {
      return "";
    }
    assert false : "invalid number: " + num;
    return "";
  }
}
