package g_questions;

import java.util.Objects;

public class Line {
  public double slope;
  public double yIntercept;

  public Line(Point a, Point b) {
    slope = (a.y - b.y) / (a.x - b.x);
    // y = mx + b
    // y - mx = b
    yIntercept = a.y - slope * a.x;
  }

  public boolean equals(Object other) {
    Line otherLine = (Line)other;
    return (slope == otherLine.slope) && (yIntercept == otherLine.yIntercept);
  }

  public int hashCode() {
    return Objects.hash(slope, yIntercept);
  }

  public String toString() {
    return "y = " + slope + "x + " + yIntercept;
  }
}

