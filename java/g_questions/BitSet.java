package g_questions;

// replace booleans with 4 bit chunks
public class BitSet {
  int[] store;
  public static final int SIZEOF_INT = 32;

  public BitSet(int bits) {
    int size = (bits == 0) ? 0 : (bits - 1)/32 + 1;
    store = new int[size];
  }

  public boolean get(int index) {
    int storeIndex = getStoreIndexForIndex(index);
    int bitIndex = getBitIndexForIndex(index);

    return getNthBitInInt(bitIndex, store[storeIndex]);
  }

  public void set(int index, boolean value) {
    int storeIndex = getStoreIndexForIndex(index);
    int bitIndex = getBitIndexForIndex(index);

    int newValue = setNthBitInInt(bitIndex, store[storeIndex], value);
    store[storeIndex] = newValue;
  }

  private int getStoreIndexForIndex(int index) {
    return index / SIZEOF_INT;
  }

  private int getBitIndexForIndex(int index) {
    return index % SIZEOF_INT;
  }

  private boolean getNthBitInInt(int n, int i) {
    int mask = mask(n);
    return (mask & i) != 0;
  }

  private int setNthBitInInt(int n, int i, boolean value) {
    int int1 = setNthBitInInt1(n, i, value);
    int int2 = setNthBitInInt2(n, i, value);

    assert int1 == int2;
    return int2;
  }

  private int setNthBitInInt1(int n, int i, boolean value) {
    if (getNthBitInInt(n, i) == value) {
      return i;
    }
    return i ^ mask(n);
  }

  private int setNthBitInInt2(int n, int i, boolean value) {
    int mask = mask(n);
    return value ? i | mask : i & ~mask;
  }

  private int mask(int n) {
    return 1 << n;
  }
}
