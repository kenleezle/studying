// For a binary tree in which the nodes have parent pointers:
// write a function that takes a node in the tree as input
// and returns the next node in the in-order traversal of that tree as output.
package g_questions;

public class NextNodeInOrder {
  public static Node leftMostNode(Node root) {
    if (root == null) {
      return null;
    } else if (root.leftChild == null) {
      return root;
    } else {
      return leftMostNode(root.leftChild);
    }
  }

  public static boolean isLeftChildOfParent(Node node) {
    return node.equals(node.parent.leftChild);
  }

  public static boolean isRightChildOfParent(Node node) {
    return node.equals(node.parent.rightChild);
  }

  public static Node firstAncestorOnHisLeftSide(Node node) {
    if (node == null) {
      return null;
    } else if (node.parent == null) {
      return null;
    } else if (isLeftChildOfParent(node)) {
      return node.parent;
    } else if (isRightChildOfParent(node)) {
      return firstAncestorOnHisLeftSide(node.parent);
    } else {
      System.out.println("I am not my parent's right or left child. What am I??");
      return null;
    }
  }

  public static Node nextNode(Node node) {
    if (node == null) {
      return null;
    }
    else if (node.parent == null) {
      return leftMostNode(node.rightChild);
    } else if (isLeftChildOfParent(node)) {
      Node leftMostNode = leftMostNode(node.rightChild);
      return (leftMostNode == null) ? node.parent : leftMostNode;
    } else if (isRightChildOfParent(node)) {
      Node leftMostNode = leftMostNode(node.rightChild);
      return (leftMostNode == null) ? firstAncestorOnHisLeftSide(node) : leftMostNode;
    } else {
      System.out.println("I am not my parent's right or left child. What am I??");
      return null;
    }
  }
}
