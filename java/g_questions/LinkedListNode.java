package g_questions;

import java.util.ArrayList;
import java.util.List;

public class LinkedListNode {
  public int data;
  public LinkedListNode next;

  public LinkedListNode(int data) {
    this.data = data;
  }

  public LinkedListNode(int data, LinkedListNode next) {
    this.data = data;
    this.next = next;
  }

  public static List<Integer> toList(LinkedListNode head) {
    List<Integer> list = new ArrayList<Integer>();

    while (head != null) {
      list.add(head.data);
      head = head.next;
    }

    return list;
  }
  public static LinkedListNode intersection(LinkedListNode h1, LinkedListNode h2) {
    int len1 = 0;
    int len2 = 0;

    LinkedListNode tmp = h1;
    while (tmp != null) {
      tmp = tmp.next;
      len1++;
    }

    tmp = h2;
    while (tmp != null) {
      tmp = tmp.next;
      len2++;
    }

    if (len1 > len2) {
      for (int i = 0; i < len1 - len2; i++) {
        h1 = h1.next;
      }
    } else if (len2 > len1) {
      for (int i = 0; i < len2 - len1; i++) {
        h2 = h2.next;
      }
    }

    while (h1 != null && h2 != null) {
      if (h1 == h2) {
        return h1;
      }
      h1 = h1.next;
      h2 = h2.next;
    }

    return null;
  }
}
