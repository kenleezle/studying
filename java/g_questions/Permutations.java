package g_questions;

import java.util.List;
import java.util.ArrayList;

// O((n+1)!)
public class Permutations {
  public static int numberOfOperations = 0;
  public static boolean debug = false;

  public static int factorial(int n) {
    if (n <= 1) {
      return n;
    }
    return n*factorial(n-1);
  }

  private static List<List<Integer>> permuteListWithInteger(List<Integer> list, Integer toInject) {
    int resultSize = factorial(list.size() + 1);
    List<List<Integer>> results = new ArrayList<List<Integer>>(resultSize);

    for (int insertIndex = 0; insertIndex <= list.size(); insertIndex++) {
      List<Integer> permutation = new ArrayList<Integer>(list.size() + 1);

      permutation.addAll(list.subList(0, insertIndex));
      permutation.add(toInject);
      permutation.addAll(list.subList(insertIndex, list.size()));

      numberOfOperations += list.size() + 1;

      results.add(permutation);
    }
    return results;
  }

  private static List<List<Integer>> permute(List<Integer> list) {
    int resultSize = factorial(list.size());
    List<List<Integer>> results = new ArrayList<List<Integer>>(resultSize);

    if (list.size() <= 1) {
      numberOfOperations += 1;
      results.add(list);
      return results;
    }

    List<List<Integer>> subListPermutations = permute(list.subList(1, list.size()));
    for (List<Integer> subListPermutation : subListPermutations) {
      results.addAll(permuteListWithInteger(subListPermutation, list.get(0)));
    }
    return results;
  }

  public static List<List<Integer>> permuteList(List<Integer> list) {
    numberOfOperations = 0;
    List<List<Integer>> permutations = permute(list);
    if (debug) System.out.println(list.size() + " items with " + numberOfOperations + " operations");
    return permutations;
  }
}
