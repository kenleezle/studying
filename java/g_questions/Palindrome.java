package g_questions;

public class Palindrome {
  public static boolean isPalindrome(String word) {
    if (word.length() <= 1) {
      return true;
    }
    int indexOfFirstChar = 0;
    int indexOfLastChar = word.length() - 1;

    char firstChar = word.charAt(indexOfFirstChar);
    char lastChar = word.charAt(indexOfLastChar);

    if (firstChar != lastChar) {
      return false;
    }

    return isPalindrome(word.substring(indexOfFirstChar + 1, indexOfLastChar));
  }
}
