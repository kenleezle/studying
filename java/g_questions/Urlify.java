package g_questions;

public class Urlify {
  public static String urlify(char[] url, int len) {
    int countSpaces = 0;
    for (int i = 0; i < len; i++) {
      if (url[i] == ' ') {
        countSpaces++;
      }
    }

    int toI = len + (2 * countSpaces) - 1;
    int fromI = len - 1;

    assert (toI + 1 <= url.length) : "not enough buffer at end of char[] url";

    while (fromI >= 0) {
      if (url[fromI] != ' ') {
        url[toI--] = url[fromI--];
      } else {
        url[toI--] = '0';
        url[toI--] = '2';
        url[toI--] = '%';
        fromI--;
      }
    }

    assert toI == -1 : "toI should go all the way to 0";
    return new String(url);
  }
}
