package g_questions;

import java.util.List;
import java.util.Collections;
import java.util.HashMap;

public class CountSummable {

  public static HashMap<Integer, Boolean> summableNumbers;

  public static int fastCount(List<Integer> list1, List<Integer> list2, int sum) {
    int countResult = 0;
    summableNumbers = new HashMap<Integer, Boolean>();
    for (Integer num : list2) {
      summableNumbers.put(sum - num, true);
    }
    for (Integer num : list1) {
      if (summableNumbers.get(num) != null) {
        countResult++;
      }
    }
    return countResult;
  }


  public static int count(List<Integer> list1, List<Integer> list2, int sum) {
    int countResult = 0;
    Collections.sort(list1);
    Collections.sort(list2);

    int list1Index = 0;
    int list2Index = list2.size() - 1;

    while (list1Index < list1.size() && list2Index >= 0) {
      int currentSum;
      while ((currentSum = list1.get(list1Index) + list2.get(list2Index)) > sum) {
        list2Index--;
        if (list2Index < 0) {
          return countResult;
        }
      }

      if (currentSum == sum) {
        countResult++;
      }
      list1Index++;
    }

    return countResult;
  }
}
