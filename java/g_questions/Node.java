package g_questions;

import java.util.List;
import java.util.ArrayList;

public class Node {
  public Node leftChild;
  public Node rightChild;
  public Node parent;
  public Integer data;

  public Node() {
  }

  public Node(Integer data) {
    this.data = data;
  }

  public boolean hasNoChildren() {
    return leftChild == null && rightChild == null;
  }

  public boolean hasOneChild() {
    return leftChild == null || rightChild == null;
  }

  public List<Node> children() {
    List<Node> myChildren = new ArrayList<Node>();

    if (leftChild != null) {
      myChildren.add(leftChild);
    }

    if (rightChild != null) {
      myChildren.add(rightChild);
    }

    return myChildren;
  }
}
