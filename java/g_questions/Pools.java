import java.io.*;
import java.util.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

class Solution {
  public int[][] land;
  private boolean[][] visited;
  
  public static final int WATER = 0;
  
  public Solution(int m, int n) {
    land = new int[m][n];
    visited = new boolean[m][n];
  }
  
  public class Pool {
    int size;
    int id;
    
    public Pool(int i, int s) {
      size = s;
      id = i;
    }
    
    public String toString() {
      return "[Pool Id: " + id + ", size: "+ size + "]";
    }
  }
  
  public List<Pool> countPools() {
    List<Pool> pools = new ArrayList<Pool>();
    
    for (int i = 0; i < land.length; i++) {
      for (int j = 0; j < land[0].length; j++) {
        if (visited[i][j]) { continue; }
        if (land[i][j] == WATER) {
          int poolSize = explorePool(i, j);
          pools.add(new Pool(pools.size(), poolSize));
        }
      }
    }
    
    return pools;
  }
  
  public int explorePool(int i, int j) {
    if (isOOB(i, j)) { return 0; }
    if (visited[i][j]) { return 0; }
    
    visited[i][j] = true;
    
    if (land[i][j] != WATER) { return 0; }
    
    int count = 1;
    
    for (int oi : new int [] {-1, 0, 1}) {
      for (int oj : new int [] {-1, 0, 1}) {
        count += explorePool(i + oi, j + oj);
      }
    }
    
    System.out.println("returning: "+ count);
    return count;
  }
  
  public boolean isOOB(int i, int j) {
    if (i < 0 || j < 0) { return true; }
    if (i >= land.length || j >= land[0].length) { return true; }
    return false;
  }
  
  public static void main(String[] args) {
    Solution s = new Solution(2, 4);
    s.land = new int[][] {
      new int[] {0, 0, 1, 0},
      new int[] {0, 0, 1, 0}
    };
    
    System.out.println(s.countPools());
  }
}

