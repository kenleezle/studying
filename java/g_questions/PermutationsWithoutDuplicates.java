package g_questions;

import java.util.List;
import java.util.ArrayList;

public class PermutationsWithoutDuplicates {
  public static boolean debug = false;

  public static List<String> buildPermutations(String s) {
    List<String> permutations = new ArrayList<String>();

    if (s.length() <= 1) {
      permutations.add(s);
      return permutations;
    }

    List<String> parts = buildPermutations(s.substring(1, s.length()));
    for (String part : parts) {
      permutations.add(s.charAt(0) + part);

      for (int i = 1; i < part.length(); i++) {
        permutations.add(part.substring(0, i) + s.charAt(0) + part.substring(i, part.length()));
      }

      permutations.add(part + s.charAt(0));
    }

    return permutations;
  }
  public static List<String> bPs(String s) {
    List<String> permutations = new ArrayList<String>();

    if (s.length() <= 1) {
      permutations.add(s);
      return permutations;
    }

    for (int i = 0; i < s.length(); i++) {
      char first = s.charAt(i);
      String n_1 = s.substring(0, i) + s.substring(i+1, s.length());

      for (String part : bPs(n_1)) {
        permutations.add(first + part);
      }
    }

    return permutations;
  }

}

