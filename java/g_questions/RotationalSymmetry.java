package g_questions;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

public class RotationalSymmetry {
  public static HashMap<Character, Character> partnerMap;
  public static HashMap<Integer, List<String>> allWordsForLength;

  public static void initStaticHashes() {
    if (partnerMap != null) {
      return;
    }

    partnerMap = new HashMap<Character, Character>();
    partnerMap.put('0', '0');
    partnerMap.put('6', '9');
    partnerMap.put('8', '8');
    partnerMap.put('9', '6');


    allWordsForLength = new HashMap<Integer, List<String>>();

    List<String> lengthZero = new ArrayList<String>();
    lengthZero.add("");
    allWordsForLength.put(0, lengthZero);

    List<String> lengthOne = new ArrayList<String>();
    lengthOne.add("0");
    lengthOne.add("8");
    allWordsForLength.put(1, lengthOne);
  }

  public static boolean isSymmetric(String word) {
    initStaticHashes();

    if (word.length() == 0) {
      return true;
    } else if (word.length() == 1) {
      Character c = word.charAt(0);
      return c == partnerMap.get(c);
    } else {
      Character firstChar = word.charAt(0);
      Character lastChar = word.charAt(word.length()-1);
      if (lastChar != partnerMap.get(firstChar)) {
        return false;
      }
      return isSymmetric(word.substring(1, word.length()-1));
    }
  }

  public static List<String> wordsForLength(int length) {
    if (length == 0 || length == 1) {
      return allWordsForLength.get(length);
    }

    for (int currLength = 2; currLength <= length; currLength++) {
      List<String> wordsForCurrLength = new ArrayList<String>();

      for (String word : allWordsForLength.get(currLength - 2)) {
        for (Character rotSymChar : partnerMap.keySet()) {
          Character partnerChar = partnerMap.get(rotSymChar);
          wordsForCurrLength.add(rotSymChar + word + partnerChar);
        }
        allWordsForLength.put(currLength, wordsForCurrLength);
      }
    }
    return allWordsForLength.get(length);
  }
}
