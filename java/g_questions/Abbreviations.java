package g_questions;

import java.util.HashMap;
import java.util.List;

public class Abbreviations {
  private static HashMap<String, Integer> abbrevCount;

  public static boolean hasUniqueAbbrev(String word, List<String> words) {
    initAbbrevCount(words);
    return abbrevCount.get(getAbbrev(word)) == 1;
  }
  private static void initAbbrevCount(List<String> words) {
    if (abbrevCount != null) {
      return;
    }

    abbrevCount = new HashMap<String, Integer>();
    for (String word : words) {
      String abbrev = getAbbrev(word);
      Integer count = abbrevCount.get(abbrev);
      count = (count == null) ? 1 : count + 1;
      abbrevCount.put(abbrev, count);
    }
  }
  private static String getAbbrev(String word) {
    if (word.length() < 3) {
      return word;
    }

    return "" + word.charAt(0) + (word.length() - 2) + word.charAt(word.length()-1);
  }
}
