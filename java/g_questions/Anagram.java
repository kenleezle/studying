package g_questions;

import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

public class Anagram {
  public static String sortCharsInWord(String word) {
    char[] sortedChars = word.toCharArray();
    Arrays.sort(sortedChars);
    return new String(sortedChars);
  }
  public static List<List<String>> groupAnagramsUsingHash(List<String> words) {
    HashMap<String, List<String>> sortedWordToWords = new HashMap<String, List<String>>();
    List<List<String>> groups = new ArrayList<List<String>>();

    for (String word : words) {
      String sortedWord = sortCharsInWord(word);
      List<String> group = sortedWordToWords.get(sortedWord);
      if (group == null) {
        group = new ArrayList<String>();
        groups.add(group);
        sortedWordToWords.put(sortedWord, group);
      }
      group.add(word);
    }
    return groups;
  }
  public static List<List<String>> groupAnagrams(List<String> words) {
    List<WordWithFriend> wordsWithFriends =
      new ArrayList<WordWithFriend>(words.size());

    for (String word : words) {
      wordsWithFriends.add(new WordWithFriend(word));
    }

    Collections.sort(wordsWithFriends);

    List<List<String>> groupedAnagrams =
      new ArrayList<List<String>>();
    List<String> currentGroup = null;
    WordWithFriend prevWWF = null;

    for (WordWithFriend wwf : wordsWithFriends) {
      boolean anagramGroupChanged =
        ((prevWWF == null) || (prevWWF.compareTo(wwf) != 0));

      if (anagramGroupChanged) {
        currentGroup = new ArrayList<String>();
        groupedAnagrams.add(currentGroup);
        prevWWF = wwf;
      }
      currentGroup.add(wwf.getWord());
    }

    return groupedAnagrams;
  }
}
