package g_questions;

import java.util.List;

public class MaxSumSequence {
  public static Sequence findSequence(List<Integer> list) {
    Sequence maxSeq = new Sequence();
    Sequence compSeq = new Sequence();

    int index = 0;
    for (Integer value : list) {
      compSeq.end = index + 1;
      compSeq.sum += value;

      if (compSeq.sum > maxSeq.sum) {
        maxSeq.sum = compSeq.sum;
        maxSeq.start = compSeq.start;
        maxSeq.end = compSeq.end;
      }

      if (compSeq.sum <= 0) {
        compSeq.restartAtIndex(index + 1);
      }

      index++;
    }

    int maxSum = findMaxSum(list);
    if (maxSum >= 0) {
      assert maxSeq.sum == maxSum : "max sum doesn't agree with max seq";
    }

    return maxSeq;
  }

  public static int findMaxSum(List<Integer> nums) {
    int bSum = 0;
    int cSum = 0;
    int greatestNum = Integer.MIN_VALUE;

    for (int num : nums) {
      cSum += num;

      if (cSum < 0) {
        cSum = 0;
      }

      if (cSum > bSum) {
        bSum = cSum;
      }

      if (num > greatestNum) {
        greatestNum = num;
      }
    }

    return (bSum == 0) ? greatestNum : bSum;
  }
}
