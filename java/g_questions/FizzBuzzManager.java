package g_questions;

import java.util.List;
import java.util.ArrayList;

public class FizzBuzzManager {
  public int maxCount;
  public List<String> results;

  public FizzBuzzManager(int count) {
    this.maxCount = count;
    this.results = new ArrayList<String>();
  }

  public synchronized void registerOutput(String output) {
    results.add(output);
  }

  public int count() {
    return results.size();
  }

  public int maxCount() {
    return maxCount;
  }

  public List<String> results() {
    if (isNotFinished()) {
      run();
    }
    return results;
  }

  public boolean isNotFinished() {
    return count() < maxCount;
  }

  private void run() {
    Thread fizz = new Thread(new Fizz(this));
    fizz.start();

    Thread buzz = new Thread(new Buzz(this));
    buzz.start();

    Thread fizzbuzz = new Thread(new FizzBuzz(this));
    fizzbuzz.start();

    Thread number = new Thread(new Number(this));
    number.start();

    try {
      fizz.join();
      buzz.join();
      fizzbuzz.join();
      number.join();
    } catch (InterruptedException e) {
      System.out.println("interrupted: " + e.toString());
    }
  }

  public class Fizz implements Runnable {
    public FizzBuzzManager manager;

    public Fizz(FizzBuzzManager manager) {
      this.manager = manager;
    }

    public void run() {
      int count = manager.count();
      while (manager.isNotFinished()) {
        if (count % 3 == 0 && count % 5 != 0) {
          manager.registerOutput("fizz");
        }
        count = manager.count();
      }
    }
  }

  public class Buzz implements Runnable {
    public FizzBuzzManager manager;

    public Buzz(FizzBuzzManager manager) {
      this.manager = manager;
    }

    public void run() {
      int count = manager.count();
      while (manager.isNotFinished()) {
        if (count % 3 != 0 && count % 5 == 0) {
          manager.registerOutput("buzz");
        }
        count = manager.count();
      }
    }
  }

  public class FizzBuzz implements Runnable {
    public FizzBuzzManager manager;

    public FizzBuzz(FizzBuzzManager manager) {
      this.manager = manager;
    }

    public void run() {
      int count = manager.count();
      while (manager.isNotFinished()) {
        if (count % 3 == 0 && count % 5 == 0) {
          manager.registerOutput("fizzbuzz");
        }
        count = manager.count();
      }
    }
  }

  public class Number implements Runnable {
    public FizzBuzzManager manager;

    public Number(FizzBuzzManager manager) {
      this.manager = manager;
    }

    public void run() {
      int count = manager.count();
      while (manager.isNotFinished()) {
        if (count % 3 != 0 && count % 5 != 0) {
          manager.registerOutput(Integer.toString(count));
        }
        count = manager.count();
      }
    }
  }
}
