package g_questions;

import java.lang.StringBuilder;

public class StringCompression {
  public static String compress(String input) {
    if (input.length() <= 2) {
      return input;
    }

    int count = 0;
    char cur = input.charAt(0);
    StringBuilder output = new StringBuilder();

    for (int i = 0; i < input.length(); i++) {
      char c = input.charAt(i);
      if (c != cur) {
        output.append(cur);
        output.append(count);

        count = 1;
        cur = c;
      } else { count++; }
    }
    output.append(cur);
    output.append(count);

    if (input.length() <= output.length()) { return input; }
    else { return output.toString(); }
  }
}
