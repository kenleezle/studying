package g_questions;

import java.util.HashMap;
public class Fibonacci {
  private static HashMap<Integer, Integer> table;

  public static void initTable() {
    // Include this guard for optimization
    // For testing i commented it out
    // if (table != null) {
    //   return;
    // }
    table = new HashMap<Integer, Integer>();
    table.put(0, 0);
    table.put(1, 1);
  }

  public static int nthRecursive(int n) {
    initTable();
    return myNthRecursive(n);
  }

  private static int myNthRecursive(int n) {
    Integer nth = table.get(n);
    if (nth != null) {
      return nth;
    }

    return maybeCachedNth(n-1) + maybeCachedNth(n-2);
  }

  private static int maybeCachedNth(int n) {
    Integer nth = table.get(n);
    if (nth == null) {
      nth = myNthRecursive(n);
      table.put(n, nth);
    }
    return nth;
  }

  public static int nthIterative(int n) {
    initTable();
    for (int i = 0; i <= n; i++) {
      boolean alreadyCalculated = (table.get(i) != null);

      if (!alreadyCalculated) {
        int i_1 = table.get(i-1);
        int i_2 = table.get(i-2);
        int ith = i_1 + i_2;

        table.put(i, ith);
      }
    }
    return table.get(n);
  }

}
