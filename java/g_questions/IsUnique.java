package g_questions;
import java.util.BitSet;
import java.util.Arrays;

public class IsUnique {
  public static boolean hasAllUniqueChars(String s) {
    char[] chars = s.toCharArray();
    Arrays.sort(chars);

    if (chars.length == 0) {
      return true;
    }

    char last = chars[0];

    for (int i = 1; i < chars.length; i++) {
      char c = chars[i];

      if (c == last) {
        return false;
      }
      last = c;
    }
    return true;
  }

  public static boolean hasAllUniqueChars2(String s) {
    BitSet bs = new BitSet(256);
    for (int c : s.toCharArray()) {
      if (bs.get(c)) {
        return false;
      }
      bs.flip(c);
    }
    return true;
  }

  public static boolean hasAllUniqueChars3(String s) {
    char[] chars = s.toCharArray();
    Arrays.sort(chars);

    char last = '\0';

    for (char c : chars) {
      if (c == last) {
        return false;
      }
      last = c;
    }
    return true;
  }


}
