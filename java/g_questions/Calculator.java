package g_questions;

import java.util.Scanner;
class NumberNode implements CalcNode {
  public int number;

  public NumberNode(int n) {
    number = n;
  }

  public int getValue() {
    return number;
  }
}
class OperatorNode implements CalcNode {
  public String operator;
  public CalcNode operand1;
  public CalcNode operand2;

  public OperatorNode(CalcNode o1, String o, CalcNode o2) {
    operand1 = o1;
    operator = o;
    operand2 = o2;
  }

  public int getValue() {
    switch(operator) {
      case "+":
        return operand1.getValue() + operand2.getValue();
      case "*":
        return operand1.getValue() * operand2.getValue();
      case "-":
        return operand1.getValue() - operand2.getValue();
      case "/":
        return operand1.getValue() / operand2.getValue();
    }
    throw new RuntimeException("unexpected operator: " + operator);
  }
}
public class Calculator {
  public static CalcNode E(Scanner expr) {
    CalcNode node;
    CalcNode operand1 = F(expr);
    if (expr.hasNext("[+-]")) {
      node = new OperatorNode(operand1, expr.next("[+-]"), E(expr));
    } else {
      node = operand1;
    }
    return node;
  }
  public static CalcNode F(Scanner expr) {
    CalcNode node;
    CalcNode operand1 = N(expr);
    if (expr.hasNext("[*/]")) {
      node = new OperatorNode(operand1, expr.next("[*/]"), F(expr));
    } else {
      node = operand1;
    }
    return node;
  }
  public static CalcNode N(Scanner expr) {
    CalcNode node = null;
    if (expr.hasNextInt()) {
      node = new NumberNode(expr.nextInt());
    } else if (expr.hasNext("[(]")) {
      expr.next("[(]");
      node = E(expr);
      if (!expr.hasNext("[)]")) throw new RuntimeException("failure to parse N!");
      expr.next("[)]");
    } else {
      throw new RuntimeException("failure to parse N("+expr.nextLine()+")");
    }
    return node;
  }
  public static int calc(String expr) {
    return 0;
  }
}
