package g_questions;

import java.util.HashMap;

public class WaysToStepCounter {
  public static boolean isOptimizationEnabled = false;
  public static HashMap<Integer,Integer> stepsToWaysToStep;
  public static int numberOfOperations;
  public static boolean debug = false;

  public static int waysToStep(int numberOfSteps) {
    numberOfOperations = 0;
    stepsToWaysToStep = new HashMap<Integer, Integer>();
    int result = countWaysToStep(numberOfSteps);
    if (debug) System.out.println("steps = " + numberOfSteps + " number of operations - " + numberOfOperations);
    return result;
  }

  private static int countWaysToStep(int numberOfSteps) {
    if (isOptimizationEnabled) {
      Integer waysToStep = stepsToWaysToStep.get(numberOfSteps);
      numberOfOperations += 1;
      if (waysToStep != null) {
        return waysToStep;
      }
    }

    if (numberOfSteps <= 2) {
      numberOfOperations += 1;
      return numberOfSteps;
    } else {
      numberOfOperations += 1;
      int result = countWaysToStep(numberOfSteps - 1) + countWaysToStep(numberOfSteps - 2);
      stepsToWaysToStep.put(numberOfSteps, result);
      return result;
    }
  }

  public static int waysToStep2(int n) {
    int c, c1, c2, c3;
    c = c1 = c2 = c3 = 0;
    c1 = 1;

    for (int i = 0; i < n; i++) {
      c = c1 + c2 + c3;

      c3 = c2;
      c2 = c1;
      c1 = c;
    }

    return c;
  }
}
