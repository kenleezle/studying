package g_questions;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class PaintFill {
  public static final int SIZE = 10;
  int[][] canvas;

  public PaintFill() {
    canvas = new int[SIZE][SIZE];
  }

  public void setCanvas(int[][] canvas) {
    this.canvas = canvas;
  }

  public int[][] getCanvas() {
    return canvas;
  }

  public static List<List<Integer>> pointsAroundPoint(int x, int y) {
    List<List<Integer>> points = new ArrayList<List<Integer>>();

    points.add(Arrays.asList(x - 1, y - 1));
    points.add(Arrays.asList(x - 1, y));
    points.add(Arrays.asList(x - 1, y + 1));

    points.add(Arrays.asList(x, y - 1));
    // exclude the point itself
    // points.add(Arrays.asList(x, y));
    points.add(Arrays.asList(x, y + 1));

    points.add(Arrays.asList(x + 1, y - 1));
    points.add(Arrays.asList(x + 1, y));
    points.add(Arrays.asList(x + 1, y + 1));

    return points;
  }

  // Fill area surrounding x and y until color changes
  public void fill(int x, int y, int color) {
    if (isOOB(x, y)) return;

    fill(x, y, color, canvas[x][y]);
  }

  private void fill(int x, int y, int color, int matchColor) {
    if (isOOB(x, y)) return;
    if (canvas[x][y] != matchColor) return;

    canvas[x][y] = color;

    for (List<Integer> point : pointsAroundPoint(x, y)) {
      fill(point.get(0), point.get(1), color, matchColor);
    }
  }

  public static boolean isOOB(int x, int y) {
    return x < 0 || x >= SIZE || y < 0 || y >= SIZE;
  }
}
