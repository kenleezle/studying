package g_questions;

public class DCA {
  public static Node findDCA(Node root, Node elem1, Node elem2) {
    DCAIntermediateResult result = new DCAIntermediateResult();
    findDCAInternal(root, elem1, elem2, result);
    return result.dca;
  }

  private static void findDCAInternal(Node root, Node elem1, Node elem2, DCAIntermediateResult result) {
    if (root == null) {
      return;
    }

    boolean anAncestorIsDCA = result.hasFoundEitherElement();

    if (root.equals(elem1)) {
      result.hasFoundElement1 = true;
    } else if (root.equals(elem2)) {
      result.hasFoundElement2 = true;
    }
    if (result.hasFoundBothElements()) {
      return;
    }

    findDCAInternal(root.leftChild, elem1, elem2, result);
    if (result.isReadyForDCA() && !anAncestorIsDCA) {
      result.dca = root;
      return;
    }

    findDCAInternal(root.rightChild, elem1, elem2, result);
    if (result.isReadyForDCA() && !anAncestorIsDCA) {
      result.dca = root;
      return;
    }
  }

  public static Node findDCAWithLocalReturn(Node root, Node elem1, Node elem2) {
    DCAIntermediateResult result = findDCAInternalWithLocalReturn(root, elem1, elem2);
    return result.dca;
  }

  private static DCAIntermediateResult findDCAInternalWithLocalReturn(Node root, Node elem1, Node elem2) {
    DCAIntermediateResult result = new DCAIntermediateResult();

    if (root == null) {
      return result;
    }

    if (root.equals(elem1)) {
      result.hasFoundElement1 = true;
    } else if (root.equals(elem2)) {
      result.hasFoundElement2 = true;
    }

    DCAIntermediateResult leftResult = findDCAInternalWithLocalReturn(root.leftChild, elem1, elem2);
    result.incorporateResult(leftResult);

    if (result.isReadyForDCA()) {
      result.dca = root;
    }

    if (result.hasFoundDCA()) {
      return result;
    }

    DCAIntermediateResult  rightResult = findDCAInternalWithLocalReturn(root.rightChild, elem1, elem2);
    result.incorporateResult(rightResult);

    if (result.isReadyForDCA()) {
      result.dca = root;
    }

    return result;
  }
}
