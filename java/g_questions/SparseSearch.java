package g_questions;

import java.util.List;

public class SparseSearch {
  public static int getNextNonSpaceLeft(List<String> words, int start, int lo) {
    for (int i = start; i >= lo; i--) {
      if (!words.get(i).equals("")) {
        return i;
      }
    }
    return -1;
  }
  public static int getNextNonSpaceRight(List<String> words, int start, int hi) {
    for (int i = start; i <= hi; i++) {
      if (!words.get(i).equals("")) {
        return i;
      }
    }
    return -1;
  }

  public static int find(List<String> words, String search) {
    int lo = 0;
    int hi = words.size() - 1;

    while (lo <= hi) {
      int m = (lo + hi) / 2;
      if (!words.get(m).equals("")) {
        int comp = words.get(m).compareTo(search);
        if (comp == 0) {
          return m;
        } else if (comp < 0) {
          lo = m + 1;
        } else {
          hi = m - 1;
        }
      } else { // encountered a space
        int lm = getNextNonSpaceLeft(words, m, lo);
        if (lm != -1) {
          int comp = words.get(lm).compareTo(search);
          if (comp == 0) {
            return lm;
          } else if (comp > 0) {
            // lm > search
            hi = lm - 1;
            continue;
          } else {
            // lm < search
          }
        }
        int hm = getNextNonSpaceRight(words, m, hi);
        if (hm != -1) {
          int comp = words.get(hm).compareTo(search);
          if (comp == 0) {
            return hm;
          } else if (comp < 0) {
            // hm < search
            lo = hm + 1;
            continue;
          } else {
            // hm > search
          }
        }
        return -1;
      }
    }
    return -1;
  }
}
