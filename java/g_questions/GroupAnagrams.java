package g_questions;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

public class GroupAnagrams {
  public static List<List<String>> groupAnagrams(List<String> words) {
    HashMap<String, List<String>> groups = new HashMap<String, List<String>>();

    for (String word : words) {
      char[] letters = word.toCharArray();
      Arrays.sort(letters);

      String sortedWord = new String(letters);

      List<String> group = groups.get(sortedWord);
      if (group == null) {
        group = new ArrayList<String>();
        groups.put(sortedWord, group);
      }

      group.add(word);
    }

    List<List<String>> result = new ArrayList<List<String>>();
    for (String sortedWord : groups.keySet()) {
      List<String> group = groups.get(sortedWord);
      result.add(group);
    }
    return result;
  }
}
