package g_questions;

public class BinaryRep {
  final static int SIZE_OF_INT = 31;
  final static String ERROR = "ERROR";

  public static String fractionToString(double n) {
    if (n > 1 || n < 0) {
      return ERROR;
    }

    StringBuilder sb = new StringBuilder();
    sb.append("0.");

    while (n > 0) {
      n *= 2;

      if (n >= 1) {
        sb.append("1");
        n -= 1;
      } else {
        sb.append("0");
      }

      if (sb.length() > SIZE_OF_INT) {
        return ERROR;
      }
    }

    return sb.toString();
  }


  public static String binaryRep(int n) {
    if (n == 0) return "0";
    return n > 0 ? binaryRepForPositiveNum(n) : binaryRepForNegativeNum(n);
  }

  public static String binaryRepForNegativeNum(int n) {
    assert n < 0 : "only works for negative numbers";

    int complement = (int) Math.pow(2,SIZE_OF_INT-1) + n;
    return "1" + binaryRepForPositiveNum(complement);
  }

  public static String binaryRepForPositiveNum(int n) {
    String rep1 = binaryRepForPositiveNum1(n);
    String rep2 = binaryRepForPositiveNum2(n);
    assert rep1.equals(rep2) : "binary reps for positive numbers don't match";

    return rep1;
  }
  public static String binaryRepForPositiveNum1(int n) {
    assert n > 0 : "only works for positive numbers";

    int pow = 2;
    StringBuilder sb = new StringBuilder();

    while (n > 0) {
      int remainder = n % pow;
      if (remainder > 0) {
        sb.insert(0, '1');
        n -= remainder;
      } else {
        sb.insert(0, '0');
      }
      pow *= 2;
    }

    return sb.toString();
  }

  public static String binaryRepForPositiveNum2(int n) {
    StringBuilder sb = new StringBuilder();
    while (n > 0) {
      if (n % 2 > 0) {
        sb.insert(0,'1');
      } else {
        sb.insert(0,'0');
      }

      n /= 2;
    }
    return sb.toString();
  }
}
