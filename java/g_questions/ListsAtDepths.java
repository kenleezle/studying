package g_questions;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
public class ListsAtDepths {
  public static LinkedList<Node> allChildren(LinkedList<Node> nodes) {
    LinkedList<Node> children = new LinkedList<Node>();
    for (Node node : nodes) {
      children.addAll(node.children());
    }
    return children;
  }
  public static List<LinkedList<Node>> getLists(Node root) {
    List<LinkedList<Node>> lists = new ArrayList<LinkedList<Node>>();
    if (root == null) {
      return lists;
    }

    LinkedList<Node> current = new LinkedList<Node>();
    current.add(root);

    while (!current.isEmpty()) {
      lists.add(current);
      current = allChildren(current);
    }

    return lists;
  }
}
