package g_questions;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

public class CountUrlsOnSite {
  public static Set<String>visitedUrls;

  public static List<String> urlsOnPage(String pageUrl) {
    return new ArrayList<String>();
  }

  public static int urlsOnSite(String siteUrl) {
    visitedUrls = new HashSet<String>();
    DFSSite(siteUrl);
    return visitedUrls.size();
  }

  public static void DFSSite(String siteUrl) {
    if (visitedUrls.contains(siteUrl)) {
      return;
    }

    visitedUrls.add(siteUrl);

    for(String url : urlsOnPage(siteUrl)) {
      DFSSite(url);
    }
  }
}
