package g_questions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class LineWithMostPoints {
  public static Line lineWithMostPoints(List<Point> points) {
    HashMap<Line, HashSet<Point>> line2points = new HashMap<Line, HashSet<Point>>();

    for (int i = 0; i < points.size(); i++) {
      for (int j = i+1; j < points.size(); j++) {
        Line line = new Line(points.get(i), points.get(j));

        HashSet<Point> pointSet = line2points.get(line);
        if (pointSet == null) {
          pointSet = new HashSet<Point>();
          line2points.put(line, pointSet);
        }
        pointSet.add(points.get(i));
        pointSet.add(points.get(j));
      }
    }

    int maxCount = 0;
    Line maxLine = null;
    for (Line line : line2points.keySet()) {
      HashSet<Point> pointSet = line2points.get(line);

      if (pointSet.size() > maxCount) {
        maxCount = pointSet.size();
        maxLine = line;
      }
    }

    return maxLine;
  }
}
