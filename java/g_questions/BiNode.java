package g_questions;

public class BiNode {
  public int data;
  public BiNode node1;
  public BiNode node2;

  public BiNode(int d) {
    data = d;
  }

  public boolean isLeaf() {
    return node1 == null && node2 == null;
  }

  public static void convertFromTree(BiNode root) {
    convertFromTree(root, null, null);
  }

  private static void convertFromTree(BiNode node, BiNode prev, BiNode next) {
    if (node == null) return;
    if (node.isLeaf()) {
      node.node1 = prev;
      if (prev != null) prev.node2 = node;
      node.node2 = next;
      if (next != null) next.node1 = node;
      return;
    }
    convertFromTree(node.node1, prev, node);
    convertFromTree(node.node2, node, next);
  }

  public static String toStringAsLL(BiNode head) {
    if (head == null) return "";
    return head.data + "," + toStringAsLL(head.node2);
  }
}
