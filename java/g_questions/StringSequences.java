package g_questions;

import java.util.List;
import java.util.ArrayList;

public class StringSequences {
  public static List<String> sequences(String a, String b) {
    List<String> result = new ArrayList<String>();

    if (a.length() == 0) {
      result.add(b);
      return result;
    } else if (b.length() == 0) {
      result.add(a);
      return result;
    }

    for (String part : sequences(a.substring(1, a.length()), b)) {
      result.add(a.charAt(0) + part);
    }
    for (String part : sequences(a, b.substring(1, b.length()))) {
      result.add(b.charAt(0) + part);
    }

    return result;
  }
}
