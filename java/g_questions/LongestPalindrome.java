package g_questions;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

public class LongestPalindrome {
  public static String longestPalindrome(String a) {
      if (a.length() == 0) {
          return a;
      }
      if (a.length() == 1) {
          return a;
      }
      if (a.length() == 2) {
          return (a.charAt(0) == a.charAt(1)) ? a : a.substring(0,1);
      }

      HashMap<Integer, List<Integer>> e2s = new HashMap<Integer, List<Integer>>();
      int bStart, bEnd;
      bStart = bEnd = 0;

      // todo: aalksjdfkljasdfkj
      for (int i = 2; i < a.length(); i++) {
          char curr = a.charAt(i);
          List<Integer> myStarts = new ArrayList<Integer>();
          List<Integer> starts = e2s.get(i-1);
          if (starts != null) {
              for (Integer start : starts) {
                  int myStart = start - 1;
                  if (myStart < 0) {
                      continue;
                  }

                  if (a.charAt(myStart) == curr) {
                      myStarts.add(myStart);
                      if (i - myStart > bEnd - bStart) {
                           bEnd = i;
                           bStart = myStart;
                      }
                  }
              }
          }
          if (curr == a.charAt(i-2)) {
              myStarts.add(i-2);
              if (2 > bEnd - bStart) {
                  bEnd = i;
                  bStart = i-2;
              }
          }
          if (curr == a.charAt(i-1)) {
              myStarts.add(i-1);
              if (1 > bEnd - bStart) {
                  bEnd = i;
                  bStart = i-1;
              }
          }

          if (!myStarts.isEmpty()) {
              e2s.put(i, myStarts);
          }
      }

      if (bStart == 0 && bEnd == 0) {
          return (a.charAt(0) == a.charAt(1)) ? a.substring(0,2) : a.substring(0,1);
      }

      return a.substring(bStart, bEnd + 1);
  }
}

