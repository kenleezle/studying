package g_questions;

import java.util.List;
import java.util.ArrayList;
import java.util.PriorityQueue;

public class LargestXNumbers {

  public List<Integer> numbers;
  public PriorityQueue<Integer> largestXNumbers;
  public int x;

  public LargestXNumbers(List<Integer> numbers, int x) {
    this.numbers = numbers;
    this.x = x;
    largestXNumbers = new PriorityQueue<Integer>(x);
  }

  public List<Integer> getLargestNumbers() {
    for (Integer number : numbers) {
      proposeCandidate(number);
    }

    return new ArrayList<Integer>(largestXNumbers);
  }

  public void proposeCandidate(Integer candidate) {
    if (largestXNumbers.size() < x) {
      largestXNumbers.add(candidate);
      return;
    }

    Integer currentMin = largestXNumbers.peek();
    if (candidate > currentMin) {
      largestXNumbers.remove();
      largestXNumbers.add(candidate);
      return;
    }
  }
}
