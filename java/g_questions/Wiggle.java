package g_questions;

import java.util.List;
public class Wiggle {
  static boolean debug = false;
  public static int countLongestWSS(List<Integer> nums) {
    int length = 0;
    boolean isFirst = true;
    boolean isSecond = false;
    boolean dirChanged;
    boolean isAsc, wasAsc;
    isAsc = wasAsc = false;

    int last, diff;
    last = diff = 0;

    if (debug) System.out.println("********************");
    for (Integer num : nums) {
      diff = num - last;
      isAsc = diff > 0;

      if (isSecond) {
        wasAsc = !isAsc;
        isSecond = false;
      }

      dirChanged = (isAsc != wasAsc);

      if (debug) System.out.println("num: " + num + ", diff: " + diff + ", isAsc: " + isAsc + ", wasAsc: " + wasAsc + ", dirChanged: " + dirChanged);

      if (dirChanged || isFirst) {
        length++;
      }

      if (isFirst) {
        isSecond = true;
        isFirst = false;
      }
      wasAsc = isAsc;
      last = num;
    }
    if (debug) System.out.println("********************");
    return length;
  }
}
