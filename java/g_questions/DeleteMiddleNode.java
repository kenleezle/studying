package g_questions;

public class DeleteMiddleNode {
  public static boolean deleteMiddleNodeLinear(LinkedListNode node) {
    if (node == null || node.next == null) {
      return false;
    }

    LinkedListNode last = node;
    LinkedListNode next = node.next;

    while (next.next != null) {
      last.data = next.data;
      last = next;
      next = next.next;
    }

    last.data = next.data;
    last.next = null;

    return true;
  }

  public static boolean deleteMiddleNodeConstant(LinkedListNode node) {
    if (node == null || node.next == null) {
      return false;
    }

    node.data = node.next.data;
    node.next = node.next.next;

    return true;
  }

}
