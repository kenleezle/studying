package g_questions;

import java.util.HashMap;

public class FirstNonRepeatingCharacter {

  private static HashMap<Character, Integer> getLetterFrequencyMap(String word) {
    HashMap<Character, Integer> freqMap = new HashMap<Character, Integer>();

    for (char letter : word.toCharArray()) {
      Integer freq = freqMap.get(letter);

      boolean isFirstOccurence = freq == null;

      freq = isFirstOccurence ? 1 : freq + 1;

      freqMap.put(letter, freq);
    }

    return freqMap;
  }

  public static Character getChar(String word) {
    HashMap<Character, Integer> freqMap = getLetterFrequencyMap(word);
    for (char letter : word.toCharArray()) {
      if (freqMap.get(letter) == 1) {
        return letter;
      }
    }
    return null;
  }
}
