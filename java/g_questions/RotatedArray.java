package g_questions;

public class RotatedArray {
  public static int find(int search, int[] a) {
    int rotDeg = findRotDeg(0, a.length - 1, a);
    int find1 = findInRotArr1(rotDeg, rotDeg + a.length - 1, search, a);
    int find2 = findInRotArr2(0, a.length - 1, search, rotDeg, a);

    assert find1 == find2 : "find positions dont agree";

    return find1;
  }

  private static int findInRotArr2(int lo, int hi, int search, int pivot, int[] a) {
    if (hi < lo) {
      return -1;
    }

    int m = (lo + hi) / 2;
    int mi = (m + pivot) % a.length;
    int mval = a[mi];

    if (mval == search) {
      return mi;
    }

    if (search < mval) {
      return findInRotArr2(lo, m - 1, search, pivot, a);
    }
    return findInRotArr2(m + 1, hi, search, pivot, a);
  }

  public static int findRotDeg(int lo, int hi, int[] a) {
    int rot1 = findRotDeg1(lo, hi, a);
    int rot2 = findRotDeg2(lo, hi, a);

    assert rot1 == rot2 : "rot degrees don't agree";

    return rot1;

  }

  public static int findRotDeg2(int lo, int hi, int[] a){
    int loVal = a[lo];
    int hiVal = a[hi];

    if (hiVal >= loVal) {
      return lo;
    }

    int size = hi - lo + 1;
    if (size == 2 || size == 1) {
      return hi;
    }

    int m = lo + size/2;
    int mVal = a[m];

    if (mVal < loVal) {
      return findRotDeg2(lo, m, a);
    }
    return findRotDeg2(m + 1, hi, a);

  }

  public static int findRotDeg1(int lo, int hi, int[] a) {
    int size = hi - lo + 1;
    if (size <= 1) {
      return lo;
    } else if (size == 2) {
      return (a[lo] < a[hi]) ? lo : hi;
    }

    int m = lo + size/2;

    if (a[lo] > a[m]) {
      return findRotDeg(lo, m, a);
    } else if (a[m] > a[hi]) {
      return findRotDeg(m, hi, a);
    }

    // list is sorted
    return lo;
  }

  private static int findInRotArr1(int lo, int hi, int search, int[] a) {
    int size;
    size = hi - lo + 1;

    if (size < 1) {
      return -1;
    }

    if (size == 1) {
      return a[lo % a.length] == search ? lo % a.length : -1;
    }

    int m = (lo + size/2) % a.length;

    if (a[m] == search) {
      return m;
    } else if (search < a[m]) {
      return findInRotArr1(lo, lo + size/2 - 1, search, a);
    }
    return findInRotArr1(lo + size/2 + 1, hi, search, a);
  }
}
