package g_questions;

public class FlipBit {
  public static boolean getBit(int n, int i) {
    return (n & (1 << i)) != 0;
  }
  public static int longestSeq(int n) {
    int longestSeq = 0;
    int left = 0;
    int right = 0;
    int longestSingleSeq = 0;
    boolean onLeft = true;

    if (n == 0) {
      return 1;
    }

    for (int i = 31; i >= 0; i--) {
      boolean bit = getBit(n, i);

      if (bit) {
        if (onLeft) {
          left++;
          if (left + 1 > longestSingleSeq) {
            longestSingleSeq = left + 1;
          }
        } else {
          right++;
          if (right + 1 > longestSingleSeq) {
            longestSingleSeq = right + 1;
          }
          if (left + right + 1 > longestSeq) {
            longestSeq = left + right + 1;
          }
        }
      } else {
        if (left > 0) {
          if (onLeft) {
            onLeft = false;
          } else {
            if (right > 0) {
              left = right;
              right = 0;
            } else {
              left = 0;
              onLeft = true;
            }
          }
        }
      }

    }

    longestSingleSeq = Math.min(32, longestSingleSeq);

    return (longestSeq > longestSingleSeq) ? longestSeq : longestSingleSeq;
  }
}
