package g_questions;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;

public class FasterPermutations {
  private static HashMap<Collection<Integer>, List<List<Integer>>> listToPermutations;
  public static int numberOfOperations;
  public static boolean isOptimizationEnabled = true;
  public static boolean debug = false;

  public static List<List<Integer>> permuteList(List<Integer> list) {
    listToPermutations = new HashMap<Collection<Integer>, List<List<Integer>>>();
    numberOfOperations = 0;
    HashSet<Integer> set = new HashSet<Integer>(list);
    List<List<Integer>> permutations = permuteSet(set);
    if (debug) System.out.println(list.size() + " items with " + numberOfOperations + " operations");
    return permutations;
  }

  public static List<List<Integer>> permuteSet(Collection<Integer> set) {
    List<List<Integer>> permutations = listToPermutations.get(set);
    numberOfOperations += set.size();
    if (permutations != null && isOptimizationEnabled) {
      numberOfOperations += 1;
      return permutations;
    }

    permutations = new ArrayList<List<Integer>>();

    if (set.size() <= 1) {
      numberOfOperations += 1;
      permutations.add(new LinkedList<Integer>(set));
      return permutations;
    }

    // We should not have to perform this copy, however if we do not
    // we get a concurrency error for adding & removing things from the set while
    // we are iterating through it
    // for now we will simply calculate the number of operations (and thus the run time)
    // as if we did not have to do this extra step
    // numberOfOperations += set.size();
    // it doesn't make much of a difference anyway
    List<Integer> list = new ArrayList<Integer>(set);

    for (Integer item : list) {
      set.remove(item);

      List<List<Integer>> permsOfSubList = permuteSet(new HashSet<Integer>(set));
      for (List<Integer> permOfSubList : permsOfSubList) {
        LinkedList<Integer> perm = new LinkedList<Integer>();
        perm.add(item);
        numberOfOperations += 1;
        //numberOfOperations += permOfSubList.size();

        // TODO this should really be an O(1) linked list operation
        // however i am afraid that it is not and that it as actually an O(perm.length())
        // operation. Maybe the only way to avoid it would be to write my
        // own linked list?
        perm.addAll(permOfSubList);

        permutations.add(perm);
      }
      set.add(item);
    }
    listToPermutations.put(set, permutations);
    return permutations;
  }
}




