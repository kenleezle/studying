package g_questions;


public class SortedMerge {
  public static void merge(int[] a, int[] b, int aLen) {
    int ari = aLen - 1;
    int bri = b.length - 1;
    int awi = aLen + b.length - 1;

    while (ari >= 0 && bri >= 0) {
      if (a[ari] > b[bri]) {
        a[awi--] = a[ari--];
      } else {
        a[awi--] = b[bri--];
      }
    }
    while (ari >= 0) { a[awi--] = a[ari--]; }
    while (bri >= 0) { a[awi--] = b[bri--]; }
  }
}
