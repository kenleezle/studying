package g_questions;

public class DCAIntermediateResult {
  public boolean hasFoundElement1;
  public boolean hasFoundElement2;
  public Node dca;

  public boolean hasFoundBothElements() {
    return hasFoundElement1 && hasFoundElement2;
  }

  public boolean hasFoundEitherElement() {
    return hasFoundElement1 || hasFoundElement2;
  }

  public boolean hasFoundDCA() {
    return (dca != null);
  }

  public boolean isReadyForDCA() {
    return hasFoundBothElements() && !hasFoundDCA();
  }

  public void incorporateResult(DCAIntermediateResult result) {
    hasFoundElement1 = hasFoundElement1 || result.hasFoundElement1;
    hasFoundElement2 = hasFoundElement2 || result.hasFoundElement2;
    dca = result.dca;
  }
}
