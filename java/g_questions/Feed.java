package g_questions;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class Feed {
  private DoublyLinkedList list;
  private Map<Integer, DLLNode> map;
  private int size;

  public Feed() {
    list = new DoublyLinkedList();
    map = new HashMap<Integer, DLLNode>();
    size = 0;
  }

  public void add(Integer element) {
    DLLNode node = map.get(element);
    if (node != null) {
      list.moveToTail(node);
    } else {
      list.addToTail(element);
    }
    size++;
  }
  public boolean contains(Integer element) {
    return map.containsKey(element);
  }
  public void remove(Integer element) {
    if (!contains(element)) return;

    DLLNode node = map.get(element);
    map.remove(element);
    list.remove(node);
    size--;
  }

  public Integer pop() {
    DLLNode node = list.removeFromTail();

    if (node != null) map.remove(node.data);

    size--;
    return (node == null) ? null : node.data;
  }

  public int size() {
    return size;
  }

  public void evict(long maxDurationInFeedMs) {
    List<DLLNode> nodesToEvict = list.evict(maxDurationInFeedMs);
    for (DLLNode node : nodesToEvict)
      map.remove(node.data);
  }
}
