package g_questions;
import java.util.HashMap;

public class FractionToDecimal {
  public static int divides(double q, double p) {
    int i = 1;
    while (p * i <= q) i++;
    return i - 1;
  }

  public static String convert(int numerator, int denominator) {
    String result = "";
    result += numerator / denominator;
    if (numerator % denominator == 0) return result;
    result += ".";

    if (numerator > denominator) numerator = numerator % denominator;

    HashMap<Integer, Integer> r2i = new HashMap<Integer, Integer>();
    String fraction = "";
    numerator *= 10;
    int i = 0;
    while (numerator > 0) {
      int digit = numerator / denominator;
      int remainder = numerator % denominator;

      if (r2i.containsKey(numerator)) {
        int index = r2i.get(numerator);
        fraction = fraction.substring(0, index) + "(" + fraction.substring(index) + ")";
        break;
      }
      r2i.put(numerator, i++);

      fraction += digit;
      numerator -= digit * denominator;
      numerator *= 10;
    }
    return result + fraction;
  }
}
