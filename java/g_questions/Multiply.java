package g_questions;

public class Multiply {
  public static int multiply(int a, int b) {
    if (a == 0 || b == 0) {
      return 0;
    }

    if (a == 1) {
      return b;
    }
    if (b == 1) {
      return a;
    }

    int apow = 1;
    while (apow <= a) {
      apow <<= 1;
    }
    apow >>= 1;

    int bpow = 1;
    int bexp = 0;
    while (bpow <= b) {
      bpow <<= 1;
      bexp++;
    }
    bpow >>= 1;
    bexp--;

    int arem = a - apow;
    int brem = b - bpow;

    int abpow = apow;
    for (int i = 0; i < bexp; i++) {
      abpow <<= 1;
    }

    return abpow + multiply(apow, brem) + multiply(bpow, arem) + multiply(arem, brem);
  }
}
