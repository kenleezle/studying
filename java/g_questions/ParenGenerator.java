package g_questions;

import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

public class ParenGenerator {
  public static Set<String> allUniqueCombos(int n) {
    Set<String> set = new HashSet<String>();
    if (n == 0) {
      return set;
    } else if (n == 1) {
      set.add("()");
      return set;
    }


    for (String term : allCombos(n - 1)) {
      set.add("()" + term);
      set.add("(" + term + ")");
      set.add(term + "()");
    }

    return set;
  }
  public static List<String> allCombos(int n) {
    List<String> list = new ArrayList<String>();
    if (n == 0) {
      return list;
    } else if (n == 1) {
      list.add("()");
      return list;
    }


    for (String term : allCombos(n - 1)) {
      list.add("()" + term);
      list.add("(" + term + ")");
      list.add(term + "()");
    }

    return list;
  }

}
