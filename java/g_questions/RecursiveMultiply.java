package g_questions;

public class RecursiveMultiply {
  public static int multiply(int a, int b) {
    if (a == 0 || b == 0) { return 0; }
    if (a == 1) { return b; }
    if (b == 1) { return a; }

    if (isOdd(b)) { return a + multiply(a, b - 1); }
    if (isOdd(a)) { return b + multiply(b, a - 1); }

    return multiply(a >> 1, b >> 1) << 2;
  }
  public static int multiply2(int a, int b) {
    int count = 0;
    int product = 0;
    while (b > 0) {
      if (isOdd(b)) { product += (a << count); }
      count++;
      b = b >> 1;
    }

    return product;
  }

  public static boolean isOdd(int num) {
    return ((num & 1) == 1);
  }
}
