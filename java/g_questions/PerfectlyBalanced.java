package g_questions;

import java.util.List;
import java.util.ArrayList;

public class PerfectlyBalanced {
  public static boolean isPerfectlyBalanced(Node root) {
    return countPerfectlyBalanced(root) >= 0;
  }

  private static int countPerfectlyBalanced(Node root) {
    if (root == null) {
      return 0;
    } else if (root.hasNoChildren()) {
      return 1;
    } else if (root.hasOneChild()) {
      return -1;
    } else {
      int countOfLeftChildren = countPerfectlyBalanced(root.leftChild);
      int countOfRightChildren = countPerfectlyBalanced(root.rightChild);

      if (countOfLeftChildren == countOfRightChildren) {
        return 1 + countOfLeftChildren + countOfRightChildren;
      } else {
        return -1;
      }
    }
  }

  public static boolean isPerfectlyBalancedIterative(Node root) {
    int depth = 0;
    List<Node> nodesAtDepth = new ArrayList<Node>();
    if (root != null) {
      nodesAtDepth.add(root);
    }

    while (!nodesAtDepth.isEmpty()) {
      int actualNodesAtDepth = nodesAtDepth.size();
      int perfectNodesAtDepth = (int)Math.pow(2, (double)depth);

      if (actualNodesAtDepth != perfectNodesAtDepth) {
        return false;
      }

      depth++;
      nodesAtDepth = allChildren(nodesAtDepth);
    }

    return true;
  }

  private static List<Node> allChildren(List<Node> parents) {
    List<Node> children = (List<Node>)new ArrayList<Node>();

    for (Node parent: parents) {
      children.addAll(parent.children());
    }

    return children;
  }
}
