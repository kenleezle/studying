package g_questions;

public class Sequence {
  public int start;
  public int end;
  public int sum;

  public Sequence() {
    start = 0;
    end = 0;
    sum = 0;
  }

  public Sequence(int start, int end) {
    this.start = start;
    this.end = end;
    sum = 0;
  }

  public void restartAtIndex(int newStart) {
    start = newStart;
    end = newStart;
    sum = 0;
  }
}
