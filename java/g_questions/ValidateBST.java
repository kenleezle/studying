package g_questions;

public class ValidateBST {
  public static class IntermediateResult {
    public boolean isBST;
    public Node last;

    public IntermediateResult(boolean isBST, Node last) {
      this.isBST = isBST;
      this.last = last;
    }
  }
  public static boolean isValid(Node root) {
    return myInorder(root, null).isBST;
  }

  public static IntermediateResult myInorder(Node root, Node last) {
    if (root == null) {
      return new IntermediateResult(true, last);
    }
    IntermediateResult result = myInorder(root.leftChild, last);
    if (!result.isBST) {
      return result;
    }

    if (result.last != null && result.last.data > root.data) {
      return new IntermediateResult(false, last);
    }

    return myInorder(root.rightChild, root);
  }
}
