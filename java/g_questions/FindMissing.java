package g_questions;
import java.util.List;

public class FindMissing {

  public static int getJthBitFromIthElement(List<Integer> l, int i, int j) {
    int element = (i >= l.size()) ? i : l.get(i);
    return (element & (1 << j)) == 0 ? 0 : 1;
  }

  public static int find(List<Integer> l, int n) {
    int k = 1;
    int count = 0;
    int missing = 0;
    int mask = 0;

    while (k <= n) { 
      k <<= 1; 
      count++; 
    }

    for (int j = 0; j < count; j++) {
      for (int i = 0; i < k; i++) {
        if ((i & ((1 << j) - 1)) != mask) { continue; }
        missing = missing ^ (getJthBitFromIthElement(l, i, j) << j);
      }

      mask = missing;
    }

    return missing;
  }
}
