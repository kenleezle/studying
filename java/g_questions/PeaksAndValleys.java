package g_questions;

import java.util.List;

public class PeaksAndValleys {
  public static List<Integer> sort(List<Integer> a) {
    boolean pd = a.get(1) > a.get(0);

    for (int i = 2; i < a.size(); i++) {
      boolean cd = a.get(i) > a.get(i-1);
      if (cd == pd) {
        swap(a, i, i-1);
        cd = !cd;
      }
      pd = cd;
    }

    return a;
  }

  public static void swap(List<Integer> a, int b, int c) {
    Integer tmp = a.get(b);
    a.set(b, a.get(c));
    a.set(c, tmp);
  }
}
