package g_questions;

import java.io.*;
import java.util.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

public class ValidArithmeticExpressions {
  public static final List<Character> variables = Arrays.asList('a');
  public static final List<Character> operators = Arrays.asList('+');
  
  public List<String> validExpressions(int length) {
    List<String> results = new ArrayList<String>();
    validExpressions("", 0, 0, length, results);
    return results;
  }
  
  public boolean isValue(char c) {
    return variables.contains(c) || c == ')';
  }
  
  public boolean endsWithValue(String prefix) {
    if (prefix.length() == 0) return false;
    
    char lastChar = prefix.charAt(prefix.length() - 1);
    return isValue(lastChar);
  }

  public void validExpressions(String prefix, int index, int openCount, int length, List<String> results) {
    if (index >= length) {
      //endsWithValue(prefix) ? Arrays.asList(prefix) : new ArrayList<String>();
      if (openCount == 0 && endsWithValue(prefix)) results.add(prefix);
      return;
    }
    
    List<String> expressions = new ArrayList<String>();
    boolean lastCharIsValue = endsWithValue(prefix);
    int remainingCharCount = length - prefix.length();
    
    if (lastCharIsValue) {
      if (remainingCharCount > openCount)
        for (char operator : operators)
          validExpressions(prefix + operator, index + 1, openCount, length, results);

      if (openCount > 0)
        validExpressions(prefix + ')', index + 1, openCount - 1, length, results);

    } else {
      // last char could be: null, '(', or an operator
      for (char var : variables)
        validExpressions(prefix + var, index + 1, openCount, length, results);

      if (openCount + 1 <= remainingCharCount - 1)
        validExpressions(prefix + '(', index + 1, openCount + 1, length, results);
    }
    
    //return expressions;
  }
}

