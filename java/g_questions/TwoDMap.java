package g_questions;

public class TwoDMap {
  public int[][] map;
  public boolean[][] visited;

  public TwoDMap(int[][] map) {
    this.map = map;
    visited = new boolean[map.length][];
    for (int x = 0; x < map.length; x++) {
      visited[x] = new boolean[map[x].length];
    }
  }

  public boolean isOnMap(int x, int y) {
    boolean xIsOnMap = (x < map.length && x >= 0);
    if (!xIsOnMap) {
      return false;
    }

    boolean yIsOnMap = (y < map[x].length && y >= 0);
    return yIsOnMap;
  }

  public boolean isLand(int x, int y) {
    return map[x][y] == 1;
  }

  public boolean hasBeenVisited(int x, int y) {
    return visited[x][y];
  }

  public void markAsVisited(int x, int y) {
    visited[x][y] = true;
  }

  public static int countIslands(int[][] map) {
    return new TwoDMap(map).countIslands();
  }

  public int countIslands() {
    int count = 0;
    for (int x = 0; x < map.length; x++) {
      for (int y = 0; y < map[x].length; y++) {
        if (hasBeenVisited(x,y)) {
          continue;
        }

        if (isLand(x,y)) {
          count++;
        }

        exploreIsland(x,y);
      }
    }
    return count;
  }

  public void exploreIsland(int x, int y) {
    if (!isOnMap(x,y)) {
      return;
    }

    if (hasBeenVisited(x,y)) {
      return;
    }

    markAsVisited(x,y);

    if (!isLand(x,y)) {
      return;
    }

    exploreIsland(x-1,y);
    exploreIsland(x+1,y);
    exploreIsland(x,y-1);
    exploreIsland(x,y+1);
  }
}
