package g_questions;

import java.time.Instant;

public class DLLNode {

  public DLLNode prev;
  public DLLNode next;
  public Integer data;
  public Instant addedTime;

  public DLLNode(Integer d) {
    data = d;
    addedTime = Instant.now();
  }

  public void setAddedTimeToNow() {
    addedTime = Instant.now();
  }

  public long duration() {
    return Instant.now().toEpochMilli() - addedTime.toEpochMilli();
  }
}
