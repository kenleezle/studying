package g_questions;

public class DCAWithParent {
  public static Node moveUp(Node node, int depth) {
    for (int i = 0; i < depth; i++) {
      node = node.parent;
    }
    return node;
  }

  public static int depth(Node node) {
    int depth = 0;

    while ((node = node.parent) != null) {
      depth++;
    }

    return depth;
  }

  public static boolean isParent(Node node, Node parent) {
    while ((node = node.parent) != null) {
      if (parent.equals(node)) {
        return true;
      }
    }
    return false;
  }

  public static Node dca(Node x, Node y) {
    if (isParent(x, y)) {
      return y;
    } else if (isParent(y, x)) {
      return x;
    }

    int x_depth = depth(x);
    int y_depth = depth(y);

    if (x_depth > y_depth) {
      x = moveUp(x, x_depth - y_depth);
    } else if (y_depth > x_depth) {
      y = moveUp(y, y_depth - x_depth);
    }

    while (x != y && x != null && y != null) {
      x = x.parent;
      y = y.parent;
    }

    if (x == y) {
      return x;
    }

    return null;
  }
}
