package tree;

import g_questions.*;
import java.util.List;
import java.util.ArrayList;
public class TreeTraversals {
  public static List<Integer> preOrder(Node root) {
    List<Integer> results = new ArrayList<Integer>();
    myPreOrder(root, results);
    return results;
  }

  private static void myPreOrder(Node root, List<Integer> results) {
    if (root == null) {
      return;
    }

    results.add(root.data);

    for (Node child : root.children()) {
      myPreOrder(child, results);
    }
  }

  public static List<Integer> inOrder(Node root) {
    List<Integer> results = new ArrayList<Integer>();
    myInOrder(root, results);
    return results;
  }

  private static void myInOrder(Node root, List<Integer> results) {
    if (root == null) {
      return;
    }

    myInOrder(root.leftChild, results);
    results.add(root.data);
    myInOrder(root.rightChild, results);
  }

  public static List<Integer> postOrder(Node root) {
    List<Integer> results = new ArrayList<Integer>();
    myPostOrder(root, results);
    return results;
  }

  private static void myPostOrder(Node root, List<Integer> results) {
    if (root == null) {
      return;
    }

    for (Node child : root.children()) {
      myPostOrder(child, results);
    }
    results.add(root.data);
  }
}
