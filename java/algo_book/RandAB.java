package algo_book;

import java.util.Random;

public class RandAB {
  public static Random random = new Random();

  public static int rand01() {
    int result = random.nextInt(2);
    return result;
  }

  public static int randPow2(int exp) {
    int result = 0;
    for (int i = 0; i < exp; i++) {
      result += rand01() * Math.pow(2, i);
    }
    return result;
  }

  public static int randab(int a, int b) {
    int size = b - a;
    int exp = 0;
    while (Math.pow(2, exp) < size) {
      exp++;
    }

    int result = randPow2(exp);
    while (result > size) {
      result = randPow2(exp);
    }
    result += a;

    return a + result;
  }
}
