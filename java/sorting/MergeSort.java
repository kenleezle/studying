package sorting;

public class MergeSort {
  public static int[] sort(int[] arr) {
    mySort(arr, 0, arr.length);
    return arr;
  }

  // lo - inclusive
  // hi - exclusive
  private static void mySort(int[] arr, int lo, int hi) {
    int length = hi - lo;
    if (length < 2) {
      return;
    }

    int m = lo + length/2;
    mySort(arr, lo, m);
    mySort(arr, m, hi);
    merge(arr, lo, m, hi);
  }
  private static void merge(int[] arr, int lo, int m, int hi) {
    int leftI, leftHi, rightI, rightHi;
    leftI = lo;
    leftHi = rightI = m;
    rightHi = hi;

    int length = hi - lo;

    int[] temp = new int[length];
    int tempI = 0;

    while (leftI < leftHi && rightI < hi) {
      if (arr[leftI] < arr[rightI]) {
        temp[tempI++] = arr[leftI++];
      } else {
        temp[tempI++] = arr[rightI++];
      }
    }
    while (leftI < leftHi) {
      temp[tempI++] = arr[leftI++];
    }
    while (rightI < hi) {
      temp[tempI++] = arr[rightI++];
    }

    for (tempI = 0; tempI < length; tempI++) {
      arr[lo+tempI] = temp[tempI];
    }
  }
}
