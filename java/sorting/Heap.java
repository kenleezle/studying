package sorting;

import java.util.ArrayList;

public class Heap {
  private boolean isMinHeap;
  private ArrayList<Integer> data;

  public Heap() {
    isMinHeap = false;
    data = new ArrayList<Integer>();
  }

  public Heap(boolean isMinHeap) {
    this.isMinHeap = isMinHeap;
    data = new ArrayList<Integer>();
  }

  public Heap(ArrayList<Integer> a) {
    isMinHeap = false;
    data = a;
  }

  public static Heap build(ArrayList<Integer> a) {
    Heap h = new Heap(a);
    for (int i = a.size() / 2 - 1; i >=0; i--) {
      h.siftDown(i);
    }
    return h;
  }

  public static void sort(ArrayList<Integer> a) {
    Heap h = build(a);
    for (int i = a.size() - 1; i > 0; i--) {
      Integer tmp = a.get(i);
      a.set(i, a.get(0));
      a.set(0, tmp);
      h.siftDown(0, i);
    }
  }

  private int left(int i) {
    return 2*i + 1;
  }

  private int right(int i) {
    return 2*i + 2;
  }

  private int parent(int i) {
    return (i - 1) / 2;
  }

  private boolean compare(int a, int b) {
    if (isMinHeap) { return a <= b; }
    else { return a >= b; }
  }

  private void siftUp() {
    siftUp(data.size() - 1);
  }

  private void siftUp(int i) {
    if (i <= 0 || i >= data.size()) { return; }

    int pI = parent(i);
    int tmp = data.get(i);

    if (compare(tmp, data.get(pI))) {
      data.set(i, data.get(pI));
      data.set(pI, tmp);
      siftUp(pI);
    }
  }

  private void siftDown() {
    siftDown(0);
  }

  private void siftDown(int i) {
    siftDown(i, data.size());
  }

  private void siftDown(int i, int length) {
    if (i >= length) { return; }

    int val = data.get(i);
    int largestI = i;

    if (left(i) < length) {
      int left = data.get(left(i));
      if (compare(left, val)) {
        largestI = left(i);
      }
    }

    if (right(i) < length) {
      int right = data.get(right(i));
      if (compare(right, data.get(largestI))) {
        largestI = right(i);
      }
    }

    if (largestI != i) {
      data.set(i, data.get(largestI));
      data.set(largestI, val);
      siftDown(largestI, length);
    }
  }

  public boolean isEmpty() {
    return data.isEmpty();
  }
  public Integer peek() {
    if (isEmpty()) { return null; }
    return data.get(0);
  }

  public Integer pop() {
    if (isEmpty()) { return null; }

    Integer popped = data.get(0);
    int last = data.size() - 1;

    data.set(0, data.get(last));
    data.remove(last);
    siftDown();

    return popped;
  }

  public void push(Integer i) {
    data.add(i);
    siftUp();
  }
}
