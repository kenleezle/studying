package sorting;

import java.util.List;

public class InsertionSort {
  public static int[] sort(int[] arr) {
    for (int i = 1; i < arr.length; i++) {
      int toBeInserted = arr[i];
      int indexToInsert;
      for (indexToInsert = 0; ((indexToInsert < i) && (toBeInserted > arr[indexToInsert])); indexToInsert++);
      shiftRightByOne(arr, indexToInsert, i);
      arr[indexToInsert] = toBeInserted;
    }
    return arr;
  }
  public static void shiftRightByOne(int[] arr, int start, int end) {
    for (int i = end; i > start; i--) {
      arr[i] = arr[i-1];
    }
  }
  public static List<Integer> sort(List<Integer> list) {
    for (int i = 1; i < list.size(); i++) {
      Integer toBeInserted = list.get(i);
      int indexToInsert;
      for (indexToInsert = 0; ((indexToInsert < i) && (toBeInserted > list.get(indexToInsert))); indexToInsert++);
      shiftRightByOne(list, indexToInsert, i);
      list.set(indexToInsert, toBeInserted);
    }
    return list;
  }
  public static void shiftRightByOne(List<Integer> list, int start, int end) {
    for (int i = end; i > start; i--) {
      list.set(i, list.get(i-1));
    }
  }

}
