package sorting;

public class SelectionSort {
  public static int[] sort(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      int indexOfSmallest = i;
      int j;
      for (j = i; j < arr.length; j++) {
        if (arr[j] < arr[indexOfSmallest]) {
          indexOfSmallest = j;
        }
      }

      int value = arr[indexOfSmallest];
      shiftRight(arr, i, indexOfSmallest);
      arr[i] = value;
    }
    return arr;
  }

  public static void shiftRight(int[] arr, int start, int end) {
    for (int i = end; i > start; i--) {
      arr[i] = arr[i-1];
    }
  }
}
