package sorting;

public class QuickSort {
  public static int[] sort(int[] arr) {
    rSort(arr, 0, arr.length - 1);
    return arr;
  }

  // lo - inclusive
  // hi - inclusive
  private static void rSort(int[] arr, int lo, int hi) {
    if (lo < hi) {
      int p_i = partition(arr, lo, hi); // pivot index
      rSort(arr, lo, p_i - 1);
      rSort(arr, p_i + 1, hi);
    }
  }
  private static int partition(int[] arr, int lo, int hi) {
    int p = arr[hi]; // pivot
    int p_i = lo;
    for (int i = lo; i < hi; i++) {
      if (arr[i] <= p) {
        swap(arr, p_i, i);
        p_i++;
      }
    }
    swap(arr, p_i, hi);
    return p_i;
  }
  private static void swap(int[] arr, int l, int r) {
    if (l == r) {
      return;
    }
    int tmp = arr[l];
    arr[l] = arr[r];
    arr[r] = tmp;
  }
}
