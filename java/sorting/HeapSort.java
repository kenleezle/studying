package sorting;

import java.util.ArrayList;
import java.util.List;

public class HeapSort {
  public static List<Integer> sort(List<Integer> list) {
    recursiveHeapify(list, 0, list.size());
    for (int unsortedSize = list.size()-1; unsortedSize > 0; unsortedSize--) {
      swap(list, 0, unsortedSize);
      recursiveSiftDown(list, 0, unsortedSize);
    }
    return list;
  }
  public static void recursiveHeapify(List<Integer> list, int rootIndex, int size) {
    if (children(list, rootIndex, size).isEmpty()) {
      return;
    }
    int indexOfLeftChild = indexOfLeftChild(rootIndex);
    int indexOfRightChild = indexOfRightChild(rootIndex);
    if (isIndexPresentInHeap(indexOfLeftChild, size)) {
      recursiveHeapify(list, indexOfLeftChild, size);
    }
    if (isIndexPresentInHeap(indexOfRightChild, size)) {
      recursiveHeapify(list, indexOfRightChild, size);
    }
    recursiveSiftDown(list, rootIndex, size);
  }

  private static void recursiveSiftDown(List<Integer> list, int rootIndex, int size) {
    int leftChildIndex = indexOfLeftChild(rootIndex);
    int rightChildIndex = indexOfRightChild(rootIndex);

    Integer parent = list.get(rootIndex);
    Integer leftChild = leftChild(list, rootIndex, size);
    Integer rightChild = rightChild(list, rootIndex, size);

    if (children(list, rootIndex, size).isEmpty()) {
      return;
    }

    if ((rightChild != null) && (parent < leftChild) && (parent < rightChild)) {
      if (leftChild > rightChild) {
        swap(list, rootIndex, leftChildIndex);
        recursiveSiftDown(list, leftChildIndex, size);
      } else {
        swap(list, rootIndex, rightChildIndex);
        recursiveSiftDown(list, rightChildIndex, size);
      }
    } else if (parent < leftChild) {
      swap(list, rootIndex, leftChildIndex);
      recursiveSiftDown(list, leftChildIndex, size);
    } else if ((rightChild != null) && (parent < rightChild)) {
      swap(list, rootIndex, rightChildIndex);
      recursiveSiftDown(list, rightChildIndex, size);
    }
  }

  private static void swap(List<Integer> list, int parentIndex, int childIndex) {
    Integer temp = list.get(parentIndex);
    list.set(parentIndex, list.get(childIndex));
    list.set(childIndex, temp);
  }

  private static List<Integer> children(List<Integer> list, int parentIndex, int size) {
    List<Integer> children = new ArrayList<Integer>();
    Integer leftChild = leftChild(list, parentIndex, size);
    Integer rightChild = rightChild(list, parentIndex, size);
    if (leftChild != null) {
      children.add(leftChild);
    }
    if (rightChild != null) {
      children.add(rightChild);
    }
    return children;
  }

  private static Integer leftChild(List<Integer> list, int parentIndex, int size) {
    int indexOfLeftChild = indexOfLeftChild(parentIndex);
    if (isIndexPresentInHeap(indexOfLeftChild, size)) {
      return list.get(indexOfLeftChild);
    }
    return null;
  }

  private static Integer rightChild(List<Integer> list, int parentIndex, int size) {
    int indexOfRightChild = indexOfRightChild(parentIndex);
    if (isIndexPresentInHeap(indexOfRightChild, size)) {
      return list.get(indexOfRightChild);
    }
    return null;
  }

  private static int indexOfLeftChild(int parentIndex) {
    return (parentIndex * 2) + 1;
  }

  private static int indexOfRightChild(int parentIndex) {
    return (parentIndex * 2) + 2;
  }

  private static boolean isIndexPresentInHeap(int nodeIndex, int listSize) {
    return (nodeIndex < listSize);
  }
}
