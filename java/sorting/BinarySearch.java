package sorting;

import java.util.List;
public class BinarySearch {
  public static int findIndex(List<Integer> list, Integer search) {
    int lo = 0; // inclusive
    int hi = list.size() - 1; // inclusive

    while (lo <= hi) {
      int mi = (lo + hi)/2;
      int me = list.get(mi);

      if (me == search) {
        return mi;
      } else if (search < me) {
        hi = mi - 1;
      } else {
        lo = mi + 1;
      }
    }

    return -1;
  }
}

