package sorting;

import java.util.ArrayList;
import java.util.List;

public class RadixSort {
  public static List<Integer> sort(List<Integer> list) {
    List<List<Integer>> buckets = initializeBuckets(10);
    int maxSigPos = maxSigDigForList(list);
    for (int sigPos = 1; sigPos <= maxSigPos; sigPos++) {
      for (Integer item : list) {
        int digit = digitInSigPos(item, sigPos);
        List<Integer> bucket = buckets.get(digit);
        bucket.add(item);
      }
      emptyBucketsIntoList(buckets, list);
    }
    return list;
  }

  public static List<List<Integer>> initializeBuckets(int numBuckets) {
    ArrayList<List<Integer>> buckets = new ArrayList<List<Integer>>(numBuckets);
    for (int bucketIndex = 0; bucketIndex < numBuckets; bucketIndex++) {
      buckets.add((List<Integer>)new ArrayList<Integer>());
    }
    return buckets;
  }

  public static int maxSigDigForList(List<Integer> list) {
    int maxSigPos = 0;
    for (Integer item : list) {
      int sigDig = sigDigForItem(item);
      if (sigDig > maxSigPos) {
        maxSigPos = sigDig;
      }
    }
    return maxSigPos;
  }

  public static int sigDigForItem(Integer item) {
    int exponent;
    for (exponent = 0; item/(int)Math.pow(10, exponent) >= 1; exponent++);
    return exponent;
  }

  public static int digitInSigPos(Integer item, int sigPos) {
    return (item/((int)Math.pow(10,(sigPos - 1)))) % 10;
  }

  public static void emptyBucketsIntoList(List<List<Integer>> buckets, List<Integer> list) {
    int listIndex = 0;
    for (List<Integer> bucket : buckets) {
      for (Integer item : bucket) {
        list.set(listIndex++, item);
      }
    }
    emptyBuckets(buckets);
  }

  public static void emptyBuckets(List<List<Integer>> buckets) {
    for (List<Integer> bucket : buckets) {
      bucket.clear();
    }
  }

}
