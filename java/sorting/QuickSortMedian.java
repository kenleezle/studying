package sorting;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class QuickSortMedian {
  public static Integer median(List<Integer> list) {
    int medianIndex = list.size()/2;
    List<Integer> copyOfList = (List<Integer>)new ArrayList<Integer>(list);

    sortToFindMedian(copyOfList, 0, copyOfList.size(), medianIndex);
    return copyOfList.get(medianIndex);
  }

  public static void sortToFindMedian(List<Integer> list, int start, int end, int medianIndex) {
    int size = end - start;
    if (size <= 1) {
      return;
    } else if (medianIndex >= end) {
      return;
    } else if (medianIndex < start) {
      return;
    }

    int partitionIndex = end - 1;
    partitionIndex = partition(list, start, end, partitionIndex);
    if (partitionIndex == medianIndex) {
      return;
    }

    sortToFindMedian(list, start, partitionIndex, medianIndex);
    sortToFindMedian(list, partitionIndex + 1, end, medianIndex);
  }

  public static int partition(List<Integer> list, int start, int end, int partitionIndex) {
    int finalPartitionIndex = start;
    Integer partition = list.get(partitionIndex);

    for (int i = start; i < partitionIndex; i++) {
      if (list.get(i) < partition) {
        swap(list, i, finalPartitionIndex++);
      }
    }
    swap(list, partitionIndex, finalPartitionIndex);
    return finalPartitionIndex;
  }

  public static void swap(List<Integer> list, int leftIndex, int rightIndex) {
    Integer leftValue = list.get(leftIndex);
    list.set(leftIndex, list.get(rightIndex));
    list.set(rightIndex, leftValue);
  }
}
